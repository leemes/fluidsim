#include "clsourceloader.h"
#include <QFile>
#include <QStringList>
#include <QDebug>

static const QStringList ignoreIncludes = {
    "cldefs.h",
    "../cldefs.h",
    "../../cldefs.h"
};

CLSourceLoader::CLSourceLoader(QString basePath) :
    m_basePath(basePath)
{
}

void CLSourceLoader::addAutoInclude(QString typeName, QString fileName)
{
    m_autoIncludes[typeName] = fileName;
}

QByteArray CLSourceLoader::loadSource(const QString &fileName, const QByteArray &prependCode) const
{
    QStringList alreadyIncluded = ignoreIncludes;
    QByteArray source = prependCode + load(fileName, alreadyIncluded);
    resolveIncludes(source, alreadyIncluded);
    source.prepend(autoInclude(source));
    resolveIncludes(source, alreadyIncluded);
    //qDebug() << "########################################################################################################################################";
    //qDebug() << source;
    //qDebug() << "########################################################################################################################################";
    return source;
}

QByteArray CLSourceLoader::loadAndResolveIncludes(const QString &fileName, QStringList &alreadyIncluded) const
{
    QByteArray source = load(fileName, alreadyIncluded);
    resolveIncludes(source, alreadyIncluded);
    return source;
}

QByteArray CLSourceLoader::load(const QString &fileName, QStringList &alreadyIncluded) const
{
    if (alreadyIncluded.contains(fileName))
        return QByteArray();
    alreadyIncluded.append(fileName);

    QFile file(m_basePath + fileName);
    if (!file.open(QFile::ReadOnly))
        qFatal("CLSourceLoader: Error opening file \"%s\".", qPrintable(file.fileName()));
    return file.readAll();
}

void CLSourceLoader::resolveIncludes(QByteArray &source, QStringList &alreadyIncluded) const
{
    while (source.contains("\n#include") || source.startsWith("#include")) {
        int pos = source.startsWith("#include") ? 0 : (source.indexOf("\n#include") + 1);
        int len = source.indexOf("\n", pos) + 1 - pos;
        QByteArray includeDirective = source.mid(pos, len);
        Q_ASSERT(includeDirective.startsWith("#include") && includeDirective.endsWith("\n"));
        // strip "#include", "\n" and trim
        includeDirective.chop(1);
        includeDirective = includeDirective.mid(strlen("#include")).trimmed();
        if (!(includeDirective.startsWith("\"") && includeDirective.endsWith("\"")))
            qFatal("CLSourceLoader: Syntax error when parsing an #include directive.");
        includeDirective = includeDirective.mid(1, includeDirective.length() - 2);

        QByteArray includedFile = loadAndResolveIncludes(includeDirective, alreadyIncluded);

        source = source.left(pos) + includedFile + source.mid(pos + len);
    }
}

QByteArray CLSourceLoader::autoInclude(QByteArray sourceCode) const
{
    QByteArray includes;
    for (QString key : m_autoIncludes.keys()) {
        if (sourceCode.contains(key.toUtf8()))
            includes += "#include \"" + m_autoIncludes[key] + "\"\n";
    }
    return includes;
}
