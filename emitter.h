#ifndef EMITTER_H
#define EMITTER_H

#include <QVector3D>
#include <QObject>

class Emitter : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qreal rate READ rate WRITE setRate NOTIFY changed)
    Q_PROPERTY(qreal density READ density WRITE setDensity NOTIFY changed)
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY changed)
    Q_PROPERTY(QVector3D position READ position WRITE setPosition NOTIFY changed)
    Q_PROPERTY(QVector3D velocity READ velocity WRITE setVelocity NOTIFY changed)

    qreal m_rate = 10.0f;
    qreal m_density = 1.0f;
    bool m_enabled = true;
    QVector3D m_position = { 0.5, 0.5, 0.5 };
    QVector3D m_velocity = { 0.0, 0.0, -0.001 };

    qreal m_keptBack = 0.0f;


public:
    qreal rate() const { return m_rate; }
    qreal density() const { return m_density; }
    bool isEnabled() const { return m_enabled; }
    QVector3D position() const { return m_position; }
    QVector3D velocity() const { return m_velocity; }

    int emitCount(float dt); // Returns the number of particles to be emitted in this frame. The number might change if the rate is not an integer.


public slots:
    void setRate(qreal arg) {
        if (m_rate != arg) {
            m_rate = arg;
            emit changed();
        }
    }
    void setDensity(qreal arg) {
        if (m_density != arg) {
            m_density = arg;
            emit changed();
        }
    }
    void setEnabled(bool arg) {
        if (m_enabled != arg) {
            m_enabled = arg;
            emit changed();
        }
    }
    void setPosition(QVector3D arg) {
        if (m_position != arg) {
            m_position = arg;
            emit changed();
        }
    }
    void setVelocity(QVector3D arg) {
        if (m_velocity != arg) {
            m_velocity = arg;
            emit changed();
        }
    }


signals:
    void changed();
};

#endif // EMITTER_H
