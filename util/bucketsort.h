#ifndef BUCKETSORT_H
#define BUCKETSORT_H

#include <qclkernel.h>
#include <qclprogram.h>
#include "scan.h"
#include "rotate.h"

class CLContext;
class CLEventTree;

/*
 * The expressions have to describe the expression in a return statement of a function like:
 *
 *   - uint keyExpression(T_in element)
 *   - T_out outputExpression(T_in element, uint index)
 *
 * where 'index' in the outputExpression is the source (original) index of the element.
 * The returned element will be stored at the target (new) index.
 */
class BucketSort
{
public:
    BucketSort(CLContext &context,
               uint numBuckets,
               const QString &inputTypeName = "int", size_t inputTypeSize = sizeof(cl_int),
               const QString &outputTypeName = "int", size_t outputTypeSize = sizeof(cl_int),
               const QString &keyExpression = "element",
               const QString &outputExpression = "element",
               uint maxGroupSize = 256);

    uint numBuckets() const { return m_numBuckets; }
    QString outputExpression() const { return m_outputExpression; }
    QString keyExpression() const { return m_keyExpression; }
    uint maxGroupSize() const { return m_maxGroupSize; }

    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, uint numElements, const QCLBuffer &bucketStarts = QCLBuffer());

    uint minBufferSize() const { return m_maxGroupSize; }
    uint minBufferSizeForNumElements(uint numElements) const;

    void allocateAuxiliaryBuffers(uint numElements);
    void freeAuxiliaryBuffers();

public:
    CLContext &m_context;
    QCLProgram m_program;
    QCLKernel m_kernelInitialStep;
    QCLKernel m_kernelFinalStep;
    const cl_uint m_numBuckets;
    const size_t m_inputTypeSize;
    const size_t m_outputTypeSize;
    QString m_outputExpression;
    QString m_keyExpression;
    const uint m_maxGroupSize;

    // auxiliary:
    uint m_preparedForNumElements;
    cl_uint m_intermediatePitch; // The pitch between the buckets in the following buffer
    QCLBuffer m_intermediateBuffer; // For every bucket: the number of elements per group going in this bucket (indexing: [bucket * m_intermediatePitch + group])
    QCLBuffer m_intermediateTransposed;  // The same, but transposed layout for coalesced access in the final step

    // other algos used:
    Scan m_scanBuckets;
    Rotate m_transposeIntermediate;

    // steps:
    CLEventTree initialStep(const QCLBuffer &input, int numElements);
    CLEventTree scanBucketsStep();
    CLEventTree copyBucketStarts(const QCLBuffer &bucketStarts);
    CLEventTree transposeIntermediateStep();
    CLEventTree finalStep(const QCLBuffer &input, const QCLBuffer &output, int numElements);
};

#endif // BUCKETSORT_H
