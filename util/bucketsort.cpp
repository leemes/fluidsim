#include "bucketsort.h"
#include "clcontext.h"
#include "cleventtree.h"
#include "util.h"
#include "debug.h"


BucketSort::BucketSort(CLContext &context,
                       uint numBuckets,
                       const QString &inputTypeName, size_t inputTypeSize,
                       const QString &outputTypeName, size_t outputTypeSize,
                       const QString &keyExpression, const QString &outputExpression,
                       uint maxGroupSize) :
    m_context(context),
    m_numBuckets(numBuckets),
    m_inputTypeSize(inputTypeSize),
    m_outputTypeSize(outputTypeSize),
    m_maxGroupSize(maxGroupSize),
    m_preparedForNumElements(0),
    m_scanBuckets(context, "uint", sizeof(cl_uint), Scan::Exclusive),
    m_transposeIntermediate(context, "uint", Rotate::Transpose)
{
    Q_ASSERT(maxGroupSize >= numBuckets);

    QByteArray defineExpressions;
    defineExpressions += "#define KEY(element) " + keyExpression.toLocal8Bit() + "\n";
    defineExpressions += "#define OUTPUT(element,index) " + outputExpression.toLocal8Bit() + "\n";

    QMap<QString,QString> defineConstants;
    defineConstants["NUM_BUCKETS"] = QString::number(numBuckets);
    defineConstants["GROUPSIZE"] = QString::number(m_maxGroupSize);
    defineConstants["T_IN"] = inputTypeName;
    defineConstants["T_OUT"] = outputTypeName;

    m_program = context.buildProgram("util/bucketsort.cl", defineExpressions, defineConstants);

    m_kernelInitialStep = m_program.createKernel("initialStep");
    if (m_kernelInitialStep.isNull())
        qFatal("Failed to create kernel for BucketSort.");
    m_kernelFinalStep = m_program.createKernel("finalStep");
    if (m_kernelFinalStep.isNull())
        qFatal("Failed to create kernel for BucketSort.");

    m_kernelInitialStep.setLocalWorkSize(m_kernelInitialStep.declaredWorkGroupSize());
    m_kernelFinalStep.setLocalWorkSize(m_kernelFinalStep.declaredWorkGroupSize());
}

CLEventTree BucketSort::operator ()(const QCLBuffer &input, const QCLBuffer &output, uint numElements, const QCLBuffer &bucketStarts)
{
    allocateAuxiliaryBuffers(numElements);

    Q_ASSERT(input.memoryId() != output.memoryId()); // No in-place operation supported!
    Q_ASSERT(input.access() == QCLBuffer::ReadOnly || input.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(input.size() >= m_inputTypeSize * minBufferSizeForNumElements(numElements));
    Q_ASSERT(output.access() == QCLBuffer::WriteOnly || output.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(output.size() >= m_outputTypeSize * minBufferSizeForNumElements(numElements));
    Q_ASSERT(bucketStarts.isNull() || bucketStarts.access() == QCLBuffer::WriteOnly || bucketStarts.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(bucketStarts.isNull() || bucketStarts.size() >= sizeof(cl_uint) * m_numBuckets);

    CLEventTree events("BucketSort");

    //if (numBuckets() == 4 && maxGroupSize() == 64)
    //   debug<uint>(const_cast<QCLBuffer&>(input), numElements);

    events << initialStep(input, numElements);
    //if (numBuckets() == 4 && maxGroupSize() == 64)
    //    debug<uint>(m_intermediateBuffer, m_intermediatePitch*m_numBuckets);

    events << scanBucketsStep();
    //if (numBuckets() == 4 && maxGroupSize() == 64)
    //    debug<uint>(m_intermediateBuffer, m_intermediatePitch*m_numBuckets);

    events << transposeIntermediateStep();

    if (!bucketStarts.isNull()) {
        events << copyBucketStarts(bucketStarts);
    }

    events << finalStep(input, output, numElements);
    //if (numBuckets() == 4 && maxGroupSize() == 64)
    //    debug<uint>(const_cast<QCLBuffer&>(output), numElements);

    return events;
}

uint BucketSort::minBufferSizeForNumElements(uint numElements) const
{
    return numElements ? qMax(minBufferSize(), roundUpPowerOfTwo(numElements)) : minBufferSize();
}

void BucketSort::allocateAuxiliaryBuffers(uint numElements)
{
    if(m_preparedForNumElements == minBufferSizeForNumElements(numElements))
        return;

    freeAuxiliaryBuffers();

    m_preparedForNumElements = minBufferSizeForNumElements(numElements);

    // The number of elements in the intermediate arrays equals the number of groups for the first and last step.
    Q_ASSERT(m_preparedForNumElements / m_maxGroupSize > 0);
    m_intermediatePitch = m_preparedForNumElements / m_maxGroupSize;

    // The total intermediate buffer has to be large enough for the scan algorithm to operate on it.
    const uint intermediateSize = m_scanBuckets.minBufferSizeForNumElements(m_intermediatePitch * m_numBuckets);

    // Allocate the intermediate buffer
    m_intermediateBuffer     = m_context.createBufferDevice(sizeof(cl_uint) * intermediateSize, QCLMemoryObject::ReadWrite);
    m_intermediateTransposed = m_context.createBufferDevice(sizeof(cl_uint) * intermediateSize, QCLMemoryObject::ReadWrite);

    // Nested algorithms:
    m_scanBuckets.allocateAuxiliaryBuffers(m_intermediatePitch * m_numBuckets);
}

void BucketSort::freeAuxiliaryBuffers()
{
    if (m_preparedForNumElements) {
        m_intermediateBuffer = QCLBuffer();
        m_preparedForNumElements = 0;
    }
}

CLEventTree BucketSort::initialStep(const QCLBuffer &input, int numElements)
{
    m_kernelInitialStep.setGlobalWorkSize(m_intermediatePitch * m_maxGroupSize);
    return CLEventTree(m_kernelInitialStep(input, m_intermediateBuffer, numElements, m_intermediatePitch), "initialStep");
}

CLEventTree BucketSort::scanBucketsStep()
{
    return CLEventTree(m_scanBuckets(m_intermediateBuffer, m_intermediateBuffer, m_intermediatePitch * m_numBuckets), "scanBucketsStep");
}

CLEventTree BucketSort::copyBucketStarts(const QCLBuffer &bucketStarts)
{
    // The intermediate buffer is laid out bucket by bucket (bucket = row).
    // Its first column are the bucket starts. So we simply copy the first column.
    // TODO: Is this available in OpenCL 1.0 or do we need a kernel for this?
    return CLEventTree(m_intermediateTransposed.copyToAsync(0, m_numBuckets * sizeof(cl_uint), bucketStarts, 0), "copyBucketStarts");
}

CLEventTree BucketSort::transposeIntermediateStep()
{
    return CLEventTree(m_transposeIntermediate(m_intermediateBuffer, m_intermediateTransposed, QSize(m_intermediatePitch, m_numBuckets)), "transposeIntermediateStep");
}

CLEventTree BucketSort::finalStep(const QCLBuffer &input, const QCLBuffer &output, int numElements)
{
    m_kernelFinalStep.setGlobalWorkSize(m_intermediatePitch * m_maxGroupSize);
   // QCLBuffer debug1 = m_context.createBufferDevice(m_numBuckets * maxGroupSize() * 4, QCLMemoryObject::ReadWrite);
   // QCLBuffer debug2 = m_context.createBufferDevice(maxGroupSize() * 4, QCLMemoryObject::ReadWrite);
    CLEventTree e(m_kernelFinalStep(input, output, m_intermediateTransposed, numElements), "finalStep");
   // debug<cl_uint,32,11>(debug1);
   // debug<cl_uint,32,11>(debug2);
    return e;
}
