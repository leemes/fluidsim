#ifndef STREAMCOMPACTION_H
#define STREAMCOMPACTION_H

#include <qclkernel.h>
#include "fill.h"
#include "transform.h"
#include "scan.h"
#include "move.h"


class QCLBuffer;
class CLContext;
class CLEventTree;

class StreamCompaction
{
public:
    StreamCompaction(CLContext &context,
                     const QString &inputTypeName = "int", size_t inputTypeSize = sizeof(cl_int),
                     const QString &outputTypeName = "int", size_t outputTypeSize = sizeof(cl_int),
                     uint predicateTransformStride = 1, const QString &predicateTransformExpression = "element > 0",
                     const QString &outputExpression = "element",
                     const QString &fillExpression = "-1",
                     uint maxGroupSize = 256);

    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, uint numElements);

    void allocateAuxiliaryBuffers(uint numElements);
    void freeAuxiliaryBuffers();

private:
    CLContext &m_context;
    const uint m_inputTypeSize;
    const uint m_outputTypeSize;

    // aux:
    uint m_preparedForNumElements;
    QCLBuffer m_predicateAuxBuffer;

    // other algos:
    Fill m_fill;
    Transform m_transform;
    Scan m_scan;
    Move m_move;
};

#endif // STREAMCOMPACTION_H
