#ifndef MULTILOWERBOUND_H
#define MULTILOWERBOUND_H

#include <qclkernel.h>
#include <qclprogram.h>

class CLContext;
class CLEventTree;

class MultiLowerBound
{
public:
    MultiLowerBound(CLContext &context,
                    const QString &typeName,
                    const QString &keyExpression,
                    uint numSearchesPerRange = 256);

    // "numSearchesPerRange" equals the local work group size.
    uint numSearchesPerRange() const { return m_numSearchesPerRange; }

    CLEventTree operator()(const QCLBuffer &input, uint numElements,
                           const QCLBuffer &rangeStarts, uint numRanges,
                           const QCLBuffer &lowerBoundsOutput);

private:
    QCLProgram m_program;
    QCLKernel m_kernel;
    const uint m_numSearchesPerRange;
};

#endif // MULTILOWERBOUND_H
