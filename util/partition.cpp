#include "partition.h"
#include "clcontext.h"
#include "cleventtree.h"
#include <qclprogram.h>
#include "debug.h"


Partition::Partition(CLContext &context, const QString &typeName, size_t typeSize,
                     const QString &keyExpression, uint numBits,
                     uint maxBitsPerBucketSortLevel) :
    m_numBits(numBits),
#ifdef USE_MULTI_LOWER_BOUND
    m_maxBitsPerBucketSortLevel(maxBitsPerBucketSortLevel),
    m_maxBitsPerLowerBoundLevel(9),
#endif
    m_radixSort(context, typeName, typeSize, calculateRadixSortSteps(keyExpression, numBits, maxBitsPerBucketSortLevel)),
#ifdef USE_MULTI_LOWER_BOUND
    m_lastBucketSortNumBuckets(1 << m_radixSort.steps().first().keyNumBits)
#else
    m_scanPartitions(context, "uint", sizeof(cl_uint), Scan::Exclusive)
#endif
{

#ifdef USE_MULTI_LOWER_BOUND
    // The most significant radix sort step can return a bucket starts array. So we start with setting the remaining number of bits accordingly.
    uint currentBits = numBits - m_radixSort.steps().first().keyNumBits;

    // Continue with the second-most significant radix sort step and execute "multi lower bounds" to extend the bucket starts array.
    while (currentBits)
    {
        // Add aux buffer which has 2^(processed bits) size for bucket starts, where "processed bits" is the number of
        // bits processed BEFORE this lower bound, i.e. the ALREADY known bucket starts until now
      //  qDebug("Allocated 2^%d", (numBits - currentBits));
        m_auxiliaryBuffer << context.createBufferDevice(sizeof(cl_uint) * (1 << (numBits - currentBits)), QCLMemoryObject::ReadWrite);

        // Add step to "expand" the aux buffer we just created to a multiple of 2^levelNumBits.
        uint levelNumBits = qMin(currentBits, m_maxBitsPerLowerBoundLevel);
        uint remainingBits = currentBits - levelNumBits;
        QString subKeyExpression = calculateSubKeyExpression(keyExpression, remainingBits, levelNumBits);
        m_multiLowerBounds.emplace_back(context, typeName, subKeyExpression, 1 << levelNumBits);

        // Advance the current bits
        currentBits = remainingBits;
    }
#else
    QByteArray defineExpressions;
    defineExpressions += "#define KEY(element) " + keyExpression.toLocal8Bit() + "\n";

    QMap<QString,QString> defineConstants;
    defineConstants["T"] = typeName;

    QCLProgram program = context.buildProgram("util/partition.cl", defineExpressions, defineConstants);

    m_kernelClearPartitions = program.createKernel("clearPartitions");
    if (m_kernelClearPartitions.isNull())
        qFatal("Failed to create kernel for Partition.");

    m_kernelFindPartitionEnds = program.createKernel("findPartitionEnds");
    if (m_kernelFindPartitionEnds.isNull())
        qFatal("Failed to create kernel for Partition.");

    m_kernelSubtractPartitionStarts = program.createKernel("subtractPartitionStarts");
    if (m_kernelSubtractPartitionStarts.isNull())
        qFatal("Failed to create kernel for Partition.");

    m_kernelClearPartitions.setLocalWorkSize(256);
    m_kernelFindPartitionEnds.setLocalWorkSize(256);
    m_kernelSubtractPartitionStarts.setLocalWorkSize(256);
#endif
}

Partition::~Partition()
{
}

CLEventTree Partition::operator ()(QCLBuffer &input, QCLBuffer &output, uint numElements, QCLBuffer &partitionStartsOutput)
{
    Q_ASSERT(input.access() == QCLBuffer::ReadOnly || input.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(output.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(partitionStartsOutput.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(partitionStartsOutput.size() >= sizeof(cl_uint) * ((1U << m_numBits) + 1));

    CLEventTree events("Partition");

    // Sort elements
    events << sortStep(input, output, numElements);

#ifdef USE_MULTI_LOWER_BOUND
    events << expandPartitionStartsStep(output, numElements, partitionStartsOutput);
#else
    // Compute partition starts in 4 steps
    events << clearPartitionsStep(partitionStartsOutput);
    events << findPartitionEndsStep(output, numElements, partitionStartsOutput);
    events << subtractPartitionStartsStep(output, numElements, partitionStartsOutput);
    events << scanPartitionSizesStep(partitionStartsOutput);
#endif

    // Write the numElements at one past the end of the partitionStartsOutput
    events << CLEventTree(partitionStartsOutput.writeAsync(sizeof(cl_uint) << m_numBits, &numElements, sizeof(cl_uint)), "writeGlobalEndStep");

    return events;
}

const QCLBuffer &Partition::lastMoveTable() const
{
    return m_radixSort.lastMoveTable();
}

QVector<RadixSort::Step> Partition::calculateRadixSortSteps(const QString &keyExpression, uint numBits, uint maxBitsPerLevel)
{
    QVector<RadixSort::Step> steps;
    uint currentBits = numBits;
    while (currentBits) {
        uint levelNumBits = qMin(currentBits, maxBitsPerLevel);
        uint remainingBits = currentBits - levelNumBits;
        QString subKeyExpression = calculateSubKeyExpression(keyExpression, remainingBits, levelNumBits);
        // Add step
        steps << RadixSort::Step{ subKeyExpression, (int)levelNumBits };
        currentBits = remainingBits;
    }
    return steps;
}

QString Partition::calculateSubKeyExpression(const QString &keyExpression, uint bitOffsetSubKey, uint numBitsSubKey)
{
    // Take the original key
    QString expression = keyExpression;
    // Maybe shift to the right
    if (bitOffsetSubKey) {
        expression = "(" + expression + ") >> " + QString::number(bitOffsetSubKey);
    }
    // Apply mask
    uint mask = (1 << numBitsSubKey) - 1;
    expression = "(" + expression + ") & 0x" + QString::number(mask, 16);
    return expression;
}

CLEventTree Partition::sortStep(QCLBuffer &input, QCLBuffer &output, uint numElements)
{
#ifdef USE_MULTI_LOWER_BOUND
    return CLEventTree(m_radixSort(input, output, numElements, m_auxiliaryBuffer[0]), "sortStep");
#else
    return CLEventTree(m_radixSort(input, output, numElements), "sortStep");
#endif
}

#ifdef USE_MULTI_LOWER_BOUND
CLEventTree Partition::expandPartitionStartsStep(QCLBuffer &sorted, uint numElements, QCLBuffer &partitionStartsOutput)
{
    CLEventTree events("expandPartitionStartsStep");

    // In the beginning, the number of known range starts equals the number of buckets of the last bucket sort (execution order, i.e. the most significant one)
    uint numRanges = m_lastBucketSortNumBuckets;

    for (uint i = 0; i < m_multiLowerBounds.size(); ++i)
    {
        MultiLowerBound & multiLowerBound = m_multiLowerBounds[i];
        bool isLast = (i == m_multiLowerBounds.size() - 1);

        // The last step should use the "partitionStartsOutput" buffer as its output; all others should use the aux buffer of the next step.
        const QCLBuffer *currentRangeStarts = &m_auxiliaryBuffer[i];
        const QCLBuffer *currentLowerBoundsOutput = isLast ? &partitionStartsOutput : &m_auxiliaryBuffer[i+1];

      //  std::cout << "IN" << std::endl;
      //  debug<cl_uint>(*const_cast<QCLBuffer*>(currentRangeStarts));

        events << multiLowerBound(sorted, numElements, *currentRangeStarts, numRanges, *currentLowerBoundsOutput);

      //  std::cout << "OUT" << std::endl;
      //  debug<cl_uint>(*const_cast<QCLBuffer*>(currentLowerBoundsOutput));

        // We just multiplied the number of ranges by "numSearchesPerRange", so account this for the next iteration
        numRanges *= multiLowerBound.numSearchesPerRange();
    }

    return events;
}
#else
CLEventTree Partition::clearPartitionsStep(QCLBuffer &partitions)
{
    m_kernelClearPartitions.setRoundedGlobalWorkSize(1U << m_numBits);
    return CLEventTree(m_kernelClearPartitions(partitions, 1U << m_numBits), "clearPartitionsStep");
}

CLEventTree Partition::findPartitionEndsStep(QCLBuffer &sorted, uint numElements, QCLBuffer &partitions)
{
    m_kernelFindPartitionEnds.setRoundedGlobalWorkSize(numElements);
    return CLEventTree(m_kernelFindPartitionEnds(sorted, numElements, partitions), "findPartitionEndsStep");
}

CLEventTree Partition::subtractPartitionStartsStep(QCLBuffer &sorted, uint numElements, QCLBuffer &partitions)
{
    m_kernelSubtractPartitionStarts.setRoundedGlobalWorkSize(numElements);
    return CLEventTree(m_kernelSubtractPartitionStarts(sorted, numElements, partitions), "subtractPartitionStartsStep");
}

CLEventTree Partition::scanPartitionSizesStep(QCLBuffer &partitions)
{
    return CLEventTree(m_scanPartitions(partitions, partitions, 1U << m_numBits), "scanPartitionSizesStep");
}
#endif
