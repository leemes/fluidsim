#include "scantiny.h"
#include "clcontext.h"
#include <qclprogram.h>

ScanTiny::ScanTiny(CLContext &context, int numElements, Mode mode) :
    m_numElements(numElements),
    m_mode(mode)
{
    Q_ASSERT(numElements % 2 == 0);

    QMap<QString, QString> defines;
    defines["T"] = "uint";
    defines["W"] = numElements % 16 == 0 ? "16" : numElements % 8 == 0 ? "8" : numElements % 4 == 0 ? "4" : "2";

    QCLProgram program = context.buildProgram("util/scantiny.cl", defines);

    QString kernelName = mode == Inclusive ? "inclusiveScanTiny" : "exclusiveScanTiny";
    m_kernel = program.createKernel(kernelName);
    if (m_kernel.isNull())
        qFatal("Failed to create kernel for ScanTiny.");

    m_kernel.setLocalWorkSize(1);
    m_kernel.setGlobalWorkSize(1);
}

QCLEventList ScanTiny::operator ()(const QCLBuffer &input, const QCLBuffer &output, uint valueOffset)
{
    return m_kernel(input, output, m_numElements, valueOffset);
}

