#include "fill.h"
#include "clcontext.h"
#include "cleventtree.h"


Fill::Fill(CLContext &context, const QString &typeName, const QString &outputExpression, uint maxGroupSize) :
    m_maxGroupSize(maxGroupSize)
{
    QByteArray defineExpressions;
    defineExpressions += "#define OUTPUT(element,index) " + outputExpression.toLocal8Bit() + "\n";

    QMap<QString,QString> defineConstants;
    defineConstants["GROUPSIZE"] = QString::number(m_maxGroupSize);
    defineConstants["T"] = typeName;

    m_program = context.buildProgram("util/fill.cl", defineExpressions, defineConstants);

    m_kernel = m_program.createKernel("fill");
    if (m_kernel.isNull())
        qFatal("Failed to create kernel for Fill.");

    m_kernel.setLocalWorkSize(m_kernel.declaredWorkGroupSize());
}

CLEventTree Fill::operator ()(const QCLBuffer &array, uint numElements)
{
    m_kernel.setRoundedGlobalWorkSize(numElements);
    return CLEventTree(m_kernel(array, numElements), "Fill");
}
