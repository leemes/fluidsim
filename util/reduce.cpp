#include "reduce.h"
#include <qclcontext.h>
#include <qclprogram.h>
#include <QStringList>
#include "clsourceloader.h"
#include "util.h"
#include "debug.h"


Reduce::Reduce(QCLContext &context, const CLSourceLoader &sourceLoader,
               const char *typeName, size_t typeSize,
               const char *reduceExpression, const char *neutralElement,
               uint maxGroupSize) :
    m_context(context),
    m_typeSize(typeSize),
    m_expression(reduceExpression),
    m_maxGroupSize(maxGroupSize),
    m_preparedForNumElements(0),
    m_numLevels(0),
    m_levels(nullptr)
{
    QByteArray defineExpression = "#define REDUCE(a,b) " + QByteArray(reduceExpression) + "\n";
    QCLProgram program = context.createProgramFromSourceCode(sourceLoader.loadSource("util/reduce.cl").prepend(defineExpression));
    QStringList options;
    options << "-DT=" + QString(typeName);
    options << "-DNEUTRAL_ELEMENT=" + QString(neutralElement);
    if(!program.build(options.join(" ")))
        qFatal("Failed to build program for Reduce.");

    m_kernel = program.createKernel("reduce");
    if (m_kernel.isNull())
        qFatal("Failed to create kernel for Reduce.");

    m_itemWidth = 16; // FIXME: Can be dynamic ("-DW=...")
    m_maxGroupWidth = m_maxGroupSize * m_itemWidth;
}

Reduce::~Reduce()
{
    freeAuxiliaryBuffers();
}

QCLEventList Reduce::operator ()(const QCLBuffer &input, const QCLBuffer &output, uint numElements)
{
    allocateAuxiliaryBuffers(numElements);

    Q_ASSERT(input.access() == QCLBuffer::ReadOnly || input.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(output.access() == QCLBuffer::WriteOnly || output.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(input.size() >= m_typeSize * minBufferSizeForNumElements(numElements));
    Q_ASSERT(output.size() >= m_typeSize * 1);

    QCLEventList events;

    // Set the "special" buffers in the first and last step
    m_levels[0].in = &input;
    m_levels[m_numLevels-1].out = &output;

    uint currentNumElements = numElements;
    for (uint l = 0; l < m_numLevels; ++l) {
        const Level &level = m_levels[l];
        //std::cout << "IN:" << std::endl;
        //debug<int,32,5>(*const_cast<QCLBuffer*>(level.in));
        events << step(*level.in, *level.out, currentNumElements, level.groupSize, level.numGroups);
        //std::cout << "OUT:" << std::endl;
        //debug<int,32,5>(*const_cast<QCLBuffer*>(level.out));
        currentNumElements = level.numGroups;
    }

    return events;
}

uint Reduce::minBufferSizeForNumElements(uint numElements) const
{
    return numElements ? qMax(minBufferSize(), roundUpPowerOfTwo(numElements)) : minBufferSize();
}

void Reduce::allocateAuxiliaryBuffers(uint numElements)
{
    if(m_preparedForNumElements == minBufferSizeForNumElements(numElements))
        return;

    freeAuxiliaryBuffers();

    m_preparedForNumElements = minBufferSizeForNumElements(numElements);

    // Compute number of levels
    uint current = m_preparedForNumElements;
    m_numLevels = 0;
    while (current > 1) {
        current /= m_maxGroupWidth;
        ++m_numLevels;
    }
    //qDebug("%d levels for %d elements", m_numLevels, m_preparedForNumElements);
    m_levels = new Level[m_numLevels];

    // Compute reduction widths per level.
    // Try to reduce m_maxGroupWidth items, but avoid too small last levels,
    // i.e. make sure that all level output sizes are >= m_itemWidth.
    current = m_preparedForNumElements;
    for (uint l = 0; l < m_numLevels; ++l) {
        uint groupSize = m_maxGroupSize;
        uint width = groupSize * m_itemWidth;
        uint outputSize = current / width;
        if (outputSize < m_itemWidth && outputSize != 1) {
            // The next level would become too small. To avoid this, don't reduce that much.
            // Case 1: This is the last level, i.e. reduce till the end.
            // Case 2: This is the level before the last => reduce to outputSize = m_itemWidth.
            if (l == m_numLevels - 1) {
                outputSize = 1;
            } else {
                outputSize = m_itemWidth;
            }
            // Invert the formulas.
            width = current / outputSize;
            groupSize = width / m_itemWidth;
        }

        Q_ASSERT(groupSize > 0);
        Q_ASSERT(width > 0);
        Q_ASSERT(outputSize >= m_itemWidth || outputSize == 1);
        Q_ASSERT(outputSize == 1 || l != m_numLevels - 1);

        // "in" for the first and "out" for the last level are the global in and out buffers -- set them later.
        // For all other arrays, the aux buffer is used for "out", the previous aux buffer for "in".
        bool last = outputSize == 1;
        const QCLBuffer *in  =  l      ? &m_levels[l-1].auxiliaryBuffer : 0;
        const QCLBuffer *out = (!last) ? &m_levels[l  ].auxiliaryBuffer : 0;
        m_levels[l] = Level{
                (!last) ? m_context.createBufferDevice(m_typeSize * outputSize, QCLBuffer::ReadWrite) : QCLBuffer(),
                in, out,
                groupSize,
                outputSize
            };
        //qDebug("level %d: %d -> %d (using %d groups of size %d)", l, current, m_levels[l].numGroups, m_levels[l].numGroups, m_levels[l].groupSize);
        current = outputSize;
    }
    Q_ASSERT(current == 1);
}

void Reduce::freeAuxiliaryBuffers()
{
    if (m_preparedForNumElements) {
        delete[] m_levels;
        m_levels = nullptr;
        m_preparedForNumElements = 0;
    }
}

QCLEventList Reduce::step(const QCLBuffer &input, const QCLBuffer &output, uint numElements, uint groupSize, uint numGroups)
{
    m_kernel.setLocalWorkSize(groupSize);
    m_kernel.setGlobalWorkSize(groupSize * numGroups);

    // Allocate local memory (last argument) -- FIXME: How can this be done in QtOpenCL?
    uint localMemSize = groupSize * 33 / 32; // avoids bank-conflicts
    clSetKernelArg(m_kernel.kernelId(), 3, m_typeSize * localMemSize, nullptr);

    return m_kernel(input, output, numElements);
}
