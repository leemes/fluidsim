#include "transform.h"
#include "clcontext.h"
#include "cleventtree.h"
#include <qclkernel.h>


Transform::Transform(CLContext &context, const QString &inputTypeName, size_t inputTypeSize, const QString &outputTypeName, size_t outputTypeSize, uint inputStride, const QString &outputExpression, uint maxGroupSize) :
    m_maxGroupSize(maxGroupSize),
    m_inputTypeSize(inputTypeSize),
    m_outputTypeSize(outputTypeSize),
    m_inputStride(inputStride)
{
    QByteArray defineExpressions;
    defineExpressions += "#define OUTPUT(input, index, numElements) " + outputExpression.toLocal8Bit() + "\n";

    QMap<QString,QString> defineConstants;
    defineConstants["GROUPSIZE"] = QString::number(m_maxGroupSize);
    defineConstants["INPUT_STRIDE"] = QString::number(m_inputStride);
    defineConstants["T_IN"] = inputTypeName;
    defineConstants["T_OUT"] = outputTypeName;

    m_program = context.buildProgram("util/transform.cl", defineExpressions, defineConstants);

    m_kernel = m_program.createKernel("transform");
    if (m_kernel.isNull())
        qFatal("Failed to create kernel for Transform.");

    m_kernel.setLocalWorkSize(m_kernel.declaredWorkGroupSize());
}

CLEventTree Transform::operator ()(const QCLBuffer &input, const QCLBuffer &output, uint numElements)
{
    Q_ASSERT((m_inputTypeSize * m_inputStride) == m_outputTypeSize || input.memoryId() != output.memoryId()); // No in-place operation supported (unless the type sizes match)!
    Q_ASSERT(input.access() == QCLBuffer::ReadOnly || input.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(input.size() >= m_inputTypeSize * numElements * m_inputStride);
    Q_ASSERT(output.access() == QCLBuffer::WriteOnly || output.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(output.size() >= m_outputTypeSize * numElements);

    m_kernel.setRoundedGlobalWorkSize(numElements);
    return CLEventTree(m_kernel(input, output, numElements), "Transform");
}

