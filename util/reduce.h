#ifndef REDUCE_H
#define REDUCE_H

#include <qclkernel.h>

class QCLBuffer;
class QCLContext;
class CLSourceLoader;

// Standard reduce expressions
constexpr const char *ReduceExprAdd = "a + b";
constexpr const char *ReduceExprMul = "a * b";
constexpr const char *ReduceExprMin = "min(a, b)";
constexpr const char *ReduceExprMax = "max(a, b)";

constexpr const char *NeutralElementAdd = "0";
constexpr const char *NeutralElementMul = "1";
constexpr const char *NeutralElementMin = "T_MAX";
constexpr const char *NeutralElementMax = "T_MIN";

class Reduce
{
public:
    Reduce(QCLContext &context, const CLSourceLoader &sourceLoader,
           const char *typeName = "uint", size_t typeSize = sizeof(cl_uint),
           const char *reduceExpression = ReduceExprAdd,
           const char *neutralElement = NeutralElementAdd,
           uint maxGroupSize = 128);
    ~Reduce();

    const char *reduceExpression() const { return m_expression; }
    uint maxGroupSize() const { return m_maxGroupSize; }

    QCLEventList operator()(const QCLBuffer &input, const QCLBuffer &output, uint numElements);

    uint minBufferSize() const { return m_itemWidth; }
    uint minBufferSizeForNumElements(uint numElements) const;

    void allocateAuxiliaryBuffers(uint numElements);
    void freeAuxiliaryBuffers();

private:
    QCLContext &m_context;
    QCLKernel m_kernel;
    const size_t m_typeSize;
    const char *m_expression;
    const uint m_maxGroupSize;
    uint m_itemWidth;
    uint m_maxGroupWidth;

    // auxiliary:
    uint m_preparedForNumElements;
    uint m_numLevels;
    struct Level {
        QCLBuffer auxiliaryBuffer;
        const QCLBuffer *in, *out;
        uint groupSize;
        uint numGroups;
    };
    Level *m_levels;

    // steps:
    QCLEventList step(const QCLBuffer &input, const QCLBuffer &output, uint numElements, uint groupSize, uint numGroups);
};

#endif // REDUCE_H
