#ifndef SCAN_H
#define SCAN_H

#include <qclkernel.h>
#include <qclprogram.h>
#include <memory>

class QCLBuffer;
class CLContext;
class CLEventTree;

class Scan
{
public:
    enum Mode {
        Exclusive,
        Inclusive
    };

    Scan(CLContext &context,
         const char *typeName = "uint", size_t typeSize = sizeof(cl_uint),
         Mode mode = Inclusive,
         uint maxGroupSize = 256);

    Mode mode() const { return m_mode; }
    uint maxGroupSize() const { return m_maxGroupSize; }

    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, uint numElements);

    uint minBufferSize() const { return m_itemWidth; }
    uint minBufferSizeForNumElements(uint numElements) const;

    void allocateAuxiliaryBuffers(uint numElements);
    void freeAuxiliaryBuffers();

private:
    CLContext &m_context;
    QCLProgram m_program;
    QCLKernel m_kernelScan;
    QCLKernel m_kernelAdd;
    Mode m_mode;
    const uint m_maxGroupSize;
    size_t m_typeSize;
    uint m_itemWidth;
    uint m_maxGroupWidth;

    // auxiliary:
    uint m_preparedForNumElements;
    uint m_numLevels;
    struct Level {
        QCLBuffer auxiliaryBuffer;
        const QCLBuffer *in, *out;
        uint groupSize;
        uint numGroups;
    };
    std::unique_ptr<Level[]> m_levels;

    // steps:
    CLEventTree scanStep(const QCLBuffer &input, const QCLBuffer &output, const QCLBuffer &higherLevel, uint groupSize, uint numGroups);
    CLEventTree addStep(const QCLBuffer &array, const QCLBuffer &higherLevel, uint groupSize, uint numGroups);

    Q_DISABLE_COPY(Scan)
};

#endif // SCAN_H
