#ifndef ROTATE_H
#define ROTATE_H

#include <qclkernel.h>
#include <qclprogram.h>
#include "cleventtree.h"

class CLContext;

class Rotate
{
public:
    enum Mode {
        RotateCW,       // rotate clockwise
        RotateCCW,      // rotate counter-clockwise
        Transpose       // don't rotate but transpose X and Y coordinates
    };

    Rotate(CLContext &context,
           const QString &typeName, Mode mode = RotateCW,
           QSize maxGroupSize = QSize(16, 16));

    Mode mode() const { return m_mode; }
    QSize maxGroupSize() const { return m_maxGroupSize; }

    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, QSize size);
    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, cl_uint2 size);
    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, uint width, uint height);

private:
    QCLProgram m_program;
    QCLKernel m_kernel;
    const Mode m_mode;
    const QSize m_maxGroupSize;
};

inline CLEventTree Rotate::operator()(const QCLBuffer &input, const QCLBuffer &output, cl_uint2 size) {
    return (*this)(input, output, QSize(size.s[0], size.s[1]));
}
inline CLEventTree Rotate::operator()(const QCLBuffer &input, const QCLBuffer &output, uint width, uint height) {
    return (*this)(input, output, QSize(width, height));
}

#endif // ROTATE_H
