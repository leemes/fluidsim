#include "move.h"
#include "clcontext.h"
#include "cleventtree.h"


Move::Move(CLContext &context, const QString &inputTypeName, const QString &outputTypeName, const QString &outputExpression, Move::Mode mode, bool onlyIfLeftDiffers, uint maxGroupSize) :
    m_mode(mode),
    m_maxGroupSize(maxGroupSize)
{
    QByteArray defineExpressions;
    defineExpressions += "#define OUTPUT(element,index) " + outputExpression.toLocal8Bit() + "\n";

    QMap<QString,QString> defineConstants;
    defineConstants["GROUPSIZE"] = QString::number(m_maxGroupSize);
    defineConstants["T_IN"] = inputTypeName;
    defineConstants["T_OUT"] = outputTypeName;
    defineConstants["MODE"] = (mode == TableOldNew) ? "MODE_OLDNEW" : "MODE_NEWOLD";
    if (onlyIfLeftDiffers) defineConstants["ONLY_IF_LEFT_DIFFERS"] = "1";

    m_program = context.buildProgram("util/move.cl", defineExpressions, defineConstants);

    m_kernel = m_program.createKernel("move");
    if (m_kernel.isNull())
        qFatal("Failed to create kernel for Move.");

    m_kernel.setLocalWorkSize(m_maxGroupSize);
}

CLEventTree Move::operator ()(const QCLBuffer &input, const QCLBuffer &output, uint numElements, const QCLBuffer &moveTable, int oldIndexDelta, int newIndexDelta)
{
    m_kernel.setRoundedGlobalWorkSize(numElements);
    return CLEventTree(m_kernel(input, output, numElements, moveTable, oldIndexDelta, newIndexDelta), "Move");
}
