#include "multilowerbound.h"
#include "clcontext.h"
#include "cleventtree.h"


MultiLowerBound::MultiLowerBound(CLContext &context, const QString &typeName, const QString &keyExpression, uint numSearchesPerRange) :
    m_numSearchesPerRange(numSearchesPerRange)
{
    QByteArray defineExpressions;
    defineExpressions += "#define KEY(element) " + keyExpression.toLocal8Bit() + "\n";

    QMap<QString,QString> defineConstants;
    defineConstants["T"] = typeName;
    defineConstants["GROUPSIZE"] = QString::number(numSearchesPerRange);

    m_program = context.buildProgram("util/multilowerbound.cl", defineExpressions, defineConstants);

    m_kernel = m_program.createKernel("multiLowerBound");
    if (m_kernel.isNull())
        qFatal("Failed to create kernel for MultiLowerBound.");

    m_kernel.setLocalWorkSize(numSearchesPerRange);
}

CLEventTree MultiLowerBound::operator ()(const QCLBuffer &input, uint numElements, const QCLBuffer &rangeStarts, uint numRanges, const QCLBuffer &lowerBoundsOutput)
{
    m_kernel.setRoundedGlobalWorkSize(numRanges * m_numSearchesPerRange);
    return CLEventTree(m_kernel(input, numElements, rangeStarts, numRanges, lowerBoundsOutput), "MultiLowerBound");
}
