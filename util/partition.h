#ifndef PARTITION_H
#define PARTITION_H

// Two different methods to compute partition starts were implemented.
// If USE_MULTI_LOWER_BOUND is enabled, a multi lower bound search extends the bucket starts found by the last bucket sort step to all partitions.
// Otherwise, the partition starts are computed by first finding the partition sizes and then scanning this result.
// Multi lower bound seems to be faster for medium sized input arrays (< 1 million), while scanning is faster for large arrays (> 1 million).
#define USE_MULTI_LOWER_BOUND


#include <qclkernel.h>
#include "radixsort.h"
#ifdef USE_MULTI_LOWER_BOUND
#include "multilowerbound.h"
#else
#include "scan.h"
#endif

class CLContext;
class CLEventTree;


class Partition
{
public:
    Partition(CLContext &context,
              const QString &typeName, size_t typeSize,
              const QString &keyExpression, uint numBits,
              uint maxBitsPerBucketSortLevel = 6);
    ~Partition();

    uint numBits() const { return m_numBits; }

    CLEventTree operator()(QCLBuffer &input, QCLBuffer &output,
                           uint numElements, QCLBuffer &partitionStartsOutput);
    const QCLBuffer &lastMoveTable() const;

    uint minBufferSize() const { return m_radixSort.minBufferSize(); }
    uint minBufferSizeForNumElements(uint numElements) const { return m_radixSort.minBufferSizeForNumElements(numElements); }

private:
    const uint m_numBits;
#ifdef USE_MULTI_LOWER_BOUND
    const uint m_maxBitsPerBucketSortLevel;
    const uint m_maxBitsPerLowerBoundLevel;
    QVector<QCLBuffer> m_auxiliaryBuffer;
#else
    QCLKernel m_kernelClearPartitions;
    QCLKernel m_kernelFindPartitionEnds;
    QCLKernel m_kernelSubtractPartitionStarts;
#endif

    // other algos:
    RadixSort m_radixSort;
#ifdef USE_MULTI_LOWER_BOUND
    std::vector<MultiLowerBound> m_multiLowerBounds;
    const uint m_lastBucketSortNumBuckets;
#else
    Scan m_scanPartitions;
#endif

    static QVector<RadixSort::Step> calculateRadixSortSteps(const QString &keyExpression, uint numBits, uint maxBitsPerLevel);
    static QString calculateSubKeyExpression(const QString &keyExpression, uint bitOffsetSubKey, uint numBitsSubKey);

    CLEventTree sortStep(QCLBuffer &input, QCLBuffer &output, uint numElements);
#ifdef USE_MULTI_LOWER_BOUND
    CLEventTree expandPartitionStartsStep(QCLBuffer &sorted, uint numElements, QCLBuffer &partitionStartsOutput);
#else
    CLEventTree clearPartitionsStep(QCLBuffer &partitions);
    CLEventTree findPartitionEndsStep(QCLBuffer &sorted, uint numElements, QCLBuffer &partitions);
    CLEventTree subtractPartitionStartsStep(QCLBuffer &sorted, uint numElements, QCLBuffer &partitions);
    CLEventTree scanPartitionSizesStep(QCLBuffer &partitions);
#endif
};

#endif // PARTITION_H
