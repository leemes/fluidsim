#ifndef MOVE_H
#define MOVE_H

#include <qclkernel.h>
#include <qclprogram.h>

class CLContext;
class CLEventTree;

/**
 * Moves elements from an input array into an output array using a move table (of type cl_uint).
 * If onlyIfLeftDiffers is set, writing is disabled if the left neighbour in the moveTable has the same value. The very first element is always processed.
 * The outputExpression should return a T_OUT and can access:
 *  - T_IN element
 *  - uint index
 */
class Move
{
public:
    enum Mode {
        TableOldNew,    // => moveTable[oldIndex] = newIndex
        TableNewOld     // => moveTable[newIndex] = oldIndex
    };

    Move(CLContext &context,
         const QString &inputTypeName, const QString &outputTypeName,
         const QString &outputExpression = "element",
         Mode mode = TableOldNew,
         bool onlyIfLeftDiffers = false,
         uint maxGroupSize = 256);

    Mode mode() const { return m_mode; }
    uint maxGroupSize() const { return m_maxGroupSize; }

    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, uint numElements,
                           const QCLBuffer &moveTable, int oldIndexDelta = 0, int newIndexDelta = 0);

private:
    QCLProgram m_program;
    QCLKernel m_kernel;
    const Mode m_mode;
    const uint m_maxGroupSize;
};

#endif // MOVE_H
