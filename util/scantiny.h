#ifndef SCANTINY_H
#define SCANTINY_H

#include <qclkernel.h>

class QCLBuffer;
class CLContext;

class ScanTiny
{
public:
    enum Mode {
        Exclusive,
        Inclusive
    };

    ScanTiny(CLContext &context, int numElements, Mode mode = Inclusive);

    int numElements() const { return m_numElements; }
    Mode mode() const { return m_mode; }

    QCLEventList operator() (const QCLBuffer &input, const QCLBuffer &output, uint valueOffset = 0);

private:
    QCLKernel m_kernel;
    int m_numElements;
    Mode m_mode;
};

#endif // SCANTINY_H
