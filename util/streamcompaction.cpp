#include "streamcompaction.h"
#include "cleventtree.h"
#include "clcontext.h"

#include "debug.h"
using namespace std;

StreamCompaction::StreamCompaction(CLContext &context, const QString &inputTypeName, size_t inputTypeSize, const QString &outputTypeName, size_t outputTypeSize,
                                   uint predicateTransformStride, const QString &predicateTransformExpression, const QString &outputExpression, const QString &fillExpression, uint maxGroupSize) :
    m_context(context),
    m_inputTypeSize(inputTypeSize),
    m_outputTypeSize(outputTypeSize),
    m_preparedForNumElements(0),
    m_fill(context, outputTypeName, fillExpression),
    m_transform(context, inputTypeName, inputTypeSize, "uint", sizeof(cl_uint), predicateTransformStride, predicateTransformExpression),
    m_scan(context, "uint", sizeof(cl_uint), Scan::Inclusive),
    m_move(context, inputTypeName, outputTypeName, outputExpression, Move::TableOldNew, true)
{
}


CLEventTree StreamCompaction::operator ()(const QCLBuffer &input, const QCLBuffer &output, uint numElements)
{
    allocateAuxiliaryBuffers(numElements);

    CLEventTree events("StreamCompaction");

  //  cout << "input:" << endl;
  //  debug<cl_uint>(input);

    // Fill the output buffer with the "fillExpression"
    events << m_fill(output, numElements);

  //  cout << "initialized output with \"fillExpression\":" << endl;
  //  debug<cl_uint>(output);

    // Apply the predicate on the input and store it in an aux buffer
    events << m_transform(input, m_predicateAuxBuffer, numElements);

  //  cout << "predicate on input:" << endl;
  //  debug<cl_uint>(m_predicateAuxBuffer);

    // Scan this aux buffer to find the target position (+1, because it's an inclusive scan)
    events << m_scan(m_predicateAuxBuffer, m_predicateAuxBuffer, numElements);

 //   cout << "scanned predicate:" << endl;
 //   debug<cl_uint>(m_predicateAuxBuffer);

    // Move the elements to the target position (but only the elements whose predicate was true,
    // i.e. the scanned value differs from the left neighbour in the array)
    events << m_move(input, output, numElements, m_predicateAuxBuffer, 0, -1);

  //  cout << "moved (and applied \"outputExpression\"):" << endl;
  //  debug<cl_uint>(output);
  //  cout << endl;

    return events;
}

void StreamCompaction::allocateAuxiliaryBuffers(uint numElements)
{
    // Note: This algorithm itself has no minimum buffer size for its inputs.
    // But the aux buffer has to conform to the min buffer size of the scan algorithm which we apply on it.

    if(m_preparedForNumElements == m_scan.minBufferSizeForNumElements(numElements))
        return;

    freeAuxiliaryBuffers();

    m_preparedForNumElements = m_scan.minBufferSizeForNumElements(numElements);

    m_predicateAuxBuffer = m_context.createBufferDevice(sizeof(cl_uint) * m_preparedForNumElements, QCLMemoryObject::ReadWrite);
}

void StreamCompaction::freeAuxiliaryBuffers()
{
    if (m_preparedForNumElements) {
        m_predicateAuxBuffer = QCLBuffer();
        m_preparedForNumElements = 0;
    }
}
