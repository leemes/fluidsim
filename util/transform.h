#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <qclkernel.h>
#include <qclprogram.h>

class QCLBuffer;
class CLContext;
class CLEventTree;


/**
 * outputExpression can access the following parameters:
 *  - T_IN *input       The whole input array, accessible by index
 *  - uint index        The index of the current element itself, already multiplied with INPUT_STRIDE
 *  - uint numElements  The number of elements as passed to the algorithm
 *
 * For example, to compare the current element with the next for inequality, use an expression like:
 *  input[index + 1] != input[index]                (direct neighbour)
 *  input[index + INPUT_STRIDE] != input[index]     (strided neighbour)
 *
 * It's your own responsibility to take care that such "foreign" index addresses are bound-checked.
 */
class Transform
{
public:
    Transform(CLContext &context,
              const QString &inputTypeName = "int", size_t inputTypeSize = sizeof(cl_int),
              const QString &outputTypeName = "int", size_t outputTypeSize = sizeof(cl_int),
              uint inputStride = 1,
              const QString &outputExpression = "element",
              uint maxGroupSize = 256);

    uint maxGroupSize() const { return m_maxGroupSize; }

    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, uint numElements);

private:
    QCLProgram m_program;
    QCLKernel m_kernel;
    const uint m_maxGroupSize;
    const uint m_inputTypeSize;
    const uint m_outputTypeSize;
    const uint m_inputStride;
};

#endif // TRANSFORM_H
