#ifndef FILL_H
#define FILL_H

#include <qclkernel.h>
#include <qclprogram.h>

class QCLBuffer;
class CLContext;
class CLEventTree;

class Fill
{
public:
    Fill(CLContext &context,
         const QString &typeName = "int",
         const QString &outputExpression = "element",
         uint maxGroupSize = 256);

    CLEventTree operator()(const QCLBuffer &array, uint numElements);

private:
    QCLProgram m_program;
    QCLKernel m_kernel;
    const uint m_maxGroupSize;
};

#endif // FILL_H
