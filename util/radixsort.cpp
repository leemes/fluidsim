#include "radixsort.h"
#include "clsourceloader.h"
#include "clcontext.h"
#include "cleventtree.h"
#include "bucketsort.h"
#include <QStringList>
#include "util.h"
#include "debug.h"
using std::cout;
using std::endl;


RadixSort::RadixSort(CLContext &context, const QString &typeName, size_t typeSize, QVector<Step> steps) :
    m_context(context),
    m_typeSize(typeSize),
    m_steps(steps),
    m_preparedForNumElements(0),
    m_move(context, typeName, typeName, "element", Move::TableNewOld)
{
    // TODO: Fix this restriction. For this, we need to "merge" first and last steps, as they are the same in this case.
    Q_ASSERT(steps.count() >= 2);

    const int k = steps.count();

  //  qDebug("RadixSort steps (original order):");
    uint combinedKeyNumBits = 0;
    m_totalNumBuckets = 1;
    for (int s = 0; s < k; ++s)
    {
        const Step &step = steps[s];

        Q_ASSERT(step.keyNumBits <= 8);

      //  qDebug("  Step %d: bits = %d, key = %s", s, step.keyNumBits, qPrintable(step.keyExpression));

        // Don't count the least significant step, because it's not part of the combined key for intermediate storage
        if (s != steps.count() - 1)
            combinedKeyNumBits += step.keyNumBits;

        m_totalNumBuckets *= (1 << step.keyNumBits);
    }
    // The sum of the num bits of all steps except the least significant one has to be less or equal 31.
    Q_ASSERT(combinedKeyNumBits <= 31);


    // Reversed copy of the steps (much easier to handle, since this equals now the execution order)
    QVector<Step> stepsReversed;
    for (int s = k - 1; s >= 0; --s)
        stepsReversed.append(steps[s]);


    // Create bucket sort type descriptions
    struct Type {
        QString name;
        size_t size;
    };
    Type firstInputType   = { typeName, typeSize         };
    Type intermediateType = { "uint2",  sizeof(cl_uint2) };
    Type lastOutputType   = { "uint",   sizeof(cl_uint)  };

    // Create expression combining all keys (except of the first step executed)
    // Something similar to: (KEY2) | (KEY1) << 4 | (KEY0) << 8
    QStringList subExpressions;
    uint currentBitOffset = 0;
    for (const Step &step : stepsReversed)
    {
        // Don't include the first step for this
        if (&step == &stepsReversed.first())
            continue;

        if (currentBitOffset)
            subExpressions << QString("((uint)(%1) << %2)").arg(step.keyExpression).arg(currentBitOffset);
        else
            subExpressions << QString("(uint)(%1)").arg(step.keyExpression);
        currentBitOffset += step.keyNumBits;
    }
    QString combinedKeyExpression = subExpressions.join(" | ");
  //  qDebug("COMBINED KEY EXPRESSION: '%s'", qPrintable(combinedKeyExpression));

    // Create output expression for the first step, which is
    // uint2 with .x = original index, .y = combined key
    QString firstOutputExpression = "(uint2)(index, " + combinedKeyExpression + ")";

    // Create bucket sort instances
  //  qDebug("Compiled steps (execution order):");
    currentBitOffset = 0;
    for (int s = 0; s < k; ++s)
    {
        const Step &step = stepsReversed[s];

        const uint numBuckets = (1 << step.keyNumBits);

        // The types this step operates on
        Type inputType  = (s == 0)     ? firstInputType : intermediateType;
        Type outputType = (s == k - 1) ? lastOutputType : intermediateType;

        // Find the expressions for key and output
        QString keyExpression;
        QString outputExpression;
        if (s == 0) {
            keyExpression = step.keyExpression;
            outputExpression = firstOutputExpression;
        } else {
            const uint mask = numBuckets - 1;
            keyExpression = currentBitOffset ? QString("(element.y >> %1)").arg(currentBitOffset) : QString("element.y");
            keyExpression.append(QString(" & 0x%1").arg(mask, 0, 16));
            if (s == k - 1) {
                outputExpression = "element.x"; // Only output the original index, throw away the combined keys
            } else {
                outputExpression = "element";   // Keep as is
            }
            currentBitOffset += step.keyNumBits;
        }
        //qDebug("  Step %d: Ti = %s, To = %s, key = %s, output = %s",
        //       s, qPrintable(inputType.name), qPrintable(outputType.name),
        //       qPrintable(keyExpression), qPrintable(outputExpression));

        // Create an instance for the bucket sort step
        BucketSort *bs = new BucketSort(context, numBuckets,
                                        inputType.name, inputType.size,
                                        outputType.name, outputType.size,
                                        keyExpression, outputExpression);
        m_bucketSorts.push_back(std::unique_ptr<BucketSort>(bs));
    }

    // For the bucket starts, we use the bucket starts output of the last bucket sort step executed.
    // This is the number of buckets in the last step.
    m_lastNumBuckets = 1 << stepsReversed.last().keyNumBits;
}

RadixSort::~RadixSort()
{
}

CLEventTree RadixSort::operator ()(const QCLBuffer &input, const QCLBuffer &output, uint numElements, const QCLBuffer &mostSignificantBucketStarts)
{
    allocateAuxiliaryBuffers(numElements);

    Q_ASSERT(input.access() == QCLBuffer::ReadOnly || input.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(input.size() >= m_typeSize * minBufferSizeForNumElements(numElements));
    Q_ASSERT(output.access() == QCLBuffer::WriteOnly || output.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(output.size() >= m_typeSize * minBufferSizeForNumElements(numElements));
    Q_ASSERT(mostSignificantBucketStarts.isNull() || mostSignificantBucketStarts.access() == QCLBuffer::WriteOnly || mostSignificantBucketStarts.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(mostSignificantBucketStarts.isNull() || mostSignificantBucketStarts.size() >= sizeof(cl_uint) * m_lastNumBuckets);

    CLEventTree events("RadixSort");

    //cout << "input:" << endl;
    //debug<cl_char>(const_cast<QCLBuffer&>(input));

    // The output of the last bucket sort is the move table for the move step.
    events << bucketSortsStep(input, numElements, m_moveTable, mostSignificantBucketStarts);

    //cout << "oldindex(newindex):" << endl;
    //debug<cl_uint>(*const_cast<QCLBuffer*>(moveTable));

    events << moveStep(input, output, numElements, *m_moveTable);

    //cout << "output:" << endl;
    //debug<cl_char>(const_cast<QCLBuffer&>(output));

    return events;
}

const QCLBuffer &RadixSort::lastMoveTable() const
{
    return *m_moveTable;
}

uint RadixSort::minBufferSizeForNumElements(uint numElements) const
{
    return numElements ? qMax(minBufferSize(), roundUpPowerOfTwo(numElements)) : minBufferSize();
}

void RadixSort::allocateAuxiliaryBuffers(uint numElements)
{
    if(m_preparedForNumElements == minBufferSizeForNumElements(numElements))
        return;

    freeAuxiliaryBuffers();

    m_preparedForNumElements = minBufferSizeForNumElements(numElements);

    // Allocate ping and pong buffers for the intermediate results (between bucket sorts) (cl_uint2)
    m_pingBuffer = m_context.createBufferDevice(sizeof(cl_uint2) * m_preparedForNumElements, QCLMemoryObject::ReadWrite);
    m_pongBuffer = m_context.createBufferDevice(sizeof(cl_uint2) * m_preparedForNumElements, QCLMemoryObject::ReadWrite);
}

void RadixSort::freeAuxiliaryBuffers()
{
    if (m_preparedForNumElements) {
        m_pingBuffer = QCLBuffer();
        m_pongBuffer = QCLBuffer();
        m_preparedForNumElements = 0;
    }
}

CLEventTree RadixSort::bucketSortsStep(const QCLBuffer &input, uint numElements, const QCLBuffer *&moveTable, const QCLBuffer &mostSignificantBucketStarts_optional)
{
    const QCLBuffer *currentInput = &input;
    const QCLBuffer *currentOutput = &m_pingBuffer;

    CLEventTree events("bucketSortsStep");

    for (uint s = 0; s < m_bucketSorts.size(); ++s)
    {
        BucketSort & bucketSort = *m_bucketSorts[s].get();

       // qDebug() << currentInput << currentInput->size() << bucketSort.m_inputTypeSize;
       // qDebug() << currentOutput << currentOutput->size() << bucketSort.m_outputTypeSize;

        bool isFirst = s == 0;
        bool isLast  = s == m_bucketSorts.size() - 1;

        if (isLast)
            events << bucketSort(*currentInput, *currentOutput, numElements, mostSignificantBucketStarts_optional);
        else
            events << bucketSort(*currentInput, *currentOutput, numElements);

       // if (!isLast) {
       //     cout << "intermediate:" << endl;
       //     debug<uint>(*const_cast<QCLBuffer*>(currentOutput));
       // }

        // Only the first step reads from the input buffer. Pretend that we just read from the pong buffer.
        if (isFirst)
            currentInput = &m_pongBuffer;

        std::swap(currentInput, currentOutput);
    }

    // Since we swapped after the last step too, the last output is what is now in currentInput.
    moveTable = currentInput;
    return events;
}

CLEventTree RadixSort::moveStep(const QCLBuffer &input, const QCLBuffer &output, uint numElements, const QCLBuffer &moveTable)
{
    return CLEventTree(m_move(input, output, numElements, moveTable), "moveStep");
}
