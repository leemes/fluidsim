#include "scan.h"
#include "clcontext.h"
#include "cleventtree.h"
#include "util.h"
#include "debug.h"
#include <qcllocalmemory.h>


Scan::Scan(CLContext &context, const char *typeName, size_t typeSize, Scan::Mode mode, uint maxGroupSize) :
    m_context(context),
    m_mode(mode),
    m_maxGroupSize(maxGroupSize),
    m_typeSize(typeSize),
    m_preparedForNumElements(0),
    m_numLevels(0)
{
    QMap<QString,QString> defines;
    defines["T"] = typeName;
    defines["MODE"] = (mode == Inclusive) ? "MODE_INCLUSIVE" : "MODE_EXCLUSIVE";

    m_program = context.buildProgram("util/scan.cl", defines);

    m_kernelScan = m_program.createKernel("scan");
    if (m_kernelScan.isNull())
        qFatal("Failed to create kernel for Scan.");

    m_kernelAdd = m_program.createKernel("scanAdd");
    if (m_kernelAdd.isNull())
        qFatal("Failed to create kernel for Scan.");

    m_itemWidth = 16; // FIXME: Can be dynamic ("-DW=...")
    m_maxGroupWidth = m_maxGroupSize * m_itemWidth;
}

CLEventTree Scan::operator ()(const QCLBuffer &input, const QCLBuffer &output, uint numElements)
{
    allocateAuxiliaryBuffers(numElements);

    Q_ASSERT(input.access() == QCLBuffer::ReadOnly || input.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(output.access() == QCLBuffer::WriteOnly || output.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(input.size() >= m_typeSize * minBufferSizeForNumElements(numElements));
    Q_ASSERT(output.size() >= m_typeSize * minBufferSizeForNumElements(numElements));

    CLEventTree events("Scan");

    // Set the "special" buffers in the first step
    m_levels[0].in = &input;
    m_levels[0].out = &output;

    for (uint l = 0; l < m_numLevels; ++l) {
        const Level &level = m_levels[l];
        //debug<int,32,5>(*const_cast<QCLBuffer*>(level.in));
        events << scanStep(*level.in, *level.out, level.auxiliaryBuffer, level.groupSize, level.numGroups);
    }
    // The "add" step starts at the second top-most level (for the top-most level, there is nothing to do)
    for (int l = m_numLevels - 2; l >= 0; --l) {
        const Level &level = m_levels[l];
        events << addStep(*level.out, level.auxiliaryBuffer, level.groupSize, level.numGroups);
    }

    return events;
}

uint Scan::minBufferSizeForNumElements(uint numElements) const
{
    return numElements ? qMax(minBufferSize(), roundUpPowerOfTwo(numElements)) : minBufferSize();
}

void Scan::allocateAuxiliaryBuffers(uint numElements)
{
    if(m_preparedForNumElements == minBufferSizeForNumElements(numElements))
        return;

    freeAuxiliaryBuffers();

    m_preparedForNumElements = minBufferSizeForNumElements(numElements);

    // Compute number of levels
    uint current = m_preparedForNumElements;
    m_numLevels = 0;
    while (current > 1) {
        current /= m_maxGroupWidth;
        ++m_numLevels;
    }
    //qDebug("%d levels for %d elements", m_numLevels, m_preparedForNumElements);
    m_levels.reset(new Level[m_numLevels]);

    // Compute reduction widths per level.
    // Try to reduce m_maxGroupWidth items, but avoid too small last levels,
    // i.e. make sure that all level output sizes are >= m_itemWidth.
    current = m_preparedForNumElements;
    for (uint l = 0; l < m_numLevels; ++l) {
        uint groupSize = m_maxGroupSize;
        uint width = groupSize * m_itemWidth;
        uint outputSize = current / width;
        if (outputSize < m_itemWidth && outputSize != 1) {
            // The next level would become too small. To avoid this, don't reduce that much.
            // Case 1: This is the last level, i.e. reduce till the end.
            // Case 2: This is the level before the last => reduce to outputSize = m_itemWidth.
            if (l == m_numLevels - 1) {
                outputSize = 1;
            } else {
                outputSize = m_itemWidth;
            }
            // Invert the formulas.
            width = current / outputSize;
            groupSize = width / m_itemWidth;
        }

        Q_ASSERT(groupSize > 0);
        Q_ASSERT(width > 0);
        Q_ASSERT(outputSize >= m_itemWidth || outputSize == 1);
        Q_ASSERT(outputSize == 1 || l != m_numLevels - 1);

        // "in" and "out" for the first level are the global in and out buffers -- set them later.
        // For all other arrays, the aux buffer is used for in and out.
        const QCLBuffer *in  = l ? &m_levels[l-1].auxiliaryBuffer : 0;
        const QCLBuffer *out = l ? &m_levels[l-1].auxiliaryBuffer : 0;
        m_levels[l] = Level{
                (outputSize > 1) ? m_context.createBufferDevice(m_typeSize * outputSize, QCLBuffer::ReadWrite) : QCLBuffer(),
                in, out,
                groupSize,
                outputSize
            };
        //qDebug("level %d: %d -> %d (using %d groups of size %d)", l, current, m_levels[l].numGroups, m_levels[l].numGroups, m_levels[l].groupSize);
        current = outputSize;
    }
    Q_ASSERT(current == 1);
}

void Scan::freeAuxiliaryBuffers()
{
    if (m_preparedForNumElements) {
        m_levels.reset();
        m_preparedForNumElements = 0;
    }
}

CLEventTree Scan::scanStep(const QCLBuffer &input, const QCLBuffer &output, const QCLBuffer &higherLevel, uint groupSize, uint numGroups)
{
    m_kernelScan.setLocalWorkSize(groupSize);
    m_kernelScan.setGlobalWorkSize(groupSize * numGroups);

    // Size of local memory needed -- this formula avoids bank-conflicts
    uint localMemSize = m_typeSize * (groupSize * 33 / 32);

    return CLEventTree(m_kernelScan(input, output, higherLevel, QCLLocalMemory(localMemSize)), "scanStep");
}

CLEventTree Scan::addStep(const QCLBuffer &array, const QCLBuffer &higherLevel, uint groupSize, uint numGroups)
{
    m_kernelAdd.setLocalWorkSize(groupSize);
    m_kernelAdd.setGlobalWorkSize(groupSize * numGroups);

    return CLEventTree(m_kernelAdd(array, higherLevel), "addStep");
}


