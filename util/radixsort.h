#ifndef RADIXSORT_H
#define RADIXSORT_H

#include <qclkernel.h>
#include <memory>
#include "move.h"

class CLContext;
class CLEventTree;
class BucketSort;

/*
 * The expressions have to describe the body of a function like this:
 *
 *   - uint keyExpression(T_in element)
 *   - T_out outputExpression(T_in element, uint index)
 */
class RadixSort
{
public:
    struct Step
    {
        QString keyExpression;
        int keyNumBits;
    };

    RadixSort(CLContext &context,
              const QString &typeName, size_t typeSize,
              QVector<Step> steps);
    ~RadixSort();

    QVector<Step> steps() const { return m_steps; }

    CLEventTree operator()(const QCLBuffer &input, const QCLBuffer &output, uint numElements, const QCLBuffer &bucketStarts = QCLBuffer());
    const QCLBuffer & lastMoveTable() const;

    uint minBufferSize() const { return 1024; }
    uint minBufferSizeForNumElements(uint numElements) const;

    void allocateAuxiliaryBuffers(uint numElements);
    void freeAuxiliaryBuffers();

private:
    CLContext &m_context;
    const size_t m_typeSize;
    const QVector<Step> m_steps;

    // auxiliary:
    uint m_preparedForNumElements;
    QCLBuffer m_pingBuffer;
    QCLBuffer m_pongBuffer;
    uint m_lastNumBuckets;
    uint m_totalNumBuckets;

    const QCLBuffer *m_moveTable;

    // other algos:
    std::vector<std::unique_ptr<BucketSort>> m_bucketSorts;
    Move m_move;

    CLEventTree bucketSortsStep(const QCLBuffer &input, uint numElements, const QCLBuffer *&m_moveTable, const QCLBuffer &mostSignificantBucketStarts_optional); // Sets "moveTable" to the last step's output (ping or pong)
    CLEventTree moveStep(const QCLBuffer &input, const QCLBuffer &output, uint numElements, const QCLBuffer &m_moveTable);
};

#endif // RADIXSORT_H
