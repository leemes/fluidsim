#include "rotate.h"
#include "clcontext.h"
#include "cleventtree.h"

Rotate::Rotate(CLContext &context, const QString &typeName, Rotate::Mode mode, QSize maxGroupSize) :
    m_mode(mode),
    m_maxGroupSize(maxGroupSize)
{
    QMap<QString,QString> defineConstants;
    defineConstants["GROUPSIZE_X"] = QString::number(m_maxGroupSize.width());
    defineConstants["GROUPSIZE_Y"] = QString::number(m_maxGroupSize.height());
    defineConstants["T"] = typeName;
    defineConstants["MODE"] = (mode == RotateCW) ? "MODE_ROTATE_CW" : (mode == RotateCCW) ? "MODE_ROTATE_CCW" : "MODE_TRANSPOSE";

    m_program = context.buildProgram("util/rotate.cl", defineConstants);

    m_kernel = m_program.createKernel("rotateMemory");
    if (m_kernel.isNull())
        qFatal("Failed to create kernel for Rotate.");

    m_kernel.setLocalWorkSize(m_maxGroupSize);
}

CLEventTree Rotate::operator ()(const QCLBuffer &input, const QCLBuffer &output, QSize size)
{
    m_kernel.setRoundedGlobalWorkSize(size);
    return CLEventTree(m_kernel(input, output, size), "Rotate");
}
