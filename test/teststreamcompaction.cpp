#include "teststreamcompaction.h"
#include "util/streamcompaction.h"
#include "clcontext.h"
#include "cleventtree.h"
using namespace std;


bool TestStreamCompaction::run(CLContext &context)
{
    if (!run1(context))
        return false;

    if (!run2(context))
        return false;

    return true;
}

bool TestStreamCompaction::run1(CLContext &context)
{
    StreamCompaction testCase(context,
                              "uint", sizeof(cl_uint),
                              "uint2", sizeof(cl_uint2),
                              1,
                              "input[index] % 2",
                              "(uint2)(input[index], index)",
                              "(uint2)(42, 1337)");

    auto predicate = [](uint element){ return element % 2; };
    auto output = [](uint element, uint index){ return cl_uint2{ element, index }; };
    cl_uint2 fill = { 42, 1337 };

    typedef cl_uint T;
    typedef cl_uint2 U;
    const uint numElements = 1 << 20;

    T *inData = new T[numElements];
    for (uint i = 0; i < numElements; ++i) {
        inData[i] = rand() % 1000;
    }

    U *expected = new U[numElements];
    uint currentIndex = 0;
    for (uint i = 0; i < numElements; ++i) {
        T element = inData[i];
        if (predicate(element))
            expected[currentIndex++] = output(element, i);
    }
    for (; currentIndex < numElements; ++currentIndex)
        expected[currentIndex] = fill;


    QCLBuffer inBuffer = context.createBufferCopy(inData, sizeof(T) * numElements, QCLMemoryObject::ReadWrite);
    QCLBuffer outBuffer = context.createBufferDevice(sizeof(U) * numElements, QCLMemoryObject::ReadWrite);

    testCase(inBuffer, outBuffer, numElements);

    bool ok = verify((uint*)expected, outBuffer, 2*numElements);

    delete[] inData;
    delete[] expected;

    return ok;
}

bool TestStreamCompaction::run2(CLContext &context)
{
    // This test case is what we use in the grid to find non-empty cell groups

    const uint cellGroupSize = 8;

    StreamCompaction testCase(context,
                              "uint", sizeof(cl_uint),
                              "uint", sizeof(cl_uint),
                              cellGroupSize,
                              "input[index+INPUT_STRIDE] > input[index]",
                              "index",
                              "UINT_MAX");

    typedef cl_uint T;
    const uint numElements = 1 << 14;

    // This test case assumes a sorted input
    T *inData = new T[numElements * cellGroupSize + 1];
    for (uint i = 0; i < numElements * cellGroupSize + 1; ++i) {
        inData[i] = rand() % (1 << 10);
    }
    inData[0] = 0; // We want one cell to start at 0 (which is the case in the actual grid algorithm)
    std::sort(inData, inData + (numElements * cellGroupSize + 1));

    T *expected = new T[numElements];
    uint currentIndex = 0;
    for (uint i = 0; i < numElements; ++i) {
        if (inData[cellGroupSize * i] != inData[cellGroupSize * (i + 1)])
            expected[currentIndex++] = i;
    }
    for (; currentIndex < numElements; ++currentIndex)
        expected[currentIndex] = UINT_MAX;


    QCLBuffer inBuffer = context.createBufferCopy(inData, sizeof(T) * (numElements * cellGroupSize + 1), QCLMemoryObject::ReadWrite);
    QCLBuffer outBuffer = context.createBufferDevice(sizeof(T) * numElements, QCLMemoryObject::ReadWrite);

    testCase(inBuffer, outBuffer, numElements);

    bool ok = verify(expected, outBuffer, numElements);

    delete[] inData;
    delete[] expected;

    return ok;
}
