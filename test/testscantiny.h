#ifndef TESTSCANTINY_H
#define TESTSCANTINY_H

#include "abstracttest.h"

class ScanTiny;

class TestScanTiny : AbstractTest
{
public:
    static bool run(CLContext &context);

private:
    static bool run(CLContext &context, ScanTiny &testCase);
};

#endif // TESTSCANTINY_H
