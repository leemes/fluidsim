#ifndef TESTSTREAMCOMPACTION_H
#define TESTSTREAMCOMPACTION_H

#include "abstracttest.h"

class TestStreamCompaction : public AbstractTest
{
public:
    static bool run(CLContext &context);

private:
    static bool run1(CLContext &context);
    static bool run2(CLContext &context);
};

#endif // TESTSTREAMCOMPACTION_H
