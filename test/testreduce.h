#ifndef TESTREDUCE_H
#define TESTREDUCE_H

#include "abstracttest.h"

class Reduce;

class TestReduce : AbstractTest
{
public:
    static bool run(QCLContext &context, const CLSourceLoader &sourceLoader);

private:
    template<typename T>
    static bool runType(QCLContext &context, const CLSourceLoader &sourceLoader, const char *typeName);

    template<typename T>
    static bool runCase(QCLContext &context, Reduce &testCase, std::function<T(T,T)> reduce);

    template<typename T>
    static bool runSize(QCLContext &context, Reduce &testCase, uint numElements, std::function<T(T,T)> reduce);
};

#endif // TESTREDUCE_H
