#ifndef TESTPARTITION_H
#define TESTPARTITION_H

#include "abstracttest.h"

class TestPartition : public AbstractTest
{
public:
    static bool run(CLContext &context);
};

#endif // TESTPARTITION_H
