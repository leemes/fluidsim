#include "testscantiny.h"
#include "util/scantiny.h"
#include "clcontext.h"

bool TestScanTiny::run(CLContext &context)
{
    ScanTiny testCases[] = {
        { context, 2, ScanTiny::Inclusive },
        { context, 2, ScanTiny::Exclusive },
        { context, 4, ScanTiny::Inclusive },
        { context, 4, ScanTiny::Exclusive },
        { context, 8, ScanTiny::Inclusive },
        { context, 8, ScanTiny::Exclusive },
        { context, 16, ScanTiny::Inclusive },
        { context, 16, ScanTiny::Exclusive },
        { context, 32, ScanTiny::Inclusive },
        { context, 32, ScanTiny::Exclusive },
        { context, 64, ScanTiny::Inclusive },
        { context, 64, ScanTiny::Exclusive },
        { context, 128, ScanTiny::Inclusive },
        { context, 128, ScanTiny::Exclusive }
    };

    for (ScanTiny & testCase : testCases)
        if (!run(context, testCase))
            return false;

    return true;
}

bool TestScanTiny::run(CLContext &context, ScanTiny &testCase)
{
    typedef uint T;

    T valueOffset = rand();

    T *inData = new T[testCase.numElements()];
    for (int i = 0; i < testCase.numElements(); ++i) {
        inData[i] = rand();
    }

    T *expected = new T[testCase.numElements() + 1];
    T currentSum = valueOffset;
    if (testCase.mode() == ScanTiny::Inclusive) {
        for (int i = 0; i < testCase.numElements(); ++i)
            expected[i] = (currentSum += inData[i]);
    } else {
        for (int i = 0; i < testCase.numElements(); ++i) {
            expected[i] = currentSum;
            currentSum += inData[i];
        }
    }

    QCLBuffer inBuffer = context.createBufferCopy(inData, sizeof(T) * testCase.numElements(), QCLMemoryObject::ReadOnly);
    QCLBuffer outBuffer = context.createBufferDevice(sizeof(T) * testCase.numElements(), QCLMemoryObject::WriteOnly);

    testCase(inBuffer, outBuffer, valueOffset);

    return verify(expected, outBuffer, testCase.numElements());
}
