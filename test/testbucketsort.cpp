#include "testbucketsort.h"
#include "util/bucketsort.h"
#include "clcontext.h"
#include "cleventtree.h"
#include <QDebug>
#include "debug.h"
#include "util.h"


bool operator ==(cl_float2 a, cl_float2 b) {
    return a.s[0] == b.s[0] && a.s[1] == b.s[1];
}
bool operator ==(cl_uint2 a, cl_uint2 b) {
    return a.s[0] == b.s[0] && a.s[1] == b.s[1];
}
std::ostream & operator <<(std::ostream & out, cl_float2 v) {
    return out << "(float2)";
}
std::ostream & operator <<(std::ostream & out, cl_uint2 v) {
    return out << "(uint2)";
}



bool TestBucketSort::run(CLContext &context)
{
    // BASIC TEST with uint -> uint, simple modulo bucketing
    return runType<cl_uint,cl_uint>(
        context,
        "uint", "uint",
        "element % NUM_BUCKETS",
        "element",
        [](cl_uint element, cl_uint numBuckets){ return element % numBuckets; },
        [](cl_uint element, cl_uint index){ return element; },
        []{ return rand() % 1000; })

    // ADVANCED TEST which stores a different output also depending on the original index
    && runType<cl_float2,cl_uint2>(
        context,
        "float2", "uint2",
        "(uint)(element.x * NUM_BUCKETS)",
        "(uint2)((uint)(element.y * 100), index)",
        [](cl_float2 element, cl_uint numBuckets){ return (uint)(element.s[0] * numBuckets); },
        [](cl_float2 element, cl_uint index){ return cl_uint2{(uint)(element.s[1] * 100), index}; },
        []{ return cl_float2{frand(), frand()}; })
    ;
}


template<typename T_IN, typename T_OUT>
bool TestBucketSort::runType(CLContext &context,
                             const char *inputTypeName, const char *outputTypeName,
                             const char *keyExpression, const char *outputExpression,
                             std::function<cl_uint(T_IN,cl_uint)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                             std::function<T_IN()> rand)
{
    qDebug("TYPES: %s -> %s, EXPRESSIONS: key(element) = %s, output(element,index) = %s", inputTypeName, outputTypeName, keyExpression, outputExpression);

    for (uint numBuckets = 2; numBuckets <= 128; numBuckets *= 2)
    {
        qDebug("  NUM BUCKETS = %d", numBuckets);

        for (uint maxGroupSize = qMax(8U, numBuckets); maxGroupSize <= 256; maxGroupSize *= 2)
        {
            qDebug("    GROUP SIZE = %d", maxGroupSize);

            BucketSort testCase = {
                context,
                numBuckets,
                inputTypeName, sizeof(T_IN),
                outputTypeName, sizeof(T_OUT),
                keyExpression, outputExpression,
                maxGroupSize
            };

            if (!runCase<T_IN, T_OUT>(context, testCase, std::bind2nd(key, numBuckets), output, rand))
                return false;
        }
    }

    return true;
}


template<typename T_IN, typename T_OUT>
bool TestBucketSort::runCase(CLContext &context, BucketSort &testCase,
                             std::function<cl_uint(T_IN)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                             std::function<T_IN()> rand)
{
    for (int numElements = 1; numElements < (1<<20); numElements += 1 + std::rand() % (1 + numElements)) {
        qDebug("      numElements = %d", numElements);
        if (!runSize<T_IN,T_OUT>(context, testCase, numElements, key, output, rand))
            return false;
    }
    return true;
}


template<typename T_IN, typename T_OUT>
bool TestBucketSort::runSize(CLContext &context, BucketSort &testCase, int numElements,
                             std::function<cl_uint(T_IN)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                             std::function<T_IN()> rand)
{
    if (numElements == 0)
        return true;

    const uint allocElements = testCase.minBufferSizeForNumElements(numElements);
    //qDebug() << "numElements:" << numElements << "--" << "allocated:" << allocElements;

    // allocate host arrays
    T_IN *inData = new T_IN[allocElements];
    cl_uint *bucketStartsData = new cl_uint[testCase.numBuckets()];
    cl_uint *bucketStartsCopy = new cl_uint[testCase.numBuckets()];
    T_OUT *expected = new T_OUT[numElements];

    // create elements, count bucket sizes
    memset(bucketStartsData, 0, sizeof(cl_uint) * testCase.numBuckets());
    for (int i = 0; i < numElements; ++i) {
        inData[i] = rand();
        cl_uint b = key(inData[i]);
        Q_ASSERT(b < testCase.numBuckets());
        ++bucketStartsData[b];
    }
    //cout << "in" << endl;
    //debug(inData, numElements);

    // exclusively scan bucketStarts:
    cl_uint currentStart = 0;
    for (uint b = 0; b < testCase.numBuckets(); ++b) {
        cl_uint thisBucket = bucketStartsData[b];
        bucketStartsData[b] = currentStart;
        currentStart += thisBucket;
    }
    //cout << "bucketStarts" << endl;
    //debug(bucketStartsData, testCase.numBuckets());

    // put in buckets
    memcpy(bucketStartsCopy, bucketStartsData, sizeof(cl_uint) * testCase.numBuckets());
    for (int i = 0; i < numElements; ++i) {
        cl_uint b = key(inData[i]);
        cl_uint &pos = bucketStartsCopy[b];
        expected[pos] = output(inData[i], i);
        ++pos;
    }
    //cout << "expected" << endl;
    //debug(expected, numElements);

    QCLBuffer inBuffer = context.createBufferCopy(inData, sizeof(T_IN) * allocElements, QCLMemoryObject::ReadOnly);
    QCLBuffer outBuffer = context.createBufferDevice(sizeof(T_OUT) * allocElements, QCLMemoryObject::WriteOnly);
    QCLBuffer bucketStartsOut = context.createBufferDevice(sizeof(cl_uint) * testCase.numBuckets(), QCLMemoryObject::WriteOnly);

    testCase(inBuffer, outBuffer, numElements, bucketStartsOut);

    bool ok = verify(expected, outBuffer, numElements)
            && verify(bucketStartsData, bucketStartsOut, testCase.numBuckets());

    if (!ok) {
        std::cerr << "Input was:" << std::endl;
        debug(inData, numElements, std::cerr);
    }

    delete[] inData;
    delete[] bucketStartsData;
    delete[] bucketStartsCopy;
    delete[] expected;

    return ok;
}

