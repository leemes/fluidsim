#include "testreduce.h"
#include "util/reduce.h"
#include "clsourceloader.h"
#include <qclcontext.h>

bool TestReduce::run(QCLContext &context, const CLSourceLoader &sourceLoader)
{
#define RUN_TYPE(type) \
    do { if (!runType<cl_##type>(context, sourceLoader, #type)) return false; } while(0)

    // Note: these are DEVICE types and they are prefixed with "cl_" to get a corresponding host type, so "long" for example is 64bit
    RUN_TYPE(int);
    RUN_TYPE(uint);
    RUN_TYPE(char);
    RUN_TYPE(uchar);
    RUN_TYPE(short);
    RUN_TYPE(ushort);
    RUN_TYPE(long);
    RUN_TYPE(ulong);

    return true;

#undef RUN_TYPE
}

template<typename T>
bool TestReduce::runType(QCLContext &context, const CLSourceLoader &sourceLoader,
                         const char *typeName)
{
    QMap<const char*,std::function<T(T,T)>> reduceFunctors;
    reduceFunctors[ReduceExprAdd] = std::plus<T>();
    reduceFunctors[ReduceExprMul] = std::multiplies<T>();
    reduceFunctors[ReduceExprMin] = &qMin<T>;
    reduceFunctors[ReduceExprMax] = &qMax<T>;

    QMap<const char*,const char*> neutralElements;
    neutralElements[ReduceExprAdd] = NeutralElementAdd;
    neutralElements[ReduceExprMul] = NeutralElementMul;
    neutralElements[ReduceExprMin] = NeutralElementMin;
    neutralElements[ReduceExprMax] = NeutralElementMax;

    std::vector<Reduce> testCases;
    for (uint groupSize = 8; groupSize <= 512; groupSize *= 2) {
        for (auto expression : reduceFunctors.keys()) {
            testCases.emplace_back(context, sourceLoader,
                                   typeName, sizeof(T),
                                   expression, neutralElements[expression],
                                   groupSize);
        }
    }

    qDebug("TYPE T = %s", typeName);
    for (Reduce & testCase : testCases) {
        qDebug("  GROUP SIZE = %d, EXPRESSION = '%s', NEUTRAL ELEMENT = %s", testCase.maxGroupSize(), testCase.reduceExpression(), neutralElements[testCase.reduceExpression()]);
        if (!runCase<T>(context, testCase, reduceFunctors[testCase.reduceExpression()]))
            return false;
    }

    return true;
}

template<typename T>
bool TestReduce::runCase(QCLContext &context, Reduce &testCase, std::function<T(T,T)> reduce)
{
#define RUN_SIZE(size) \
    do { if (!runSize<T>(context, testCase, size, reduce)) return false; } while(0)

    // Some random sizes, tiny
    qDebug("    Tiny...");
    for (uint i = 0; i < 10; ++i)
        RUN_SIZE((unsigned)rand() % 64);

    // Some random sizes, small
    qDebug("    Small...");
    for (uint i = 0; i < 10; ++i)
        RUN_SIZE((unsigned)rand() % (1 << 10));

    // A random size, large
    qDebug("    Large...");
    RUN_SIZE((unsigned)rand() % (1 << 20));

    // Some power of two sizes
    qDebug("Powers of two...");
    for (uint e = 1; e <= 24; ++e)
        RUN_SIZE(1 << e);

    return true;

#undef RUN_SIZE
}

template<typename T>
bool TestReduce::runSize(QCLContext &context, Reduce &testCase, uint numElements, std::function<T(T,T)> reduce)
{
    if (numElements == 0)
        return true;

    const uint allocElements = testCase.minBufferSizeForNumElements(numElements);
    qDebug() << "numElements:" << numElements << "--" << "allocated:" << allocElements;

    T *inData = new T[allocElements];
    for (uint i = 0; i < numElements; ++i) {
        inData[i] = rand() % 80;
        // avoid even numbers when multiplying (such a test would be too boring, since the result is almost always 0)
        if (testCase.reduceExpression() == ReduceExprMul && inData[i] % 2 == 0)
            inData[i]++;
    }

    T expected = inData[0];
    for (uint i = 1; i < numElements; ++i)
        expected = reduce(expected, inData[i]);

    QCLBuffer inBuffer = context.createBufferCopy(inData, sizeof(T) * allocElements, QCLMemoryObject::ReadOnly);
    QCLBuffer outBuffer = context.createBufferDevice(sizeof(T) * 1, QCLMemoryObject::WriteOnly);

    testCase(inBuffer, outBuffer, numElements);
    bool ok = verify(inData, inBuffer, numElements)
            && verify(&expected, outBuffer, 1);

    if (!ok) {
        std::cerr << "Input was:" << std::endl;
        debug(inData, numElements, std::cerr);
    }

    delete [] inData;

    return ok;
}

