#include "runtests.h"
#include "testrotate.h"
#include "testscan.h"
#include "testbucketsort.h"
#include "testradixsort.h"
#include "testmultilowerbound.h"
#include "testpartition.h"
#include "teststreamcompaction.h"

#define TEST(Class) do { \
    qDebug("#########   " #Class "   #########"); \
    if (!Class::run(context)) qFatal("Test failed. Aborting."); \
} while(0)



void runTests(CLContext &context)
{
    srand(0);

    TEST(TestRotate);
    TEST(TestScan);
    TEST(TestBucketSort);
    TEST(TestRadixSort);
    TEST(TestMultiLowerBound);
    TEST(TestPartition);
    TEST(TestStreamCompaction);
}

// REMOVED:
//#include "testscantiny.h"
//TEST(TestScanTiny);
//#include "testreduce.h"
//TEST(TestReduce);     // some strange bug with T = uint and reduce = max(a,b)
