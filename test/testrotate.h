#ifndef TESTROTATE_H
#define TESTROTATE_H

#include "abstracttest.h"

class Rotate;

class TestRotate : AbstractTest
{
public:
    static bool run(CLContext &context);

private:
    template<typename T>
    static bool runType(CLContext &context, const char *typeName);

    template<typename T>
    static bool runCase(CLContext &context, Rotate &testCase);

    template<typename T>
    static bool runSize(CLContext &context, Rotate &testCase, QSize size);
};

#endif // TESTROTATE_H
