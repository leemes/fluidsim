#include "testmultilowerbound.h"
#include "util/multilowerbound.h"
#include "clcontext.h"
#include "cleventtree.h"

bool TestMultiLowerBound::run(CLContext &context)
{
    typedef cl_uint T;

    const uint numRanges = 4;
    const uint numSearchesPerRange = 8;
    const uint numSearches = numSearchesPerRange * numRanges;
    uint numElements = 1 << 6;

    MultiLowerBound testCase(context, "uint", QString("element % %1").arg(numSearchesPerRange), numSearchesPerRange);
    auto key = [](T element) { return element % numSearchesPerRange; };

    T *inData = new T[numElements];
    std::srand(0);
    std::generate_n(inData, numElements, []{ return std::rand() / (RAND_MAX / numSearches); });
    std::sort(inData, inData + numElements);
    //std::cout << "input:" << std::endl;
    //debug(inData, numElements);

    //T *debugKeys = new T[numElements];
    //for (uint i = 0; i < numElements; ++i)
    //    debugKeys[i] = key(inData[i]);
    //std::cout << "debugKeys:" << std::endl;
    //debug(debugKeys, numElements);

    T rangeStartsData[numRanges];
    auto higherLevelKey = [&](T element) { return element / numSearchesPerRange; };
    for (uint range = 0; range < numRanges; ++range) {
        rangeStartsData[range] = std::lower_bound(inData, inData + numElements, range * numSearchesPerRange) - inData;
    }
    //std::cout << "range starts:" << std::endl;
    //debug(rangeStartsData, numRanges);

    T expected[numSearches];
    for (uint search = 0; search < numSearches; ++search) {
        expected[search] = std::lower_bound(inData, inData + numElements, search) - inData;
    }
    //std::cout << "expected:" << std::endl;
    //debug(expected, numSearches);

    QCLBuffer inputBuffer             = context.createBufferCopy(inData,          sizeof(T)       * numElements, QCLMemoryObject::ReadOnly);
    QCLBuffer rangeStartsBuffer       = context.createBufferCopy(rangeStartsData, sizeof(cl_uint) * numRanges,   QCLMemoryObject::ReadOnly);
    QCLBuffer lowerBoundsOutputBuffer = context.createBufferDevice(               sizeof(cl_uint) * numSearches, QCLMemoryObject::WriteOnly);

    testCase(inputBuffer, numElements, rangeStartsBuffer, numRanges, lowerBoundsOutputBuffer);

    //std::cout << "actual:" << std::endl;
    //debug<cl_uint,numSearchesPerRange>(lowerBoundsOutputBuffer);

    return verify(expected, lowerBoundsOutputBuffer, numSearches);
}
