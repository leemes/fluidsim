#ifndef TESTSCAN_H
#define TESTSCAN_H

#include "abstracttest.h"

class Scan;

class TestScan : AbstractTest
{
public:
    static bool run(CLContext &context);

private:
    template<typename T>
    static bool runType(CLContext &context, const char *typeName);

    template<typename T>
    static bool runCase(CLContext &context, Scan &testCase);

    template<typename T>
    static bool runSize(CLContext &context, Scan &testCase, uint numElements);
};

#endif // TESTSCAN_H
