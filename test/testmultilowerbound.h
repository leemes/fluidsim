#ifndef TESTMULTILOWERBOUND_H
#define TESTMULTILOWERBOUND_H

#include "abstracttest.h"

class TestMultiLowerBound : public AbstractTest
{
public:
    static bool run(CLContext &context);
};

#endif // TESTMULTILOWERBOUND_H
