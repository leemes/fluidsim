#ifndef TESTBUCKETSORT_H
#define TESTBUCKETSORT_H

#include "abstracttest.h"

class BucketSort;

class TestBucketSort : AbstractTest
{
public:
    static bool run(CLContext &context);

private:
    template<typename T_IN, typename T_OUT>
    static bool runType(CLContext &context,
                        const char *inputTypeName, const char *outputTypeName,
                        const char *keyExpression, const char *outputExpression,
                        std::function<cl_uint(T_IN,cl_uint)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                        std::function<T_IN()> rand);

    template<typename T_IN, typename T_OUT>
    static bool runCase(CLContext &context, BucketSort &testCase,
                        std::function<cl_uint(T_IN)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                        std::function<T_IN()> rand);

    template<typename T_IN, typename T_OUT>
    static bool runSize(CLContext &context, BucketSort &testCase, int numElements,
                        std::function<cl_uint(T_IN)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                        std::function<T_IN()> rand);
};

#endif // TESTBUCKETSORT_H

