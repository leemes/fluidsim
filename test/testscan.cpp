#include "testscan.h"
#include "clcontext.h"
#include "cleventtree.h"
#include "util/scan.h"

bool TestScan::run(CLContext &context)
{
#define RUN_TYPE(type) \
    do { if (!runType<cl_##type>(context, #type)) return false; } while(0)

    // Note: these are DEVICE types and they are prefixed with "cl_" to get a corresponding host type, so "long" for example is 64bit
    RUN_TYPE(int);
    RUN_TYPE(uint);
    RUN_TYPE(char);
    RUN_TYPE(uchar);
    RUN_TYPE(short);
    RUN_TYPE(ushort);
    RUN_TYPE(long);
    RUN_TYPE(ulong);

    return true;

#undef RUN_TYPE
}

template<typename T>
bool TestScan::runType(CLContext &context, const char *typeName)
{
    qDebug("TYPE T = %s", typeName);
    for (uint groupSize = 8; groupSize <= 512; groupSize *= 2)
    {
        Scan testInclusive(context, typeName, sizeof(T), Scan::Inclusive, groupSize);
        Scan testExclusive(context, typeName, sizeof(T), Scan::Exclusive, groupSize);

        if (!runCase<T>(context, testInclusive))
            return false;
        if (!runCase<T>(context, testExclusive))
            return false;
    }

    return true;
}

template<typename T>
bool TestScan::runCase(CLContext &context, Scan &testCase)
{
    qDebug("  GROUP SIZE = %d, MODE = %s", testCase.maxGroupSize(), testCase.mode() == Scan::Inclusive ? "INCLUSIVE" : "EXCLUSIVE");

#define RUN_SIZE(size) \
    do { if (!runSize<T>(context, testCase, size)) return false; } while(0)

    // Some random sizes, tiny
    qDebug("    Tiny...");
    for (uint i = 0; i < 10; ++i)
        RUN_SIZE((unsigned)rand() % 64);

    // Some random sizes, small
    qDebug("    Small...");
    for (uint i = 0; i < 10; ++i)
        RUN_SIZE((unsigned)rand() % (1 << 10));

    // A random size, large
    qDebug("    Large...");
    RUN_SIZE((unsigned)rand() % (1 << 20));

    // Some power of two sizes
    qDebug("    Powers of two...");
    for (uint e = 1; e <= 24; ++e)
        RUN_SIZE(1 << e);

    return true;

#undef RUN_SIZE
}

template<typename T>
bool TestScan::runSize(CLContext &context, Scan &testCase, uint numElements)
{
    const uint allocElements = testCase.minBufferSizeForNumElements(numElements);
    //qDebug() << "numElements:" << numElements << "--" << "allocated:" << allocElements;

    T *inData = new T[allocElements];
    for (uint i = 0; i < numElements; ++i) {
        inData[i] = rand();
    }

    T *expected = new T[numElements + 1];
    T currentSum = 0;
    if (testCase.mode() == Scan::Inclusive) {
        for (uint i = 0; i < numElements; ++i)
            expected[i] = (currentSum += inData[i]);
    } else {
        for (uint i = 0; i < numElements; ++i) {
            expected[i] = currentSum;
            currentSum += inData[i];
        }
    }

    QCLBuffer inBuffer = context.createBufferCopy(inData, sizeof(T) * allocElements, QCLMemoryObject::ReadWrite);
    //QCLBuffer outBuffer = context.createBufferDevice(sizeof(T) * allocElements, QCLMemoryObject::WriteOnly);

    testCase(inBuffer, inBuffer, numElements);
    bool ok = verify(expected, inBuffer, numElements);

    delete [] inData;
    delete [] expected;

    return ok;
}

