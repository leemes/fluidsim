#include "testrotate.h"
#include "clcontext.h"
#include "cleventtree.h"
#include "util/rotate.h"

bool TestRotate::run(CLContext &context)
{
#define RUN_TYPE(type) \
    do { if (!runType<cl_##type>(context, #type)) return false; } while(0)

    // Note: these are DEVICE types and they are prefixed with "cl_" to get a corresponding host type, so "long" for example is 64bit
    RUN_TYPE(char);
    RUN_TYPE(uint);
    RUN_TYPE(long);
    RUN_TYPE(float);

    return true;

#undef RUN_TYPE
}

template<typename T>
bool TestRotate::runType(CLContext &context, const char *typeName)
{
    qDebug("TYPE T = %s", typeName);

    std::vector<Rotate> testCases;
    for (uint groupW = 4; groupW <= 1024; groupW *= 2) {
        for (uint groupH = 4; groupH <= 1024 / groupW; groupH *= 2) {
            QSize groupSize(groupW, groupH);
            testCases.emplace_back(context, typeName, Rotate::RotateCW, groupSize);
            testCases.emplace_back(context, typeName, Rotate::RotateCCW, groupSize);
            testCases.emplace_back(context, typeName, Rotate::Transpose, groupSize);
        }
    }

    for (Rotate & testCase : testCases) {
        qDebug("  GROUP SIZE = %d x %d, MODE = %s", testCase.maxGroupSize().width(), testCase.maxGroupSize().height(),
               (testCase.mode() == Rotate::RotateCW) ? "ROTATE_CW" : (testCase.mode() == Rotate::RotateCCW) ? "ROTATE_CCW" : "TRANSPOSE");
        if (!runCase<T>(context, testCase))
            return false;
    }

    return true;
}

template<typename T>
bool TestRotate::runCase(CLContext &context, Rotate &testCase)
{
#define RUN_SIZE(w, h) \
    do { if (!runSize<T>(context, testCase, QSize(w, h))) return false; } while(0)

    auto r = [](uint n){ return (unsigned)rand() % n; };

    // A random size
    RUN_SIZE(r(1 << 10), r(1 << 10));

    // A random power of two size
    RUN_SIZE(1 << r(10), 1 << r(10));

    // Large in X
    RUN_SIZE(1 << 16, 1 << r(6));
    // Large in Y
    RUN_SIZE(1 << r(6), 1 << 16);

    return true;

#undef RUN_SIZE
}

template<typename T>
bool TestRotate::runSize(CLContext &context, Rotate &testCase, QSize size)
{
    //qDebug() << "    size:" << size.width() << "x" << size.height();

    const uint numElements = size.width() * size.height();
    if (numElements == 0)
        return true;

    T *inData = new T[numElements];
    for (uint i = 0; i < numElements; ++i) {
        inData[i] = rand() % 100;
    }

    T *expected = new T[numElements];
    if (testCase.mode() == Rotate::RotateCW) {
        for (int x = 0; x < size.width(); ++x)
            for (int y = 0; y < size.height(); ++y)
                expected[x * size.height() + (size.height() - y - 1)] = inData[x + size.width() * y];
    } else if (testCase.mode() == Rotate::RotateCCW) {
        for (int x = 0; x < size.width(); ++x)
            for (int y = 0; y < size.height(); ++y)
                expected[(size.width() - x - 1) * size.height() + y] = inData[x + size.width() * y];
    } else if (testCase.mode() == Rotate::Transpose) {
        for (int x = 0; x < size.width(); ++x)
            for (int y = 0; y < size.height(); ++y)
                expected[x * size.height() + y] = inData[x + size.width() * y];
    }

    QCLBuffer inBuffer = context.createBufferCopy(inData, sizeof(T) * numElements, QCLMemoryObject::ReadWrite);
    QCLBuffer outBuffer = context.createBufferDevice(sizeof(T) * numElements, QCLMemoryObject::WriteOnly);

    testCase(inBuffer, outBuffer, size);
    bool ok = verify(expected, outBuffer, numElements);

    delete [] inData;
    delete [] expected;
    return ok;

    return true;
}

