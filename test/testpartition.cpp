#include "testpartition.h"
#include "util/partition.h"
#include "clcontext.h"
#include "cleventtree.h"

bool TestPartition::run(CLContext &context)
{
    typedef cl_uint T;

    const uint keyNumBits = 24;
    const uint numPartitions = 1 << keyNumBits;
    uint numElements = 1 << 20;

    Partition testCase(context, "uint", sizeof(cl_uint), QString("element / 10"), keyNumBits);
    auto key = [](T element) { return element / 10; };
    uint allocElements = testCase.minBufferSizeForNumElements(numElements);

    // Generate and upload to device (blocking)
    T *inData = new T[allocElements];
    std::srand(0);
    std::generate_n(inData, numElements, [=]{ return std::rand() / (RAND_MAX / (numPartitions * 10) + 1); });
    QCLBuffer inputBuffer = context.createBufferCopy(inData, sizeof(T) * allocElements, QCLMemoryObject::ReadOnly);
    //std::cout << "input:" << std::endl;
    //debug(inData, numElements);

    // Sort
    std::stable_sort(inData, inData + numElements, [&](T a, T b) { return key(a) < key(b); });
    //std::cout << "input (sorted):" << std::endl;
    //debug(inData, numElements);

    // Construct key array, because std::lower_bound can't operate with a functor
    uint *keysData = new uint[numElements];
    for (uint i = 0; i < numElements; ++i) {
        auto k = key(inData[i]);
        Q_ASSERT(k < numPartitions);
        keysData[i] = k;
    }
    //std::cout << "input (sorted, keys only):" << std::endl;
    //debug(keysData, numElements);

    // Compute expected partition starts (excl. scan with one more entry containing the total sum)
    uint *expected = new uint[numPartitions + 1];
    for (uint p = 0; p < numPartitions; ++p) {
        expected[p] = std::lower_bound(keysData, keysData + numElements, p) - keysData;
    }
    expected[numPartitions] = numElements;


    QCLBuffer outputBuffer                = context.createBufferDevice(sizeof(T)       * allocElements,       QCLMemoryObject::ReadWrite);
    QCLBuffer partitionStartsOutputBuffer = context.createBufferDevice(sizeof(cl_uint) * (numPartitions + 1), QCLMemoryObject::ReadWrite);

    testCase(inputBuffer, outputBuffer, numElements, partitionStartsOutputBuffer);

    //std::cout << "actual:" << std::endl;
    //debug<cl_uint>(partitionStartsOutputBuffer);

    bool ok = verify(inData, outputBuffer, numElements)
           && verify(expected, partitionStartsOutputBuffer, numPartitions + 1);

    delete[] inData;
    delete[] keysData;
    delete[] expected;

    return ok;
}
