#include "testradixsort.h"
#include "util/radixsort.h"
#include "debug.h"
#include "clcontext.h"
#include "cleventtree.h"


bool TestRadixSort::run(CLContext &context)
{
    // TODO: multiple test cases: types, sizes, group sizes

    QString typeName = "char";
    size_t typeSize = sizeof(cl_char);
    typedef cl_char T;


    QVector<RadixSort::Step> steps;
    steps << RadixSort::Step{ "element >= 0", 1 };              // Sign
    steps << RadixSort::Step{ "abs(element / 100) % 100", 4 };  // 100-digit
    steps << RadixSort::Step{ "abs(element / 10) % 10", 4 };    //  10-digit
    steps << RadixSort::Step{ "abs(element) % 10", 4 };         //   1-digit

    RadixSort testCase(context, typeName, typeSize, steps);

    uint numElements = 1 << 20;
    uint allocElements = testCase.minBufferSizeForNumElements(numElements);

    T *inData = new T[allocElements];
    for (uint i = 0; i < numElements; ++i) {
        inData[i] = (T)rand();
    }
    //debug(inData, numElements);

    QCLBuffer inBuffer = context.createBufferCopy(inData, sizeof(T) * allocElements, QCLMemoryObject::ReadOnly);
    QCLBuffer outBuffer = context.createBufferDevice(sizeof(T) * allocElements, QCLMemoryObject::WriteOnly);

    testCase(inBuffer, outBuffer, numElements);

    delete[] inData;

    return true;
}
