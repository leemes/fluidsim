#ifndef RUNTESTS_H
#define RUNTESTS_H

class CLContext;

void runTests(CLContext &context);

#endif // RUNTESTS_H
