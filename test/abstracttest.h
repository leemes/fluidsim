#ifndef ABSTRACTTEST_H
#define ABSTRACTTEST_H

#include <qclbuffer.h>
#include <iostream>
#include "debug.h"

class QCLContext;
class CLSourceLoader;
class CLContext;


namespace detail {
    template<typename T>
    inline bool verify(const T &expected, const T &actual) {
        return expected == actual;
    }
    template<>
    inline bool verify<float>(const float &expected, const float &actual) {
        return qFuzzyCompare(expected, actual);
    }
    template<>
    inline bool verify<double>(const double &expected, const double &actual) {
        return qFuzzyCompare(expected, actual);
    }
}


class AbstractTest
{
public:
    static bool run(QCLContext &context, const CLSourceLoader &sourceLoader) {
        Q_UNUSED(context)
        Q_UNUSED(sourceLoader)
        qFatal("Please reimplement me!");
        return false;
    }
    static bool run(CLContext &context) {
        Q_UNUSED(context)
        qFatal("Please reimplement me!");
        return false;
    }

protected:
    inline AbstractTest(){}

    template<typename T>
    static bool verify(const T *expected, QCLBuffer &actual, uint numElements) {
        const T *mappedActual = reinterpret_cast<T*>(actual.map(QCLMemoryObject::ReadOnly));
        for (uint i = 0; i < numElements; ++i) {
            if (!detail::verify(expected[i], mappedActual[i]))
            {
                std::cerr << "Verification failed at index " << i << ": ";
                std::cerr << "Expected " << expected[i] << ", Actual " << mappedActual[i] << std::endl;

                std::cerr << "The whole expected results:" << std::endl;
                debug(expected, numElements, std::cerr);
                std::cerr << "The whole actual results:" << std::endl;
                debug(mappedActual, numElements, std::cerr);

                actual.unmap(const_cast<T*>(mappedActual));
                return false;
            }
        }
        actual.unmap(const_cast<T*>(mappedActual));
        return true;
    }
};

#endif // ABSTRACTTEST_H
