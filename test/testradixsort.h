#ifndef TESTRADIXSORT_H
#define TESTRADIXSORT_H

#include "abstracttest.h"

class RadixSort;

class TestRadixSort : AbstractTest
{
public:
    static bool run(CLContext &context);

private:
    /*
    template<typename T_IN, typename T_OUT>
    static bool runType(QCLContext &context, const CLSourceLoader &sourceLoader,
                        const char *inputTypeName, const char *outputTypeName,
                        const char *keyExpression, const char *outputExpression,
                        std::function<cl_uint(T_IN,cl_uint)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                        std::function<T_IN()> rand);

    template<typename T_IN, typename T_OUT>
    static bool runCase(QCLContext &context, BucketSort &testCase,
                        std::function<cl_uint(T_IN)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                        std::function<T_IN()> rand);

    template<typename T_IN, typename T_OUT>
    static bool runSize(QCLContext &context, BucketSort &testCase, int numElements,
                        std::function<cl_uint(T_IN)> key, std::function<T_OUT(T_IN,cl_uint)> output,
                        std::function<T_IN()> rand);
                        */
};

#endif // TESTRADIXSORT_H
