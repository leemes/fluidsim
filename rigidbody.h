#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include <QObject>
#include <QVector3D>
#include <QColor>

class RigidBody : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector3D position READ position WRITE moveTo NOTIFY moved)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(bool inverted READ isInverted WRITE setInverted NOTIFY invertedChanged)
    Q_PROPERTY(qreal reflectNormal READ reflectNormal WRITE setReflectNormal NOTIFY reflectNormalChanged)
    Q_PROPERTY(qreal reflectTangential READ reflectTangential WRITE setReflectTangential NOTIFY reflectTangentialChanged)

    QVector3D m_position = { .5, .5, .5 };
    QColor m_color = Qt::black;
    bool m_inverted = false;
    qreal m_reflectNormal = .7;
    qreal m_reflectTangential = .9;

public:
    explicit RigidBody(QObject *parent = 0) : QObject(parent) {}

    QVector3D position() const { return m_position; }
    QColor color() const { return m_color; }
    bool isInverted() const { return m_inverted; }
    qreal reflectNormal() const { return m_reflectNormal; }
    qreal reflectTangential() const { return m_reflectTangential; }

signals:
    void changed();
    void moved(QVector3D arg);
    void colorChanged(QColor arg);
    void invertedChanged(bool arg);
    void reflectNormalChanged(qreal arg);
    void reflectTangentialChanged(qreal arg);

public slots:
    void moveTo(QVector3D arg) {
        if (m_position != arg) {
            m_position = arg;
            emit moved(arg);
            emit changed();
        }
    }
    void setColor(QColor arg) {
        if (m_color != arg) {
            m_color = arg;
            emit colorChanged(arg);
            emit changed();
        }
    }
    void setInverted(bool arg) {
        if (m_inverted != arg) {
            m_inverted = arg;
            emit invertedChanged(arg);
            emit changed();
        }
    }
    void setReflectNormal(qreal arg) {
        Q_ASSERT(arg >= 0.0 && arg <= 1.0);
        if (m_reflectNormal != arg) {
            m_reflectNormal = arg;
            emit reflectNormalChanged(arg);
        }
    }
    void setReflectTangential(qreal arg) {
        Q_ASSERT(arg >= 0.0 && arg <= 1.0);
        if (m_reflectTangential != arg) {
            m_reflectTangential = arg;
            emit reflectTangentialChanged(arg);
        }
    }
};


class RigidBox : public RigidBody
{
    Q_OBJECT
    Q_PROPERTY(QVector3D size READ size WRITE setSize NOTIFY sizeChanged)

    QVector3D m_size;

public:
    explicit RigidBox(QObject *parent = 0) : RigidBody(parent) {}

    QVector3D size() const { return m_size; }

signals:
    void sizeChanged(QVector3D arg);

public slots:
    void setSize(QVector3D arg) {
        if (m_size != arg) {
            m_size = arg;
            emit sizeChanged(arg);
            emit changed();
        }
    }
};


class RigidSphere : public RigidBody
{
    Q_OBJECT
    Q_PROPERTY(qreal radius READ radius WRITE setRadius NOTIFY radiusChanged)

    qreal m_radius;

public:
    explicit RigidSphere(QObject *parent = 0) : RigidBody(parent) {}

    qreal radius() const { return m_radius; }

signals:
    void radiusChanged(qreal arg);

public slots:
    void setRadius(qreal arg) {
        if (m_radius != arg) {
            m_radius = arg;
            emit radiusChanged(arg);
            emit changed();
        }
    }
};

#endif // RIGIDBODY_H
