#-------------------------------------------------
#
# Project created by QtCreator 2013-05-20T15:11:01
#
#-------------------------------------------------

!greaterThan(QT_MAJOR_VERSION, 4) {
    error(This project requires at least Qt 5.0.0.)
}

QT += core gui widgets quick

TARGET = fluidsim
TEMPLATE = app

DEFINES += M_PI=3.14159265358979323846
DEFINES += M_PI_2=1.57079632679489661923


unix {
    QTOPENCL = $(HOME)/qt/qtopencl
} else {
    QTOPENCL = C:/Qt/qtopencl
}
!exists ($${QTOPENCL}) {
    error(This project depends on QtOpenCL with slight modifications. Please clone git://gitorious.org/~leemes/qt-labs/leemes-opencl.git and build according to README. Make sure you build this lib with the same Qt version than you\'ll build this project. Then set the QTOPENCL variable in this .pro file accordingly.)
}
LIBS += -L$${QTOPENCL}/lib
LIBS += -L$${QTOPENCL}/bin
include($${QTOPENCL}/src/openclgl/openclgl_dep.pri)

QMAKE_CXXFLAGS += -std=c++11 -ffast-math


SOURCES += main.cpp \
    clcontext.cpp \
    clsourceloader.cpp \
    cleventtree.cpp \
    util/multilowerbound.cpp \
    util/partition.cpp \
    util/streamcompaction.cpp \
    util/transform.cpp \
    util/fill.cpp \
    util/bucketsort.cpp \
    util/radixsort.cpp \
    util/move.cpp \
    util/rotate.cpp \
    util/scan.cpp \
    test/runtests.cpp \
    test/testbucketsort.cpp \
    test/testscan.cpp \
    test/testradixsort.cpp \
    test/testrotate.cpp \
    test/testmultilowerbound.cpp \
    test/testpartition.cpp \
    test/teststreamcompaction.cpp \
    benchmark/guibenchmark.cpp \
    benchmark/runbenchmark.cpp \
    benchmark/abstractbenchmark.cpp \
    benchmark/benchmarkradixsort.cpp \
    benchmark/benchmarkpartition.cpp \
    benchmark/benchmarkgrid.cpp \
    benchmark/benchmarkbucketsort.cpp \
    grid.cpp \
    simulation.cpp \
    emitter.cpp \
    particlesystem.cpp \
    fluidsimulation.cpp

HEADERS += \
    clcontext.h \
    clsourceloader.h \
    cleventtree.h \
    cl/cldefs.h \
    cl/end_shared.h \
    cl/begin_shared.h \
    debug.h \
    util.h \
    util/multilowerbound.h \
    util/partition.h \
    util/streamcompaction.h \
    util/transform.h \
    util/fill.h \
    util/bucketsort.h \
    util/radixsort.h \
    util/move.h \
    util/rotate.h \
    util/scan.h \
    test/runtests.h \
    test/testbucketsort.h \
    test/testscan.h \
    test/testradixsort.h \
    test/testrotate.h \
    test/testmultilowerbound.h \
    test/testpartition.h \
    test/teststreamcompaction.h \
    benchmark/guibenchmark.h \
    benchmark/runbenchmark.h \
    benchmark/abstractbenchmark.h \
    benchmark/benchmarkradixsort.h \
    benchmark/benchmarkpartition.h \
    benchmark/benchmarkgrid.h \
    benchmark/benchmarkbucketsort.h \
    size3d.h \
    rigidbody.h \
    grid.h \
    simulation.h \
    emitter.h \
    particlesystem.h \
    fluidsimulation.h



FORMS += \
    benchmark/guibenchmark.ui

OTHER_FILES += \
    cl/rand.h \
    cl/particle.h \
    cl/simulationconstants.h \
    cl/simulation.cl \
    cl/rigidbody.h \
    cl/grid.h \
    cl/util/scan.cl \
    cl/util/bucketsort.cl \
    cl/util/move.cl \
    cl/util/rotate.cl \
    cl/util/multilowerbound.cl \
    cl/util/partition.cl \
    cl/util/transform.cl \
    cl/util/fill.cl \
    qml/main.qml \
    qml/presentation/presentation.qml \
    qml/presentation/algorithm/Procedure.qml \
    qml/presentation/algorithm/Scope.qml \
    qml/presentation/algorithm/Algorithm.qml \
    qml/presentation/algorithm/Do.qml \
    qml/presentation/algorithm/If.qml \
    qml/presentation/algorithm/ElseIf.qml \
    qml/presentation/algorithm/Else.qml \
    qml/presentation/algorithm/ForEach.qml \
    qml/presentation/components/Slide.qml \
    qml/presentation/components/Presentation.qml \
    qml/presentation/components/ParText.qml \
    qml/presentation/components/ParTab.qml \
    qml/presentation/components/ParSpace.qml \
    qml/presentation/components/ParImage.qml \
    qml/presentation/components/Par.qml \
    qml/presentation/components/List.qml \
    qml/presentation/components/Group.qml \
    qml/presentation/components/Enum.qml \
    qml/presentation/components/Block.qml \
    qml/presentation/components/AbstractList.qml \
    qml/presentation/template/logo.png \
    qml/presentation/template/institutelogo.png \
    qml/presentation/template/TitleSlide.qml \
    qml/presentation/template/Slide.qml \
    qml/presentation/template/Presentation.qml \
    qml/presentation/template/List.qml \
    qml/presentation/template/Block.qml \
    qml/presentation/template/Highlight.qml \
    qml/presentation/figures/FieldBasic.qml \
    qml/presentation/figures/particle.png \
    qml/presentation/figures/DemoParticleSystem.qml \
    qml/presentation/figures/ParticleKernel.qml \
    qml/presentation/figures/ParticleSphere.qml \
    qml/presentation/figures/KernelFieldGrid.qml \
    qml/presentation/figures/GridFigure.qml \
    qml/presentation/figures/Line.qml \
    qml/presentation/helper/Slider.qml \
    qml/presentation/Demo.qml \
    qml/presentation/DemoButton.qml \
    qml/presentation/DemoButtons.qml \
    qml/presentation/demos/DemoPourFloor.qml \
    qml/presentation/demos/DemoBanner.qml \
    qml/presentation/demos/DemoRain.qml \
    qml/presentation/demos/DemoBeach.qml

unix {
    system(./makeqrc.sh -qrc cl.qrc -base cl)
    system(./makeqrc.sh -qrc qml.qrc -base qml)
}
RESOURCES += cl.qrc qml.qrc
