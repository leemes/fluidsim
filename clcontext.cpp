#include "clcontext.h"
#include "clsourceloader.h"
#include <qclcontext.h>
#include <qclcontextgl.h>
#include <QDebug>

CLContext::CLContext(QString sourceFileBase, Options options) :
    m_sourceLoader(new CLSourceLoader(sourceFileBase)),
    m_context((options & GlSharing) ? new QCLContextGL() : new QCLContext()),
    m_contextGL((options & GlSharing) ? static_cast<QCLContextGL*>(m_context.data()) : nullptr),
    m_options(options),
    m_compilerFlags(NullCompilerFlags)
{
    if (options & GlSharing) {
        if (!QGLContext::currentContext())
            qFatal("No OpenGL context was current when trying to create an OpenCLGL context.");
        qDebug() << 0 << QThread::currentThread();
        if (!m_contextGL->create())
            qFatal("This application requires OpenCLGL, but creating an OpenCLGL context failed.");
    } else {
        if (!m_context->create())
            qFatal("This application requires OpenCL, but creating an OpenCL context failed.");
    }

    qDebug("Using OpenCL device '%s'.", qPrintable(m_context->defaultDevice().name()));

    if ((options & GlSharing) && !m_contextGL->supportsObjectSharing())
        qFatal("This application requires OpenCL / OpenGL object sharing capabilities,\n"
               "but your graphics card / driver doesn't seem to support it.");

    const uint queuePropertiesMask = Profiling | OutOfOrder;
    const uint queueProperties = options & queuePropertiesMask;
    if (queueProperties)
        m_context->setCommandQueue(m_context->createCommandQueue(queueProperties));
}

CLContext::~CLContext()
{
}

void CLContext::setCompilerFlags(CompilerFlags flags)
{
    m_compilerFlags = flags;
}

void CLContext::setCompilerFlag(CompilerFlags flag)
{
    m_compilerFlags = (CompilerFlags)(m_compilerFlags | flag);
}

void CLContext::unsetCompilerFlag(CompilerFlags flag)
{
    m_compilerFlags = (CompilerFlags)(m_compilerFlags & ~flag);
}

void CLContext::addAutoInclude(QString typeName, QString fileName)
{
    m_sourceLoader->addAutoInclude(typeName, fileName);
}

QCLProgram CLContext::createProgram(const QString &relativeFileName)
{
    return createProgram(relativeFileName, "");
}

QCLProgram CLContext::createProgram(const QString &relativeFileName, const QByteArray &prependedCode)
{
    QByteArray sourceCode = m_sourceLoader->loadSource(relativeFileName, prependedCode);
    return m_context->createProgramFromSourceCode(sourceCode);
}

QCLProgram CLContext::buildProgram(const QString &relativeFileName, const QMap<QString, QString> &defineMacros)
{
    return buildProgram(relativeFileName, "", defineMacros);
}

QCLProgram CLContext::buildProgram(const QString &relativeFileName, const QByteArray &prependedCode, const QMap<QString, QString> &defineMacros)
{
    QString buildOptions;
    for (QString macro : defineMacros.keys()) {
        buildOptions += "-D " + macro + "=" + defineMacros[macro] + " ";
    }
    buildOptions += compilerFlagsToString(m_compilerFlags);
    // Prepend the code with auto-includes based on the defined macros
    QByteArray simulateCodeForDefines = "/*\n" + QStringList(defineMacros.values()).join("\n").toUtf8() + "*/\n";
    QCLProgram program = createProgram(relativeFileName, simulateCodeForDefines + prependedCode);
    //qDebug() << program.sourceCode();
    if(!program.build(buildOptions))
        qFatal("Failed to build program \"%s\".", qPrintable(relativeFileName));
    return program;
}

QCLBuffer CLContext::createBufferDevice(size_t size, QCLMemoryObject::Access access)
{
    return m_context->createBufferDevice(size, access);
}

QCLBuffer CLContext::createBufferHost(void *data, size_t size, QCLMemoryObject::Access access)
{
    return m_context->createBufferHost(data, size, access);
}

QCLBuffer CLContext::createBufferCopy(const void *data, size_t size, QCLMemoryObject::Access access)
{
    return m_context->createBufferCopy(data, size, access);
}

QCLCommandQueue CLContext::commandQueue()
{
    return m_context->commandQueue();
}

QCLImage2D CLContext::createTexture2D(uint texture, QCLMemoryObject::Access access)
{
    if (m_contextGL)
        return m_contextGL->createTexture2D(texture, access);
    else {
        qWarning("This CLContext is not set up for OpenGL object sharing, hence `createTexture2D` is not available.");
        return QCLImage2D();
    }
}

QCLBuffer CLContext::createGLBuffer(uint buffer, QCLMemoryObject::Access access)
{
    if (m_contextGL)
        return m_contextGL->createGLBuffer(buffer, access);
    else {
        qWarning("This CLContext is not set up for OpenGL object sharing, hence `createGLBuffer` is not available.");
        return QCLBuffer();
    }
}

QCLBuffer CLContext::createGLBuffer(const QGLBuffer &buffer, QCLMemoryObject::Access access)
{
    return createGLBuffer(buffer.bufferId(), access);
}

QCLEvent CLContext::acquire(const QCLMemoryObject &mem)
{
    if (m_contextGL)
        return m_contextGL->acquire(mem);
    else {
        qWarning("This CLContext is not set up for OpenGL object sharing, hence `acquire` is not available.");
        return QCLEvent();
    }
}

QCLEvent CLContext::release(const QCLMemoryObject &mem)
{
    if (m_contextGL)
        return m_contextGL->release(mem);
    else {
        qWarning("This CLContext is not set up for OpenGL object sharing, hence `release` is not available.");
        return QCLEvent();
    }
}

QString CLContext::compilerFlagToString(CLContext::CompilerFlags flag)
{
    switch (flag) {
    case OptimizationMadEnable:         return "-cl-mad-enable";
    case OptimizationNoSignedZeros:     return "-cl-no-signed-zeros";
    case OptimizationUnsafeMath:        return "-cl-unsafe-math-optimizations";
    case OptimizationFiniteMathOnly:    return "-cl-finite-math-only";
    case OptimizationFastRelaxedMath:   return "-cl-fast-relaxed-math";
    case OptimizationSinglePrecConst:   return "-cl-single-precision-constant";
    case OptimizationDenormsAreZero:    return "-cl-denorms-are-zero";
    case OptimizationStrictAliasing:    return "-cl-strict-aliasing";
    default:
        return QString();
    }
}

QString CLContext::compilerFlagsToString(CLContext::CompilerFlags flags)
{
    QStringList strings;
    for (int bit = 0; bit < 32; ++bit) {
        if (flags & (1 << bit)) {
            strings << compilerFlagToString((CompilerFlags)(1 << bit));
        }
    }
    return strings.join(" ");
}

void CLContext::finish()
{
    m_context->finish();
}
