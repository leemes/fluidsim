#include "abstractbenchmark.h"
#include <QElapsedTimer>
#include "clcontext.h"


AbstractBenchmark::AbstractBenchmark(CLContext &context, QObject *parent) :
    QObject(parent),
    m_context(context),
    m_events("")
{
}

double AbstractBenchmark::runBenchmark()
{
    uint numIterations = suggestedNumIterations();
    Q_ASSERT(numIterations != 0);
    qDebug("numIterations = %d", numIterations);

    // Prepare algorithm and execute once to make sure everything is really prepared
    prepare();
    execute();

    // Start time measuring
    context().finish();
    QElapsedTimer timer;
    timer.start();

    // Execute algorithm numIterations times
    for (uint i = 0; i < numIterations; ++i) {
        m_events = execute();
    }

    // Stop time measuring
    context().finish();
    double secondsElabsed = (double)timer.nsecsElapsed() / 1.0e9;

    // Break down the timing on a single execution (average time per execution)
    return secondsElabsed / numIterations;
}
