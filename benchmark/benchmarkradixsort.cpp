#include "benchmarkradixsort.h"
#include "util/radixsort.h"
#include "clcontext.h"
#include "cleventtree.h"
#include "util.h"


BenchmarkRadixSort::BenchmarkRadixSort(CLContext &context, QObject *parent) :
    AbstractBenchmark(context, parent),
    m_numElements(numElements_min()),
    m_numLevels(numLevels_min()),
    m_numBitsPerLevel(numBitsPerLevel_min())
{
}

BenchmarkRadixSort::~BenchmarkRadixSort()
{
}


void BenchmarkRadixSort::prepare()
{
    m_benchmark.reset(makeImpl());

    uint allocElements = m_benchmark->minBufferSizeForNumElements(m_numElements);
    cl_float8 *data = new cl_float8[allocElements];
    std::generate_n(data, allocElements, []{ return cl_float8{frand(), frand(), frand(), frand(), frand(), frand(), frand(), frand()}; });

    m_input = context().createBufferCopy(data, sizeof(*data)*allocElements, QCLMemoryObject::ReadOnly);
    m_output = context().createBufferDevice(sizeof(*data)*allocElements, QCLMemoryObject::WriteOnly);
    m_benchmark->allocateAuxiliaryBuffers(m_numElements);

    delete[] data;
}

CLEventTree BenchmarkRadixSort::execute()
{
    return (*m_benchmark)(m_input, m_output, m_numElements);
}

RadixSort *BenchmarkRadixSort::makeImpl()
{
    QVector<RadixSort::Step> steps;
    for (uint level = 0; level < m_numLevels; ++level)
        steps << RadixSort::Step{ "(uint)(element.s" + QString::number(level % 8) + " * NUM_BUCKETS)", (int)m_numBitsPerLevel };
    return new RadixSort(context(), "float8", sizeof(cl_float8), steps);
}
