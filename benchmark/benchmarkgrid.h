#ifndef BENCHMARKGRID_H
#define BENCHMARKGRID_H

#include "abstractbenchmark.h"
#include <QSharedPointer>

class Grid;

class BenchmarkGrid : public AbstractBenchmark
{
    Q_OBJECT
    Q_PROPERTY(uint numElements READ numElements WRITE setNumElements)
    Q_PROPERTY(uint depth READ depth WRITE setDepth)
    Q_CLASSINFO("numElements", "IncreaseExponential")

public:
    explicit BenchmarkGrid(CLContext &context, QObject *parent = 0);
    ~BenchmarkGrid();

    uint suggestedNumIterations() const override { return qBound(3U, 10000000U / (m_numElements * m_depth), 100U); }

public slots:
    void setNumElements(uint arg) { m_numElements = arg; }
    uint numElements() const { return m_numElements; }
    uint numElements_min() const { return 1U; }
    uint numElements_max() const { return 1U << 20; }

    void setDepth(uint arg) { m_depth = arg; }
    uint depth() const { return m_depth; }
    uint depth_min() const { return 3U; }
    uint depth_max() const { return 8U; }

protected:
    void prepare() override;
    CLEventTree execute() override;

private:
    uint m_numElements;
    uint m_depth;
    QScopedPointer<Grid> m_grid;
    QCLBuffer m_input, m_output, m_cellStartsOutput, m_usedCellsOutput;

    Grid *makeImpl();
};

#endif // BENCHMARKGRID_H
