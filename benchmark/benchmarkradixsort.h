#ifndef BENCHMARKRADIXSORT_H
#define BENCHMARKRADIXSORT_H

#include "abstractbenchmark.h"

class CLEventTree;
class RadixSort;

class BenchmarkRadixSort : public AbstractBenchmark
{
    Q_OBJECT
    Q_PROPERTY(uint numElements     READ numElements     WRITE setNumElements)
    Q_PROPERTY(uint numLevels       READ numLevels       WRITE setNumLevels)
    Q_PROPERTY(uint numBitsPerLevel READ numBitsPerLevel WRITE setNumBitsPerLevel)
    Q_CLASSINFO("numElements", "IncreaseExponential")

public:
    explicit BenchmarkRadixSort(CLContext & context, QObject *parent = 0);
    ~BenchmarkRadixSort();

    uint suggestedNumIterations() const override { return qBound(1U, 10000000U / (m_numElements * m_numLevels), 1000U); }

    void prepare() override;
    CLEventTree execute() override;

public slots:
    uint groupSize_min() const { return 1U; }
    uint groupSize_max() const { return 1U; }

    void setNumElements(uint arg) { m_numElements = arg; }
    uint numElements() const { return m_numElements; }
    uint numElements_min() const { return 1U; }
    uint numElements_max() const { return 1U << 24; }

    void setNumLevels(uint arg) { m_numLevels = arg; }
    uint numLevels() const { return m_numLevels; }
    uint numLevels_min() const { return 2U; }
    uint numLevels_max() const { return 31U / m_numBitsPerLevel; }

    void setNumBitsPerLevel(uint arg) { m_numBitsPerLevel = arg; }
    uint numBitsPerLevel() const { return m_numBitsPerLevel; }
    uint numBitsPerLevel_min() const { return 1U; }
    uint numBitsPerLevel_max() const { return qMin(6U, 31U / m_numLevels); }

private:
    uint m_numElements;
    uint m_numLevels;
    uint m_numBitsPerLevel;
    QScopedPointer<RadixSort> m_benchmark;
    QCLBuffer m_input, m_output;

    RadixSort *makeImpl();
};

#endif // BENCHMARKRADIXSORT_H
