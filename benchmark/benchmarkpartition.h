#ifndef BENCHMARKPARTITION_H
#define BENCHMARKPARTITION_H

#include "abstractbenchmark.h"
#include <QSharedPointer>

class Partition;

class BenchmarkPartition : public AbstractBenchmark
{
    Q_OBJECT
    Q_PROPERTY(uint numElements READ numElements WRITE setNumElements)
    Q_PROPERTY(uint numBits READ numBits WRITE setNumBits)
    Q_CLASSINFO("numElements", "IncreaseExponential")

public:
    explicit BenchmarkPartition(CLContext &context, QObject *parent = 0);
    ~BenchmarkPartition();

    uint suggestedNumIterations() const override { return qBound(3U, 10000000U / (m_numElements * m_numBits), 100U); }

public slots:
    void setNumElements(uint arg) { m_numElements = arg; }
    uint numElements() const { return m_numElements; }
    uint numElements_min() const { return 1U; }
    uint numElements_max() const { return 1U << 20; }

    void setNumBits(uint arg) { m_numBits = arg; }
    uint numBits() const { return m_numBits; }
    uint numBits_min() const { return 6U; }
    uint numBits_max() const { return 24U; }

protected:
    void prepare() override;
    CLEventTree execute() override;

private:
    uint m_numElements;
    uint m_numBits;
    QScopedPointer<Partition> m_partition;
    QCLBuffer m_input, m_output, m_partitionStartsOutput;

    Partition *makeImpl();
};

#endif // BENCHMARKPARTITION_H
