#ifndef ABSTRACTBENCHMARK_H
#define ABSTRACTBENCHMARK_H

#include <QObject>
#include "test/abstracttest.h"
#include "cleventtree.h"

class CLContext;

class AbstractBenchmark : public QObject, protected AbstractTest
{
    Q_OBJECT

public:
    AbstractBenchmark(CLContext & context, QObject *parent = 0);

    // A concrete algorithm can suggest the number of iterations depending
    // on previously set parameters. This should more or less result in a
    // reasonable total profiling time, i.e. not too short for exact
    // profiling but of course also not too long.
    virtual uint suggestedNumIterations() const { return 100; }

    // Prepares and then executes the algorithm suggestedNumIteration() times.
    // Returns the average time elapsed per iteration (in seconds).
    // In a subclass, do not override this function but implement the abstract
    // functions prepare() and execute().
    double runBenchmark();

    // Returns the event tree of the last execution for detailed profiling.
    CLEventTree lastEventTree() const { return m_events; }

protected:
    CLContext & context() const { return m_context; }

    virtual void prepare() = 0;
    virtual CLEventTree execute() = 0;

private:
    CLContext & m_context;
    CLEventTree m_events;
};

#endif // ABSTRACTBENCHMARK_H
