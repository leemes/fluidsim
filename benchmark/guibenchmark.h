#ifndef GUIBENCHMARK_H
#define GUIBENCHMARK_H

#include <QWidget>
#include <QModelIndex>
#include "runbenchmark.h"

namespace Ui {
class GuiBenchmark;
}

class CLContext;
class AbstractBenchmark;
class QSlider;
class QCheckBox;

class GuiBenchmark : public QWidget
{
    Q_OBJECT
    
public:
    explicit GuiBenchmark(CLContext &context, QWidget *parent = 0);
    ~GuiBenchmark();
    
private slots:
    void parameterChanged(int value);
    void updateVaryingParameters();

    void on_algorithms_activated(const QModelIndex &index);
    void on_btnRun_clicked();

    void on_results_doubleClicked(const QModelIndex &index);

private:
    Ui::GuiBenchmark *ui;
    CLContext &m_context;
    AbstractBenchmark *m_benchmark;
    QVector<Parameter> m_parameters;
    QVector<QCheckBox*> m_parameterCheckBoxes;
    QVector<QSlider*> m_parameterSliders;
    QStandardItemModel *m_resultsModel;

    void registerBenchmark(AbstractBenchmark *benchmark);
    void setBenchmark(AbstractBenchmark *benchmark);
};

#endif // GUIBENCHMARK_H
