#ifndef BENCHMARKBUCKETSORT_H
#define BENCHMARKBUCKETSORT_H

#include "abstractbenchmark.h"

class CLEventTree;
class BucketSort;

class BenchmarkBucketSort : public AbstractBenchmark
{
    Q_OBJECT
    Q_PROPERTY(uint groupSize READ groupSize WRITE setGroupSize)
    Q_PROPERTY(uint numElements READ numElements WRITE setNumElements)
    Q_PROPERTY(uint numBuckets READ numBuckets WRITE setNumBuckets)
    Q_CLASSINFO("groupSize", "IncreaseExponential")
    Q_CLASSINFO("numElements", "IncreaseExponential")
    Q_CLASSINFO("numBuckets", "IncreaseExponential")

public:
    explicit BenchmarkBucketSort(CLContext & context, QObject *parent = 0);
    ~BenchmarkBucketSort();

    uint suggestedNumIterations() const override { return qBound(1U, 10000000U / m_numElements, 10000U); }

    void prepare() override;
    CLEventTree execute() override;

public slots:
    void setGroupSize(uint arg) { m_groupSize = arg; }
    uint groupSize() const { return m_groupSize; }
    uint groupSize_min() const { return m_numBuckets; }
    uint groupSize_max() const { return qMin(1024U, (1<<14) / m_numBuckets); }

    void setNumElements(uint arg) { m_numElements = arg; }
    uint numElements() const { return m_numElements; }
    uint numElements_min() const { return 1U; }
    uint numElements_max() const { return 1U << 24; }

    void setNumBuckets(uint arg) { m_numBuckets = arg; }
    uint numBuckets() const { return m_numBuckets; }
    uint numBuckets_min() const { return 2U; }
    uint numBuckets_max() const { return qMin(64U, groupSize()); }

private:
    uint m_groupSize;
    uint m_numElements;
    uint m_numBuckets;
    QScopedPointer<BucketSort> m_bucketSort;
    QCLBuffer m_input, m_output;

    BucketSort *makeImpl();
};

#endif // BENCHMARKBUCKETSORT_H
