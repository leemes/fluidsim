#include "runbenchmark.h"
#include "benchmarkbucketsort.h"
#include <QMetaProperty>
#include <stdexcept>
#include <QApplication>
#include <unistd.h>


bool operator<(QVariant a, QVariant b) {
    if (a.userType() != b.userType()) {
        throw std::runtime_error(std::string("Incompatible QVariant types: ")
                                 + a.typeName() + " vs. " + b.typeName());
    }
    // FIXME
    return a.toLongLong() < b.toLongLong();
}
bool operator<=(QVariant a, QVariant b) {
    return a < b || a == b;
}
QVariant operator+(QVariant a, QVariant b) {
    if (a.userType() != b.userType()) {
        throw std::runtime_error(std::string("Incompatible QVariant types: ")
                                 + a.typeName() + " vs. " + b.typeName());
    }
    // FIXME
    QVariant sum = a.toLongLong() + b.toLongLong();
    sum.convert(a.type());
    return sum;
}



QVariant Parameter::increased(QVariant val)
{
    if (flags & IncreaseExponential)
        val = val + val;
    else {
        QVariant one(1);
        one.convert(val.type());
        val = val + one;
    }
    return val;
}




QVector<Parameter> benchmarkParameters(AbstractBenchmark *benchmark)
{
    QVector<Parameter> parameters;

    const QMetaObject *meta = benchmark->metaObject();

    // Collect class infos in a more convenient QMap structure
    QMap<QString,QString> classInfos;
    for (int i = AbstractBenchmark::staticMetaObject.classInfoOffset(); i < meta->classInfoCount(); ++i)
        classInfos[meta->classInfo(i).name()] = meta->classInfo(i).value();

    // Iterate through the parameters (QMetaProperties)
    for (int i = AbstractBenchmark::staticMetaObject.propertyOffset(); i < meta->propertyCount(); ++i)
    {
        QMetaProperty property = meta->property(i);
        const char *name = property.name();
        const char *typeName = property.typeName();
        QVariant currentValue = benchmark->property(name);
        QVariant min, max;

        // Initialize the QVariants with the current property value, so they have the correct internal type
        min = currentValue;
        max = currentValue;

        // If one of the methods <propertyname>_min() <propertyname>_max() aren't defined or return the wrong type, the parameter is ignored.
        // Use a hack to invoke the method with QVariant (http://qt-project.org/forums/viewthread/4399)
        if (!meta->invokeMethod(benchmark, qPrintable(QString(name)+"_min"), QGenericReturnArgument(typeName, min.data())) ||
            !meta->invokeMethod(benchmark, qPrintable(QString(name)+"_max"), QGenericReturnArgument(typeName, max.data())))
        {
            qWarning("Parameter \"%s\" is ignored because one of the following functions is not defined, not invokable or returns the wrong type:\n"
                     "    %s %s_min() const;\n"
                     "    %s %s_max() const;",
                     name, typeName, name, typeName, name);
            continue;
        }

        Parameter::Flags flags = Parameter::NoFlags;
        if (classInfos.contains(name)) {
            if (classInfos[name].contains("IncreaseExponential"))
                flags = (Parameter::Flags)(flags | Parameter::IncreaseExponential);
        }

        // Add to the parameter list
        Parameter parameter;
        parameter.flags = flags;
        parameter.name = name;
        parameter.set = [=](QVariant value){ benchmark->setProperty(name, value); };
        parameter.get = [=]{ return benchmark->property(name); };
        parameter.getMin = [=]{ QVariant min = currentValue; meta->invokeMethod(benchmark, qPrintable(QString(name)+"_min"), QGenericReturnArgument(typeName, min.data())); return min; };
        parameter.getMax = [=]{ QVariant max = currentValue; meta->invokeMethod(benchmark, qPrintable(QString(name)+"_max"), QGenericReturnArgument(typeName, max.data())); return max; };
        parameters << parameter;
    }

    return parameters;
}

void runBenchmarkVarying(AbstractBenchmark *benchmark, QVector<Parameter> varyingParameters, QStandardItemModel *model, QStringList currentParametersText)
{
    model->clear();
    model->setHorizontalHeaderLabels(QStringList() << "Parameter value" << "Time");
    model->horizontalHeaderItem(1)->setTextAlignment(Qt::AlignRight);
    runBenchmarkVarying(benchmark, varyingParameters, model, QList<QStandardItem *>(), currentParametersText);
}

void runBenchmarkVarying(AbstractBenchmark *benchmark, QVector<Parameter> varyingParameters, QStandardItemModel *model, QList<QStandardItem *> row, QStringList currentParametersText)
{
    if (varyingParameters.isEmpty()) {
        if (row.isEmpty()) {
            QStandardItem *labelItem = new QStandardItem("(no varying parameters)");
            QStandardItem *resultsItem = new QStandardItem("computing...");
            row = { labelItem, resultsItem };
            model->appendRow(row);
        }
        runBenchmarkFixed(benchmark, model, row, currentParametersText);
    } else {
        Parameter parameter = varyingParameters.first();
        QVariant min = parameter.getMin();
        QVariant max = parameter.getMax();
        for (QVariant value = min; value <= max; value = parameter.increased(value)) {
            parameter.set(value);
            QString labelText = parameter.name + " = " + value.toString();
            QStandardItem *labelItem = new QStandardItem(labelText);
            QStandardItem *resultsItem = new QStandardItem(varyingParameters.count() > 1 ? "" : "computing...");
            QList<QStandardItem*> childRow = { labelItem, resultsItem };
            if (row.isEmpty())
                model->appendRow(childRow);
            else
                row.first()->appendRow(childRow);
            QString addedParameterText = parameter.name + " = " + value.toString();
            runBenchmarkVarying(benchmark, varyingParameters.mid(1), model, childRow, QStringList(currentParametersText) << addedParameterText);
        }
    }
}



void runBenchmarkFixed(AbstractBenchmark *benchmark, QStandardItemModel *model, QList<QStandardItem *> row, QStringList currentParametersText)
{
    QApplication::processEvents();

    double milliSeconds = benchmark->runBenchmark() * 1000.0;
    CLEventTree events = benchmark->lastEventTree();

    QString result = QString::number(milliSeconds, 'f') + " ms";
    row[0]->setData(QVariant::fromValue<CLEventTree>(events), EventTreeRole);
    row[0]->setData(QVariant::fromValue<QStringList>(currentParametersText), ParametersRole);
    row[1]->setTextAlignment(Qt::AlignRight);
    row[1]->setText(result);
}


