#include "benchmarkbucketsort.h"
#include "util/bucketsort.h"
#include "clcontext.h"
#include "cleventtree.h"
#include <QElapsedTimer>
#include "util.h"




BenchmarkBucketSort::BenchmarkBucketSort(CLContext &context, QObject *parent) :
    AbstractBenchmark(context, parent),
    m_groupSize(groupSize_min()),
    m_numElements(numElements_min()),
    m_numBuckets(numBuckets_min()),
    m_bucketSort(nullptr)
{
}

BenchmarkBucketSort::~BenchmarkBucketSort()
{
}

void BenchmarkBucketSort::prepare()
{
    m_bucketSort.reset(makeImpl());

    uint allocElements = m_bucketSort->minBufferSizeForNumElements(m_numElements);
    cl_float8 *data = new cl_float8[allocElements];
    std::generate_n(data, allocElements, []{ return cl_float8{frand(), frand(), frand(), frand(), frand(), frand(), frand(), frand()}; });

    m_input = context().createBufferCopy(data, sizeof(*data)*allocElements, QCLMemoryObject::ReadOnly);
    m_output = context().createBufferDevice(sizeof(*data)*allocElements, QCLMemoryObject::WriteOnly);
    m_bucketSort->allocateAuxiliaryBuffers(m_numElements);

    delete[] data;
}

CLEventTree BenchmarkBucketSort::execute()
{
    return (*m_bucketSort)(m_input, m_output, m_numElements);
}

BucketSort *BenchmarkBucketSort::makeImpl()
{
    return new BucketSort(context(), m_numBuckets,
                          "float8", sizeof(cl_float8),
                          "float8", sizeof(cl_float8),
                          "(uint)(element.x * NUM_BUCKETS)",
                          "element",
                          groupSize());
}

