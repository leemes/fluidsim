#include "benchmarkgrid.h"
#include "grid.h"
#include "clcontext.h"

#include "cl/particle.h"


BenchmarkGrid::BenchmarkGrid(CLContext & context, QObject *parent) :
    AbstractBenchmark(context, parent),
    m_numElements(numElements_min()),
    m_depth(depth_min())
{
    context.addAutoInclude("ParticleBasic", "particle.h");
    context.addAutoInclude("ParticleRender", "particle.h");
    context.addAutoInclude("ParticleSprings", "particle.h");
}

BenchmarkGrid::~BenchmarkGrid()
{
}

void BenchmarkGrid::prepare()
{
    m_grid.reset(makeImpl());

    uint allocElements = m_grid->minBufferSizeForNumParticles(m_numElements);

    m_input = context().createBufferDevice(sizeof(ParticleBasic) * allocElements, QCLMemoryObject::ReadOnly);
    m_output = context().createBufferDevice(sizeof(ParticleBasic) * allocElements, QCLMemoryObject::ReadWrite);
    m_cellStartsOutput = context().createBufferDevice(sizeof(cl_uint) * ((1 << (m_depth * 3)) + 1), QCLMemoryObject::ReadWrite);
    m_usedCellsOutput = context().createBufferDevice(sizeof(cl_uint) * ( 1 << (m_depth * 3)     ), QCLMemoryObject::ReadWrite);
}

CLEventTree BenchmarkGrid::execute()
{
    return m_grid->update(m_input, m_output, m_numElements, m_cellStartsOutput, m_usedCellsOutput);
}

Grid *BenchmarkGrid::makeImpl()
{
    return new Grid(context(), m_depth);
}
