#include "guibenchmark.h"
#include "ui_guibenchmark.h"
#include <QStandardItemModel>
#include <QCheckBox>
#include <QSlider>
#include <QDialog>

#include "benchmarkbucketsort.h"
#include "benchmarkradixsort.h"
#include "benchmarkpartition.h"
#include "benchmarkgrid.h"

Q_DECLARE_METATYPE(QVector<int>)


GuiBenchmark::GuiBenchmark(CLContext &context, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GuiBenchmark),
    m_context(context),
    m_benchmark(nullptr)
{
    ui->setupUi(this);

    // Insert algorithms
    registerBenchmark(new BenchmarkBucketSort(context, this));
    registerBenchmark(new BenchmarkRadixSort(context, this));
    registerBenchmark(new BenchmarkPartition(context, this));
    registerBenchmark(new BenchmarkGrid(context, this));

    // Init result model
    m_resultsModel = new QStandardItemModel;
    ui->results->setModel(m_resultsModel);
}

GuiBenchmark::~GuiBenchmark()
{
    delete ui;
}

void GuiBenchmark::parameterChanged(int value)
{
    int i = m_parameterSliders.indexOf(qobject_cast<QSlider*>(sender()));
    if (i == -1)
        qFatal("This didn't come from a slider I know about!");
    const Parameter &changedParameter = m_parameters.at(i);

    // Set the parameter value on the benchmark object
    changedParameter.set(value);

    // Update minima and maxima of other parameters
    i = 0;
    for (const Parameter &parameter : m_parameters) {
        // Ignore the changed parameter
        if (&changedParameter != &parameter) {
            QSlider *slider = m_parameterSliders[i];
            int min = parameter.getMin().toInt();
            int max = parameter.getMax().toInt();
            qDebug() << parameter.name << "now has the range" << min << ".." << max;
            slider->setMinimum(min);
            slider->setMaximum(max);
        }
        ++i;
    }
}

void GuiBenchmark::updateVaryingParameters()
{
    ui->parameterOrder->clear();
    QVector<int> varyingParameters;
    for (int i = 0; i < m_parameters.count(); ++i) {
        if (!m_parameterCheckBoxes[i]->isChecked()) {
            varyingParameters << i;
        }
    }

    if (!varyingParameters.isEmpty()) {
        do {
            QString text;
            for (int i : varyingParameters) {
                if (!text.isEmpty())
                    text.append(" -> ");
                text.append(m_parameters[i].name);
            }
            ui->parameterOrder->addItem(text, QVariant::fromValue(varyingParameters));
        } while (std::next_permutation(varyingParameters.begin(), varyingParameters.end()));
        ui->parameterOrder->setCurrentIndex(0);
    }
}

void GuiBenchmark::on_algorithms_activated(const QModelIndex &index)
{
    QObject *object = index.data(Qt::UserRole).value<QObject*>();
    if (!object)
        return;
    AbstractBenchmark *benchmark = qobject_cast<AbstractBenchmark*>(object);
    if (!benchmark)
        return;

    if (benchmark != m_benchmark) {
        setBenchmark(benchmark);
        updateVaryingParameters();
    }
}

void GuiBenchmark::registerBenchmark(AbstractBenchmark *benchmark)
{
    // Remove the prefix of the class name
    const char *className = benchmark->metaObject()->className();
    const char *prefix = "Benchmark";
    int prefixLength = strlen(prefix);
    if (strncmp(className, prefix, prefixLength))
        qFatal("The name of this class doesn't start with \"%s\"!", prefix);
    const char *classNameSuffix = className + prefixLength;

    // Split camel-case into words
    QString prettyName;
    for (const char *c = classNameSuffix; *c; ++c) {
        if (c != classNameSuffix && std::toupper(*c) == *c) {
            prettyName.append(" ");
        }
        prettyName.append(*c);
    }

    // Add to the list
    auto model = ui->algorithms->model();
    int row = model->rowCount();
    model->insertRow(row);
    model->setData(model->index(row, 0), prettyName);
    model->setData(model->index(row, 0), QVariant::fromValue<QObject*>(benchmark), Qt::UserRole);
}

void GuiBenchmark::setBenchmark(AbstractBenchmark *benchmark)
{
    m_benchmark = benchmark;
    m_parameters = benchmarkParameters(benchmark);

    // Clear results
    m_resultsModel->clear();

    // Clear parameter list ("Clear layout" hack: http://stackoverflow.com/a/7082920/592323)
    QWidget().setLayout(ui->parameters->layout());

    // Update parameter list in GUI
    m_parameterSliders.resize(m_parameters.count());
    m_parameterCheckBoxes.resize(m_parameters.count());
    QGridLayout *layout = new QGridLayout();
    ui->parameters->setLayout(layout);
    int row = 0;
    for (const Parameter & parameter : m_parameters)
    {
        // Checkbox with name
        QCheckBox *checkBox = new QCheckBox;
        checkBox->setText(parameter.name);
        connect(checkBox, SIGNAL(stateChanged(int)), SLOT(updateVaryingParameters()));
        layout->addWidget(checkBox, row, 0);
        m_parameterCheckBoxes[row] = checkBox;


        // Label displaying the current value
        QLabel *currentValue = new QLabel;
        currentValue->setNum(parameter.get().toInt());
        currentValue->setFixedWidth(64);
        currentValue->setAlignment(Qt::AlignRight);
        layout->addWidget(currentValue, row, 1);

        // Slider for changing the value
        QSlider *slider = new QSlider;
        slider->setOrientation(Qt::Horizontal);
        slider->setValue(parameter.get().toInt());
        slider->setMinimum(parameter.getMin().toInt());
        slider->setMaximum(parameter.getMax().toInt());
        connect(slider, SIGNAL(valueChanged(int)), SLOT(parameterChanged(int)));
        connect(slider, SIGNAL(valueChanged(int)), currentValue, SLOT(setNum(int)));
        layout->addWidget(slider, row, 2);
        m_parameterSliders[row] = slider;

        ++row;
    }
}

void GuiBenchmark::on_btnRun_clicked()
{
    QVector<Parameter> varyingParameters; // = m_parameters;
    if (ui->parameterOrder->currentIndex() != -1) {
        QVector<int> indices = ui->parameterOrder->itemData(ui->parameterOrder->currentIndex()).value<QVector<int>>();
        for (int i : indices)
            varyingParameters << m_parameters.at(i);
    }
    runBenchmarkVarying(m_benchmark, varyingParameters, m_resultsModel);
}

void GuiBenchmark::on_results_doubleClicked(const QModelIndex &index)
{
    auto model = index.model();
    bool leaf = !model->hasChildren(index);
    if (leaf) {
        QModelIndex firstInRow = model->index(index.row(), 0, index.parent());
        CLEventTree events = firstInRow.data(EventTreeRole).value<CLEventTree>();
        QStandardItemModel *profilingModel = new QStandardItemModel;
        profilingModel->setHorizontalHeaderLabels(QStringList() << "Step" << "Time" << "Relative");
        events.addToModel(profilingModel);

        QDialog *dialog = new QDialog(this);
        dialog->setWindowTitle("OpenCL Profiling Details");
        QVBoxLayout *layout = new QVBoxLayout(dialog);

        QLabel *label = new QLabel;
        QString text = ui->algorithms->currentItem()->text() + ": ";
        QStringList params = firstInRow.data(ParametersRole).value<QStringList>();
        text += params.join(", ");
        label->setText(text);
        layout->addWidget(label);

        QTreeView *profilingView = new QTreeView(dialog);
        profilingView->setModel(profilingModel);
        profilingView->setColumnWidth(0, 300);
        profilingView->setColumnWidth(1, 120);
        profilingView->setColumnWidth(2,  70);
        layout->addWidget(profilingView);

        dialog->resize(520, 300);
        dialog->show();
    }
}
