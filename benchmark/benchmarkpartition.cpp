#include "benchmarkpartition.h"
#include "util/partition.h"
#include "clcontext.h"

typedef cl_uint T;
static const char *typeName = "uint";



BenchmarkPartition::BenchmarkPartition(CLContext & context, QObject *parent) :
    AbstractBenchmark(context, parent),
    m_numElements(numElements_min()),
    m_numBits(numBits_min())
{
}

BenchmarkPartition::~BenchmarkPartition()
{
}

void BenchmarkPartition::prepare()
{
    m_partition.reset(makeImpl());

    uint allocElements = m_partition->minBufferSizeForNumElements(m_numElements);

    m_input = context().createBufferDevice(sizeof(T) * allocElements, QCLMemoryObject::ReadOnly);
    m_output = context().createBufferDevice(sizeof(T) * allocElements, QCLMemoryObject::ReadWrite);
    m_partitionStartsOutput = context().createBufferDevice(sizeof(cl_uint) * ((1 << m_numBits) + 1), QCLMemoryObject::ReadWrite);
}

CLEventTree BenchmarkPartition::execute()
{
    return (*m_partition)(m_input, m_output, m_numElements, m_partitionStartsOutput);
}

Partition *BenchmarkPartition::makeImpl()
{
    return new Partition(context(), typeName, sizeof(T),
                         QString("element % %1").arg(1 << m_numBits),
                         m_numBits);
}
