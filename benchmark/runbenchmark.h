#ifndef RUNBENCHMARK_H
#define RUNBENCHMARK_H

#include <QVector>
#include <QVariant>
#include <QMetaProperty>
#include <QStandardItemModel>

class AbstractBenchmark;

struct Parameter {
    enum Flags {
        NoFlags             = 0x0,
        IncreaseExponential = 0x1
    } flags;
    QString name;
    std::function<void(QVariant)> set;
    std::function<QVariant()> get;
    std::function<QVariant()> getMin;
    std::function<QVariant()> getMax;
    QVariant increased(QVariant);
};

// Additional roles set in the model:
enum {
    EventTreeRole = Qt::UserRole + 1,
    ParametersRole
};

QVector<Parameter> benchmarkParameters(AbstractBenchmark *benchmark);
void runBenchmarkVarying(AbstractBenchmark *benchmark, QVector<Parameter> varyingParameters, QStandardItemModel *model, QStringList currentParametersText = QStringList());
void runBenchmarkVarying(AbstractBenchmark *benchmark, QVector<Parameter> varyingParameters, QStandardItemModel *model, QList<QStandardItem *> row, QStringList currentParametersText = QStringList());
void runBenchmarkFixed(AbstractBenchmark *benchmark, QStandardItemModel *model, QList<QStandardItem *> row, QStringList currentParametersText = QStringList());

#endif // RUNBENCHMARK_H
