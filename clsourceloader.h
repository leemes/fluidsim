#ifndef CLSOURCELOADER_H
#define CLSOURCELOADER_H

#include <QString>
#include <QMap>

/**
 * Loads OpenCL source files. Can resolve "#include" directives.
 * Does not evaluate preprocessor branches for conditional includes.
 * However, all files are only included once, such as if there was
 * a "#pragma once" directive in every file. Does not support include
 * paths relative to the current file. All paths are relative to the
 * basePath as configured in the constructor.
 */
class CLSourceLoader
{
public:
    CLSourceLoader(QString basePath);

    // Adds an "auto include" rule, which will include a header file every time a type appears in the source code.
    // Note that this "appearance" is based on string search (in source code and macros defined with "-D"), not by using a code model.
    void addAutoInclude(QString typeName, QString fileName);

    QByteArray loadSource(const QString &fileName, const QByteArray &prependCode = QByteArray()) const;

private:
    QString m_basePath;
    QMap<QString, QString> m_autoIncludes;

    QByteArray loadAndResolveIncludes(const QString &fileName, QStringList &alreadyIncluded) const;
    QByteArray load(const QString &fileName, QStringList &alreadyIncluded) const;
    void resolveIncludes(QByteArray &source, QStringList &alreadyIncluded) const;

    // Returns "#include" directives for files to be auto-included, based on the provided source code (the returned code should be prepended to the source code + the #includes should be resolved one more time).
    QByteArray autoInclude(QByteArray sourceCode) const;
};

#endif // CLSOURCELOADER_H
