#ifndef CLEVENTTREE_H
#define CLEVENTTREE_H

#include <qclevent.h>
#include <QString>
#include <iostream>
#include <QModelIndex>

class QAbstractItemModel;

class CLEventTree
{
public:
    CLEventTree(const QString &name = "(unnamed)");
    CLEventTree(const QCLEvent & singleEvent, const QString &name);
    CLEventTree(const CLEventTree & subTree, const QString &name);

    CLEventTree & operator<<(const CLEventTree & subTree);

    double milliSeconds() const;

    void print(std::ostream & stream = std::cout, int maxDepth = 256, int recursion = 0, int treeWidth = 0, double total = 0.0) const;
    void addToModel(QAbstractItemModel *model, const QModelIndex &parent = QModelIndex(), double total = -1.0) const;

private:
    QCLEvent m_event;
    QString m_name;
    QVector<CLEventTree> m_children;

    int treeWidth(int recursion = 0) const;
};
Q_DECLARE_METATYPE(CLEventTree)

std::ostream & operator <<(std::ostream &, const CLEventTree &);

#endif // CLEVENTTREE_H
