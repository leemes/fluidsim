#include "fluidsimulation.h"
#include "rigidbody.h"
#include "clcontext.h"
#include "particlesystem.h"
#include <QOpenGLFramebufferObject>
#include <QProcess>
#include <QTimer>

#include <QThread>



FluidSimulation::FluidSimulation() :
    m_particleSystem(0)
{
    setRenderTarget(FramebufferObject);
    setAntialiasing(true);

    m_profilingOutput = new QFile("profiling.data", this);
    m_profilingOutput->open(QFile::WriteOnly);

    connect(this, SIGNAL(cameraChanged()), SLOT(update()));
}

FluidSimulation::~FluidSimulation()
{
}


QQmlListProperty<RigidBody> FluidSimulation::rigidBodiesList()
{
    auto append = [](QQmlListProperty<RigidBody> *property, RigidBody *value) {
        FluidSimulation *s = qobject_cast<FluidSimulation*>(property->object); Q_ASSERT(s);
        if(s->m_particleSystem)
            s->m_particleSystem->addRigidBody(value);
        else
            s->m_rigidBodiesToBeAdded << value;
    };
    auto count = [](QQmlListProperty<RigidBody> *property) {
        FluidSimulation *s = qobject_cast<FluidSimulation*>(property->object); Q_ASSERT(s);
        if(s->m_particleSystem)
            return s->m_particleSystem->rigidBodies().count();
        else
            return s->m_rigidBodiesToBeAdded.count();
    };
    auto at = [](QQmlListProperty<RigidBody> *property, int i) {
        FluidSimulation *s = qobject_cast<FluidSimulation*>(property->object); Q_ASSERT(s);
        if(s->m_particleSystem)
            return s->m_particleSystem->rigidBodies().at(i);
        else
            return s->m_rigidBodiesToBeAdded.at(i);
    };
    auto clear = [](QQmlListProperty<RigidBody> *property) {
        FluidSimulation *s = qobject_cast<FluidSimulation*>(property->object); Q_ASSERT(s);
        if(s->m_particleSystem)
            s->m_particleSystem->clearRigidBodies();
        else
            s->m_rigidBodiesToBeAdded.clear();
    };
    return QQmlListProperty<RigidBody>(this, nullptr, append, count, at, clear);
}

QQmlListProperty<Emitter> FluidSimulation::emittersList()
{
    auto append = [](QQmlListProperty<Emitter> *property, Emitter *value) {
        FluidSimulation *s = qobject_cast<FluidSimulation*>(property->object); Q_ASSERT(s);
        if(s->m_particleSystem)
            s->m_particleSystem->addEmitter(value);
        else
            s->m_emittersToBeAdded << value;
    };
    auto count = [](QQmlListProperty<Emitter> *property) {
        FluidSimulation *s = qobject_cast<FluidSimulation*>(property->object); Q_ASSERT(s);
        if(s->m_particleSystem)
            return s->m_particleSystem->emitters().count();
        else
            return s->m_emittersToBeAdded.count();
    };
    auto at = [](QQmlListProperty<Emitter> *property, int i) {
        FluidSimulation *s = qobject_cast<FluidSimulation*>(property->object); Q_ASSERT(s);
        if(s->m_particleSystem)
            return s->m_particleSystem->emitters().at(i);
        else
            return s->m_emittersToBeAdded.at(i);
    };
    auto clear = [](QQmlListProperty<Emitter> *property) {
        FluidSimulation *s = qobject_cast<FluidSimulation*>(property->object); Q_ASSERT(s);
        if(s->m_particleSystem)
            s->m_particleSystem->clearEmitters();
        else
            s->m_emittersToBeAdded.clear();
    };
    return QQmlListProperty<Emitter>(this, nullptr, append, count, at, clear);
}

double FluidSimulation::simulationUpdateTime() const
{
    if (m_particleSystem)
        return m_particleSystem->prevSimulationUpdateTime();
    else
        return 0.0;
}

double FluidSimulation::gridConstructionTime() const
{
    if (m_particleSystem)
        return m_particleSystem->prevGridConstructionTime();
    else
        return 0.0;
}

int FluidSimulation::numParticles() const
{
    if (m_particleSystem)
        return m_particleSystem->numParticles();
    else
        return 0;
}

int FluidSimulation::gridSize() const
{
    return 1 << m_gridDepth;
}

void FluidSimulation::iterate()
{
    m_iterate = true;
    update();
}

void FluidSimulation::paint(QPainter *painter)
{
    painter->beginNativePainting();

    if (!m_initialized)
        initialize();

    if (m_iterate) {
        for (int iteration = 0; iteration < m_numIterationsPerFrame; ++iteration)
            m_particleSystem->update();
        m_iterate = false;

        emit iterated();
    }

    glClearColor(.65, .65, .65, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    m_perspAspect = width() / height();
    updateProjectionMatrix();
    glLoadMatrixf(m_projectionMatrix.constData());

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    updateViewMatrix();
    glLoadMatrixf(m_viewMatrix.constData());

    // Swap Y and Z axis
    glRotated(90.0, 1, 0, 0);
    glScaled(1, 1, -1);

    // Floor
    glBegin(GL_QUADS);
    glColor4d(.75, .75, .75, 1);
    glVertex2f(0, 0);
    glVertex2f(0, 1);
    glVertex2f(1, 1);
    glVertex2f(1, 0);
    glEnd();
    glScaled(1.0 / m_particleSystem->size(), 1.0 / m_particleSystem->size(), 1.0 / m_particleSystem->size());
    m_particleSystem->render(10.0f * height() / m_particleSystem->size() / m_camDist / m_perspFovy);

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    painter->endNativePainting();
}

void FluidSimulation::setMaxParticles(int arg) {
    if (m_maxParticles != arg) {
        m_maxParticles = arg;
        if (m_initialized)
            m_particleSystem->setMaxParticles(arg);
    }
}

void FluidSimulation::initialize()
{
    Q_ASSERT(!m_initialized && !m_particleSystem);

    qDebug() << "FluidSimulation::initialize THREAD: " << QThread::currentThread();
    qDebug() << "thread() returns " << thread();

    m_particleSystem = new ParticleSystem(m_gridDepth);
    connect(this, SIGNAL(destroyed()), m_particleSystem, SLOT(deleteLater()), Qt::QueuedConnection);

    for (Emitter *emitter : m_emittersToBeAdded)
        m_particleSystem->addEmitter(emitter);
    m_emittersToBeAdded.clear();
    for (RigidBody *rigidBody : m_rigidBodiesToBeAdded)
        m_particleSystem->addRigidBody(rigidBody);
    m_rigidBodiesToBeAdded.clear();

    m_particleSystem->setMaxParticles(m_maxParticles);
    m_particleSystem->setProfilingOutput(m_profilingOutput);

    m_initialized = true;
}

void FluidSimulation::updateProjectionMatrix()
{
    m_projectionMatrix.setToIdentity();
    m_projectionMatrix.perspective(m_perspFovy, m_perspAspect, m_perspNear, m_perspFar);
}

void FluidSimulation::updateViewMatrix()
{
    m_viewMatrix.setToIdentity();
    float x = sin(m_camYaw) * cos(m_camPitch) * m_camDist;
    float y = cos(m_camYaw) * cos(m_camPitch) * m_camDist;
    QVector3D center(.5, m_centerHeight, .5);
    QVector3D eye(x, sin(m_camPitch) * m_camDist + 0, y);
    eye += center;
    m_viewMatrix.scale(1, -1, 1);
    m_viewMatrix.lookAt(eye, center, {0, 1, 0});
}
