#!/bin/bash

QRC_FILE=resources.qrc
BASE_DIR=.
PREFIX=/

while [ $# -gt 0 ]; do
    case "$1" in
        '-qrc')
            shift
            QRC_FILE="$1"
            ;;
        '-base')
            shift
            BASE_DIR="$1"
            ;;
        '-prefix')
            shift
            PREFIX="$1"
            ;;
        *)
            echo "Wrong parameter: $1"
            echo "Usage: makeqrc.sh [options]"
            echo "Options:"
            echo "    -qrc <QRC_FILE>   (default: resources.qrc)"
            echo "    -base <BASE_DIR>  (default: .)"
            echo "    -prefix <PREFIX>  (default: /)"
            ;;
    esac
    shift
done


function makeQRC() {
    echo "<RCC>"
    echo "    <qresource prefix=\"$PREFIX\">"
    find -L $BASE_DIR -type f -not \( -name "*.qrc" -or -name ".*.swp" -or -name "*.xcf" \) | sort | sed "s@\(.*\)@        <file>\\1</file>@"
    echo "    </qresource>"
    echo "</RCC>"
}

qrc="`makeQRC`"

oldqrc="`cat "$QRC_FILE"`"
if [ "$qrc" != "$oldqrc" ]; then
    echo "$qrc" > "$QRC_FILE"
fi

