#ifndef FLUIDSIMULATION_H
#define FLUIDSIMULATION_H

#include <QQuickPaintedItem>
#include <QVector3D>
#include <QMatrix4x4>

class ParticleSystem;
class RigidBody;
class Emitter;

class FluidSimulation : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<RigidBody> rigidBodies READ rigidBodiesList)
    Q_PROPERTY(QQmlListProperty<Emitter> emitters READ emittersList)
    Q_PROPERTY(double yaw READ yaw WRITE setYaw NOTIFY cameraChanged)
    Q_PROPERTY(double pitch READ pitch WRITE setPitch NOTIFY cameraChanged)
    Q_PROPERTY(double fovy READ fovy WRITE setFovy NOTIFY cameraChanged)
    Q_PROPERTY(double centerHeight READ centerHeight WRITE setCenterHeight NOTIFY cameraChanged)

    Q_PROPERTY(double simulationUpdateTime READ simulationUpdateTime NOTIFY iterated)
    Q_PROPERTY(double gridConstructionTime READ gridConstructionTime NOTIFY iterated)
    Q_PROPERTY(int numParticles READ numParticles NOTIFY iterated)
    Q_PROPERTY(int gridSize READ gridSize NOTIFY _no_notify)

    Q_PROPERTY(int maxParticles READ maxParticles WRITE setMaxParticles NOTIFY _no_notify)

public:
    FluidSimulation();
    ~FluidSimulation();

    QQmlListProperty<RigidBody> rigidBodiesList();
    QQmlListProperty<Emitter> emittersList();

    QSize size() const { return QSize(width(), height()); }

    double yaw() const { return m_camYaw; }
    double pitch() const { return m_camPitch; }
    double fovy() const { return m_perspFovy; }
    double centerHeight() const { return m_centerHeight; }

    // Forwarded properties from ParticleSystem
    double simulationUpdateTime() const;
    double gridConstructionTime() const;
    int numParticles() const;
    int maxParticles() const { return m_maxParticles; }
    int gridSize() const;


public slots:
    void iterate();

    void paint(QPainter *painter) override;

    void setYaw(double arg) {
        if (m_camYaw != arg) {
            m_camYaw = arg;
            emit cameraChanged();
        }
    }
    void setPitch(double arg) {
        arg = qBound(-M_PI_2+.001, arg, M_PI_2-.001);
        if (m_camPitch != arg) {
            m_camPitch = arg;
            emit cameraChanged();
        }
    }
    void setFovy(double arg) {
        arg = qBound(1.0, arg, 60.0);
        if (m_perspFovy != arg) {
            m_perspFovy = arg;
            emit cameraChanged();
        }
    }
    void setCenterHeight(double arg) {
        if (m_centerHeight != arg) {
            m_centerHeight = arg;
            emit cameraChanged();
        }
    }
    void setMaxParticles(int arg);

signals:
    void cameraChanged();
    void iterated();
    void _no_notify(); // never emitted

private:
    bool m_initialized = false;
    ParticleSystem* m_particleSystem;
    QList<RigidBody*> m_rigidBodiesToBeAdded;
    QList<Emitter*> m_emittersToBeAdded;
    int m_maxParticles = 1 << 16;
    int m_numIterationsPerFrame = 1;
    int m_gridDepth = 6;
    QFile *m_profilingOutput;

    void initialize();

    // Projection-Matrix:
    float m_perspFovy = 24;
    float m_perspAspect;
    float m_perspNear = 1.0;
    float m_perspFar = 10.0;
    QMatrix4x4 m_projectionMatrix;
    void updateProjectionMatrix();

    // View-Matrix:
    QPoint m_prevCursorPos;
    float m_camYaw = 0.1;
    float m_camPitch = 0.5;
    float m_camDist = 3.0;
    float m_centerHeight = 0.5;
    QMatrix4x4 m_viewMatrix;
    void updateViewMatrix();

    // Hack: Since QML will give us a GL context in the render thread, we only iterate when painting.
    // We use this flag to indicate that we want the paint() method to update the particle system.
    bool m_iterate = false;
};


#endif // FLUIDSIMULATION_H
