#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include <QObject>
#include <QScopedPointer>
#include <QGLBuffer>
#include <qclbuffer.h>
#include "size3d.h"
#include "emitter.h"
#include "cleventtree.h"
#include "clcontext.h"
#include <QtQml/QQmlListProperty>

class CLContext;
class Grid;
class Simulation;
class Move;
class QSignalMapper;
class QFile;
class RigidBody;

class ParticleSystem : public QObject
{
    Q_OBJECT

public:
    explicit ParticleSystem(uint gridDepth);
    ~ParticleSystem();

    CLContext &context() { return m_context; }
    const CLContext &context() const { return m_context; }
    uint gridDepth() const { return m_gridDepth; }
    uint size() const { return 1U << m_gridDepth; }

    // Number of particles
    int numParticles() const { return m_numParticles; }
    void setMaxParticles(uint maxParticles);

    // Access to internal buffers
    const QCLBuffer & particleBuffer() const { return m_particleBasicBuffer; }
    const QCLBuffer & gridCellsBuffer() const { return m_cellStartsBuffer; }

    // Rigid bodies
    void addRigidBody(RigidBody *object);
    QList<RigidBody*> rigidBodies() const;
    void clearRigidBodies();

    // Emitters
    void addEmitter(Emitter *emitter);
    QList<Emitter*> emitters() const;
    void clearEmitters();

    // Profiling
    void setProfilingOutput(QFile *profilingOutput);
    double prevSimulationUpdateTime() const { return m_prevSimulationUpdateTime; }
    double prevGridConstructionTime() const { return m_prevGridConstructionTime; }
    double maxSimulationUpdateTime() const { return m_maxSimulationUpdateTime; }
    double maxGridConstructionTime() const { return m_maxGridConstructionTime; }

public slots:
    void update();
    void render(float pointSize);
    void rigidBodyChanged();
    
private:
    CLContext m_context;
    uint m_gridDepth;
    QScopedPointer<Grid> m_grid;
    QScopedPointer<Simulation> m_simulation;
    QScopedPointer<Move> m_moveParticleVelocity;

    uint m_numParticles;
    uint m_allocParticles;
    uint m_maxParticles;
    uint m_iteration = 0;
    uint m_allocRigidBodies;

    // OpenGL buffers:
    QGLBuffer m_particleBasicBufferGL;
    QGLBuffer m_particleVelocityBufferGL;

    // OpenCL buffers:
    QCLBuffer m_particleBasicBuffer;
    QCLBuffer m_particleBasicAuxBuffer;
    QCLBuffer m_particleVelocityBuffer;
    QCLBuffer m_particleVelocityAuxBuffer;
    QCLBuffer m_rigidBodiesBuffer;
    QCLBuffer m_cellStartsBuffer;
    QCLBuffer m_usedCellsBuffer;

    // Scene objects:
    QList<RigidBody*> m_rigidBodies;
    QList<Emitter*> m_emitters;

    // Profiling:
    QFile *m_profilingOutput = nullptr;
    double m_prevSimulationUpdateTime = 0.0;
    double m_prevGridConstructionTime = 0.0;
    double m_maxSimulationUpdateTime = 0.0;
    double m_maxGridConstructionTime = 0.0;

    void allocateBuffers();

    CLEventTree emitParticlesStep(QCLBuffer & particleBasic, QCLBuffer & particleVelocity);
    void writeRigidBody(const RigidBody *object, int index);

    Q_DISABLE_COPY(ParticleSystem)
};

#endif // PARTICLESYSTEM_H
