#ifndef SIMULATIONCONSTANTS_H
#define SIMULATIONCONSTANTS_H

#include "begin_shared.h"


// ENVIRONMENT
#define GRAVITY_X           0.0f
#define GRAVITY_Y           0.0f
#define GRAVITY_Z           -0.002f
#ifdef DEVICE
#define GRAVITY             (float3)( GRAVITY_X, GRAVITY_Y, GRAVITY_Z )
#else
#define GRAVITY             float3{ GRAVITY_X, GRAVITY_Y, GRAVITY_Z }
#endif

#define WIND_X              0.0f
#define WIND_Y              0.0f
#define WIND_Z              0.0f
#ifdef DEVICE
#define WIND                (float3)( WIND_X, WIND_Y, WIND_Z )
#else
#define WIND                float3{ WIND_X, WIND_Y, WIND_Z }
#endif


// TIME RESOLUTION
#define DT                  1.0f

// DENSITY
#define DENSITY_NOMINAL     8.0f
#define DENSITY_K           0.006f
#define DENSITY_K_NEAR      0.01f

// VISCOSITY
#define VISCOSITY_SIGMA     0.01f
#define VISCOSITY_BETA      0.04f
#define VISCOSITY_AIR_DRAG  0.0005f

// COMPUTATIONAL STABILITY
#define IMPULSE_MAX         0.1f
#define VELOCITY_MAX        0.4f
#define JITTER              0.0001f


#include "end_shared.h"

#endif
