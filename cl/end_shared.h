#ifdef __OPENCL_CL_H

#undef char
#undef char2
#undef char3
#undef char4
#undef char8
#undef char16

#undef uchar
#undef uchar2
#undef uchar3
#undef uchar4
#undef uchar8
#undef uchar16

#undef short
#undef short2
#undef short3
#undef short4
#undef short8
#undef short16

#undef ushort
#undef ushort2
#undef ushort3
#undef ushort4
#undef ushort8
#undef ushort16

#undef int
#undef int2
#undef int3
#undef int4
#undef int8
#undef int16

#undef uint
#undef uint2
#undef uint3
#undef uint4
#undef uint8
#undef uint16

#undef long
#undef long2
#undef long3
#undef long4
#undef long8
#undef long16

#undef ulong
#undef ulong2
#undef ulong3
#undef ulong4
#undef ulong8
#undef ulong16

#undef float
#undef float2
#undef float3
#undef float4
#undef float8
#undef float16

#undef double
#undef double2
#undef double3
#undef double4
#undef double8
#undef double16

#undef half

#endif
