#ifndef CLDEFS_H
#define CLDEFS_H

#define __kernel
#define __global
#define __local
#define __private
#define __constant
#define __read_only
#define __write_only


//-----------------------------------------------------------------------------


typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned long ulong;
typedef unsigned short half; // hack

#define CLDEFS_VECTORS(T) \
    typedef struct T##2 { \
        T x, y; \
        T s0, s1; \
        T lo, hi; \
    } T##2; \
    typedef struct T##3 { \
        T x, y, z; \
        T s0, s1, s2; \
        T##2 xx; \
        T##2 xy; \
        T##2 xz; \
        T##2 yx; \
        T##2 yy; \
        T##2 yz; \
        T##2 zx; \
        T##2 zy; \
        T##2 zz; \
    } T##3; \
    typedef struct T##4 { \
        T x, y, z, w; \
        T s0, s1, s2, s3; \
        T##2 lo, hi; \
        T##2 xx; \
        T##2 xy; \
        T##2 xz; \
        T##2 xw; \
        T##2 yx; \
        T##2 yy; \
        T##2 yz; \
        T##2 yw; \
        T##2 zx; \
        T##2 zy; \
        T##2 zz; \
        T##2 zw; \
        T##2 wx; \
        T##2 wy; \
        T##2 wz; \
        T##2 ww; \
        T##3 xxx; \
        T##3 xxy; \
        T##3 xxz; \
        T##3 xxw; \
        T##3 xyx; \
        T##3 xyy; \
        T##3 xyz; \
        T##3 xyw; \
        T##3 xzx; \
        T##3 xzy; \
        T##3 xzz; \
        T##3 xzw; \
        T##3 xwx; \
        T##3 xwy; \
        T##3 xwz; \
        T##3 xww; \
        T##3 yxx; \
        T##3 yxy; \
        T##3 yxz; \
        T##3 yxw; \
        T##3 yyx; \
        T##3 yyy; \
        T##3 yyz; \
        T##3 yyw; \
        T##3 yzx; \
        T##3 yzy; \
        T##3 yzz; \
        T##3 yzw; \
        T##3 ywx; \
        T##3 ywy; \
        T##3 ywz; \
        T##3 yww; \
        T##3 zxx; \
        T##3 zxy; \
        T##3 zxz; \
        T##3 zxw; \
        T##3 zyx; \
        T##3 zyy; \
        T##3 zyz; \
        T##3 zyw; \
        T##3 zzx; \
        T##3 zzy; \
        T##3 zzz; \
        T##3 zzw; \
        T##3 zwx; \
        T##3 zwy; \
        T##3 zwz; \
        T##3 zww; \
        T##3 wxx; \
        T##3 wxy; \
        T##3 wxz; \
        T##3 wxw; \
        T##3 wyx; \
        T##3 wyy; \
        T##3 wyz; \
        T##3 wyw; \
        T##3 wzx; \
        T##3 wzy; \
        T##3 wzz; \
        T##3 wzw; \
        T##3 wwx; \
        T##3 wwy; \
        T##3 wwz; \
        T##3 www; \
    } T##4; \
    typedef struct T##8 { \
        T x, y, z, w; \
        T s0, s1, s2, s3, s4, s5, s6, s7; \
        T##4 lo, hi; \
        T##2 xx; \
        T##2 xy; \
        T##2 xz; \
        T##2 xw; \
        T##2 yx; \
        T##2 yy; \
        T##2 yz; \
        T##2 yw; \
        T##2 zx; \
        T##2 zy; \
        T##2 zz; \
        T##2 zw; \
        T##2 wx; \
        T##2 wy; \
        T##2 wz; \
        T##2 ww; \
        T##3 xxx; \
        T##3 xxy; \
        T##3 xxz; \
        T##3 xxw; \
        T##3 xyx; \
        T##3 xyy; \
        T##3 xyz; \
        T##3 xyw; \
        T##3 xzx; \
        T##3 xzy; \
        T##3 xzz; \
        T##3 xzw; \
        T##3 xwx; \
        T##3 xwy; \
        T##3 xwz; \
        T##3 xww; \
        T##3 yxx; \
        T##3 yxy; \
        T##3 yxz; \
        T##3 yxw; \
        T##3 yyx; \
        T##3 yyy; \
        T##3 yyz; \
        T##3 yyw; \
        T##3 yzx; \
        T##3 yzy; \
        T##3 yzz; \
        T##3 yzw; \
        T##3 ywx; \
        T##3 ywy; \
        T##3 ywz; \
        T##3 yww; \
        T##3 zxx; \
        T##3 zxy; \
        T##3 zxz; \
        T##3 zxw; \
        T##3 zyx; \
        T##3 zyy; \
        T##3 zyz; \
        T##3 zyw; \
        T##3 zzx; \
        T##3 zzy; \
        T##3 zzz; \
        T##3 zzw; \
        T##3 zwx; \
        T##3 zwy; \
        T##3 zwz; \
        T##3 zww; \
        T##3 wxx; \
        T##3 wxy; \
        T##3 wxz; \
        T##3 wxw; \
        T##3 wyx; \
        T##3 wyy; \
        T##3 wyz; \
        T##3 wyw; \
        T##3 wzx; \
        T##3 wzy; \
        T##3 wzz; \
        T##3 wzw; \
        T##3 wwx; \
        T##3 wwy; \
        T##3 wwz; \
        T##3 www; \
        T##4 xxxx; \
        T##4 xxxy; \
        T##4 xxxz; \
        T##4 xxxw; \
        T##4 xxyx; \
        T##4 xxyy; \
        T##4 xxyz; \
        T##4 xxyw; \
        T##4 xxzx; \
        T##4 xxzy; \
        T##4 xxzz; \
        T##4 xxzw; \
        T##4 xxwx; \
        T##4 xxwy; \
        T##4 xxwz; \
        T##4 xxww; \
        T##4 xyxx; \
        T##4 xyxy; \
        T##4 xyxz; \
        T##4 xyxw; \
        T##4 xyyx; \
        T##4 xyyy; \
        T##4 xyyz; \
        T##4 xyyw; \
        T##4 xyzx; \
        T##4 xyzy; \
        T##4 xyzz; \
        T##4 xyzw; \
        T##4 xywx; \
        T##4 xywy; \
        T##4 xywz; \
        T##4 xyww; \
        T##4 xzxx; \
        T##4 xzxy; \
        T##4 xzxz; \
        T##4 xzxw; \
        T##4 xzyx; \
        T##4 xzyy; \
        T##4 xzyz; \
        T##4 xzyw; \
        T##4 xzzx; \
        T##4 xzzy; \
        T##4 xzzz; \
        T##4 xzzw; \
        T##4 xzwx; \
        T##4 xzwy; \
        T##4 xzwz; \
        T##4 xzww; \
        T##4 xwxx; \
        T##4 xwxy; \
        T##4 xwxz; \
        T##4 xwxw; \
        T##4 xwyx; \
        T##4 xwyy; \
        T##4 xwyz; \
        T##4 xwyw; \
        T##4 xwzx; \
        T##4 xwzy; \
        T##4 xwzz; \
        T##4 xwzw; \
        T##4 xwwx; \
        T##4 xwwy; \
        T##4 xwwz; \
        T##4 xwww; \
        T##4 yxxx; \
        T##4 yxxy; \
        T##4 yxxz; \
        T##4 yxxw; \
        T##4 yxyx; \
        T##4 yxyy; \
        T##4 yxyz; \
        T##4 yxyw; \
        T##4 yxzx; \
        T##4 yxzy; \
        T##4 yxzz; \
        T##4 yxzw; \
        T##4 yxwx; \
        T##4 yxwy; \
        T##4 yxwz; \
        T##4 yxww; \
        T##4 yyxx; \
        T##4 yyxy; \
        T##4 yyxz; \
        T##4 yyxw; \
        T##4 yyyx; \
        T##4 yyyy; \
        T##4 yyyz; \
        T##4 yyyw; \
        T##4 yyzx; \
        T##4 yyzy; \
        T##4 yyzz; \
        T##4 yyzw; \
        T##4 yywx; \
        T##4 yywy; \
        T##4 yywz; \
        T##4 yyww; \
        T##4 yzxx; \
        T##4 yzxy; \
        T##4 yzxz; \
        T##4 yzxw; \
        T##4 yzyx; \
        T##4 yzyy; \
        T##4 yzyz; \
        T##4 yzyw; \
        T##4 yzzx; \
        T##4 yzzy; \
        T##4 yzzz; \
        T##4 yzzw; \
        T##4 yzwx; \
        T##4 yzwy; \
        T##4 yzwz; \
        T##4 yzww; \
        T##4 ywxx; \
        T##4 ywxy; \
        T##4 ywxz; \
        T##4 ywxw; \
        T##4 ywyx; \
        T##4 ywyy; \
        T##4 ywyz; \
        T##4 ywyw; \
        T##4 ywzx; \
        T##4 ywzy; \
        T##4 ywzz; \
        T##4 ywzw; \
        T##4 ywwx; \
        T##4 ywwy; \
        T##4 ywwz; \
        T##4 ywww; \
        T##4 zxxx; \
        T##4 zxxy; \
        T##4 zxxz; \
        T##4 zxxw; \
        T##4 zxyx; \
        T##4 zxyy; \
        T##4 zxyz; \
        T##4 zxyw; \
        T##4 zxzx; \
        T##4 zxzy; \
        T##4 zxzz; \
        T##4 zxzw; \
        T##4 zxwx; \
        T##4 zxwy; \
        T##4 zxwz; \
        T##4 zxww; \
        T##4 zyxx; \
        T##4 zyxy; \
        T##4 zyxz; \
        T##4 zyxw; \
        T##4 zyyx; \
        T##4 zyyy; \
        T##4 zyyz; \
        T##4 zyyw; \
        T##4 zyzx; \
        T##4 zyzy; \
        T##4 zyzz; \
        T##4 zyzw; \
        T##4 zywx; \
        T##4 zywy; \
        T##4 zywz; \
        T##4 zyww; \
        T##4 zzxx; \
        T##4 zzxy; \
        T##4 zzxz; \
        T##4 zzxw; \
        T##4 zzyx; \
        T##4 zzyy; \
        T##4 zzyz; \
        T##4 zzyw; \
        T##4 zzzx; \
        T##4 zzzy; \
        T##4 zzzz; \
        T##4 zzzw; \
        T##4 zzwx; \
        T##4 zzwy; \
        T##4 zzwz; \
        T##4 zzww; \
        T##4 zwxx; \
        T##4 zwxy; \
        T##4 zwxz; \
        T##4 zwxw; \
        T##4 zwyx; \
        T##4 zwyy; \
        T##4 zwyz; \
        T##4 zwyw; \
        T##4 zwzx; \
        T##4 zwzy; \
        T##4 zwzz; \
        T##4 zwzw; \
        T##4 zwwx; \
        T##4 zwwy; \
        T##4 zwwz; \
        T##4 zwww; \
        T##4 wxxx; \
        T##4 wxxy; \
        T##4 wxxz; \
        T##4 wxxw; \
        T##4 wxyx; \
        T##4 wxyy; \
        T##4 wxyz; \
        T##4 wxyw; \
        T##4 wxzx; \
        T##4 wxzy; \
        T##4 wxzz; \
        T##4 wxzw; \
        T##4 wxwx; \
        T##4 wxwy; \
        T##4 wxwz; \
        T##4 wxww; \
        T##4 wyxx; \
        T##4 wyxy; \
        T##4 wyxz; \
        T##4 wyxw; \
        T##4 wyyx; \
        T##4 wyyy; \
        T##4 wyyz; \
        T##4 wyyw; \
        T##4 wyzx; \
        T##4 wyzy; \
        T##4 wyzz; \
        T##4 wyzw; \
        T##4 wywx; \
        T##4 wywy; \
        T##4 wywz; \
        T##4 wyww; \
        T##4 wzxx; \
        T##4 wzxy; \
        T##4 wzxz; \
        T##4 wzxw; \
        T##4 wzyx; \
        T##4 wzyy; \
        T##4 wzyz; \
        T##4 wzyw; \
        T##4 wzzx; \
        T##4 wzzy; \
        T##4 wzzz; \
        T##4 wzzw; \
        T##4 wzwx; \
        T##4 wzwy; \
        T##4 wzwz; \
        T##4 wzww; \
        T##4 wwxx; \
        T##4 wwxy; \
        T##4 wwxz; \
        T##4 wwxw; \
        T##4 wwyx; \
        T##4 wwyy; \
        T##4 wwyz; \
        T##4 wwyw; \
        T##4 wwzx; \
        T##4 wwzy; \
        T##4 wwzz; \
        T##4 wwzw; \
        T##4 wwwx; \
        T##4 wwwy; \
        T##4 wwwz; \
        T##4 wwww; \
    } T##8; \
    typedef struct T##16 { \
        T x, y, z, w, __spacer4, __spacer5, __spacer6, __spacer7, __spacer8, __spacer9, sa, sb, sc, sd, se, sf; \
        T s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, sA, sB, sC, sD, sE, sF; \
        T##8 lo, hi; \
        T##2 xx; \
        T##2 xy; \
        T##2 xz; \
        T##2 xw; \
        T##2 yx; \
        T##2 yy; \
        T##2 yz; \
        T##2 yw; \
        T##2 zx; \
        T##2 zy; \
        T##2 zz; \
        T##2 zw; \
        T##2 wx; \
        T##2 wy; \
        T##2 wz; \
        T##2 ww; \
        T##3 xxx; \
        T##3 xxy; \
        T##3 xxz; \
        T##3 xxw; \
        T##3 xyx; \
        T##3 xyy; \
        T##3 xyz; \
        T##3 xyw; \
        T##3 xzx; \
        T##3 xzy; \
        T##3 xzz; \
        T##3 xzw; \
        T##3 xwx; \
        T##3 xwy; \
        T##3 xwz; \
        T##3 xww; \
        T##3 yxx; \
        T##3 yxy; \
        T##3 yxz; \
        T##3 yxw; \
        T##3 yyx; \
        T##3 yyy; \
        T##3 yyz; \
        T##3 yyw; \
        T##3 yzx; \
        T##3 yzy; \
        T##3 yzz; \
        T##3 yzw; \
        T##3 ywx; \
        T##3 ywy; \
        T##3 ywz; \
        T##3 yww; \
        T##3 zxx; \
        T##3 zxy; \
        T##3 zxz; \
        T##3 zxw; \
        T##3 zyx; \
        T##3 zyy; \
        T##3 zyz; \
        T##3 zyw; \
        T##3 zzx; \
        T##3 zzy; \
        T##3 zzz; \
        T##3 zzw; \
        T##3 zwx; \
        T##3 zwy; \
        T##3 zwz; \
        T##3 zww; \
        T##3 wxx; \
        T##3 wxy; \
        T##3 wxz; \
        T##3 wxw; \
        T##3 wyx; \
        T##3 wyy; \
        T##3 wyz; \
        T##3 wyw; \
        T##3 wzx; \
        T##3 wzy; \
        T##3 wzz; \
        T##3 wzw; \
        T##3 wwx; \
        T##3 wwy; \
        T##3 wwz; \
        T##3 www; \
        T##4 xxxx; \
        T##4 xxxy; \
        T##4 xxxz; \
        T##4 xxxw; \
        T##4 xxyx; \
        T##4 xxyy; \
        T##4 xxyz; \
        T##4 xxyw; \
        T##4 xxzx; \
        T##4 xxzy; \
        T##4 xxzz; \
        T##4 xxzw; \
        T##4 xxwx; \
        T##4 xxwy; \
        T##4 xxwz; \
        T##4 xxww; \
        T##4 xyxx; \
        T##4 xyxy; \
        T##4 xyxz; \
        T##4 xyxw; \
        T##4 xyyx; \
        T##4 xyyy; \
        T##4 xyyz; \
        T##4 xyyw; \
        T##4 xyzx; \
        T##4 xyzy; \
        T##4 xyzz; \
        T##4 xyzw; \
        T##4 xywx; \
        T##4 xywy; \
        T##4 xywz; \
        T##4 xyww; \
        T##4 xzxx; \
        T##4 xzxy; \
        T##4 xzxz; \
        T##4 xzxw; \
        T##4 xzyx; \
        T##4 xzyy; \
        T##4 xzyz; \
        T##4 xzyw; \
        T##4 xzzx; \
        T##4 xzzy; \
        T##4 xzzz; \
        T##4 xzzw; \
        T##4 xzwx; \
        T##4 xzwy; \
        T##4 xzwz; \
        T##4 xzww; \
        T##4 xwxx; \
        T##4 xwxy; \
        T##4 xwxz; \
        T##4 xwxw; \
        T##4 xwyx; \
        T##4 xwyy; \
        T##4 xwyz; \
        T##4 xwyw; \
        T##4 xwzx; \
        T##4 xwzy; \
        T##4 xwzz; \
        T##4 xwzw; \
        T##4 xwwx; \
        T##4 xwwy; \
        T##4 xwwz; \
        T##4 xwww; \
        T##4 yxxx; \
        T##4 yxxy; \
        T##4 yxxz; \
        T##4 yxxw; \
        T##4 yxyx; \
        T##4 yxyy; \
        T##4 yxyz; \
        T##4 yxyw; \
        T##4 yxzx; \
        T##4 yxzy; \
        T##4 yxzz; \
        T##4 yxzw; \
        T##4 yxwx; \
        T##4 yxwy; \
        T##4 yxwz; \
        T##4 yxww; \
        T##4 yyxx; \
        T##4 yyxy; \
        T##4 yyxz; \
        T##4 yyxw; \
        T##4 yyyx; \
        T##4 yyyy; \
        T##4 yyyz; \
        T##4 yyyw; \
        T##4 yyzx; \
        T##4 yyzy; \
        T##4 yyzz; \
        T##4 yyzw; \
        T##4 yywx; \
        T##4 yywy; \
        T##4 yywz; \
        T##4 yyww; \
        T##4 yzxx; \
        T##4 yzxy; \
        T##4 yzxz; \
        T##4 yzxw; \
        T##4 yzyx; \
        T##4 yzyy; \
        T##4 yzyz; \
        T##4 yzyw; \
        T##4 yzzx; \
        T##4 yzzy; \
        T##4 yzzz; \
        T##4 yzzw; \
        T##4 yzwx; \
        T##4 yzwy; \
        T##4 yzwz; \
        T##4 yzww; \
        T##4 ywxx; \
        T##4 ywxy; \
        T##4 ywxz; \
        T##4 ywxw; \
        T##4 ywyx; \
        T##4 ywyy; \
        T##4 ywyz; \
        T##4 ywyw; \
        T##4 ywzx; \
        T##4 ywzy; \
        T##4 ywzz; \
        T##4 ywzw; \
        T##4 ywwx; \
        T##4 ywwy; \
        T##4 ywwz; \
        T##4 ywww; \
        T##4 zxxx; \
        T##4 zxxy; \
        T##4 zxxz; \
        T##4 zxxw; \
        T##4 zxyx; \
        T##4 zxyy; \
        T##4 zxyz; \
        T##4 zxyw; \
        T##4 zxzx; \
        T##4 zxzy; \
        T##4 zxzz; \
        T##4 zxzw; \
        T##4 zxwx; \
        T##4 zxwy; \
        T##4 zxwz; \
        T##4 zxww; \
        T##4 zyxx; \
        T##4 zyxy; \
        T##4 zyxz; \
        T##4 zyxw; \
        T##4 zyyx; \
        T##4 zyyy; \
        T##4 zyyz; \
        T##4 zyyw; \
        T##4 zyzx; \
        T##4 zyzy; \
        T##4 zyzz; \
        T##4 zyzw; \
        T##4 zywx; \
        T##4 zywy; \
        T##4 zywz; \
        T##4 zyww; \
        T##4 zzxx; \
        T##4 zzxy; \
        T##4 zzxz; \
        T##4 zzxw; \
        T##4 zzyx; \
        T##4 zzyy; \
        T##4 zzyz; \
        T##4 zzyw; \
        T##4 zzzx; \
        T##4 zzzy; \
        T##4 zzzz; \
        T##4 zzzw; \
        T##4 zzwx; \
        T##4 zzwy; \
        T##4 zzwz; \
        T##4 zzww; \
        T##4 zwxx; \
        T##4 zwxy; \
        T##4 zwxz; \
        T##4 zwxw; \
        T##4 zwyx; \
        T##4 zwyy; \
        T##4 zwyz; \
        T##4 zwyw; \
        T##4 zwzx; \
        T##4 zwzy; \
        T##4 zwzz; \
        T##4 zwzw; \
        T##4 zwwx; \
        T##4 zwwy; \
        T##4 zwwz; \
        T##4 zwww; \
        T##4 wxxx; \
        T##4 wxxy; \
        T##4 wxxz; \
        T##4 wxxw; \
        T##4 wxyx; \
        T##4 wxyy; \
        T##4 wxyz; \
        T##4 wxyw; \
        T##4 wxzx; \
        T##4 wxzy; \
        T##4 wxzz; \
        T##4 wxzw; \
        T##4 wxwx; \
        T##4 wxwy; \
        T##4 wxwz; \
        T##4 wxww; \
        T##4 wyxx; \
        T##4 wyxy; \
        T##4 wyxz; \
        T##4 wyxw; \
        T##4 wyyx; \
        T##4 wyyy; \
        T##4 wyyz; \
        T##4 wyyw; \
        T##4 wyzx; \
        T##4 wyzy; \
        T##4 wyzz; \
        T##4 wyzw; \
        T##4 wywx; \
        T##4 wywy; \
        T##4 wywz; \
        T##4 wyww; \
        T##4 wzxx; \
        T##4 wzxy; \
        T##4 wzxz; \
        T##4 wzxw; \
        T##4 wzyx; \
        T##4 wzyy; \
        T##4 wzyz; \
        T##4 wzyw; \
        T##4 wzzx; \
        T##4 wzzy; \
        T##4 wzzz; \
        T##4 wzzw; \
        T##4 wzwx; \
        T##4 wzwy; \
        T##4 wzwz; \
        T##4 wzww; \
        T##4 wwxx; \
        T##4 wwxy; \
        T##4 wwxz; \
        T##4 wwxw; \
        T##4 wwyx; \
        T##4 wwyy; \
        T##4 wwyz; \
        T##4 wwyw; \
        T##4 wwzx; \
        T##4 wwzy; \
        T##4 wwzz; \
        T##4 wwzw; \
        T##4 wwwx; \
        T##4 wwwy; \
        T##4 wwwz; \
        T##4 wwww; \
    } T##16;

CLDEFS_VECTORS(char)
CLDEFS_VECTORS(uchar)
CLDEFS_VECTORS(short)
CLDEFS_VECTORS(ushort)
CLDEFS_VECTORS(int)
CLDEFS_VECTORS(uint)
CLDEFS_VECTORS(long)
CLDEFS_VECTORS(ulong)
CLDEFS_VECTORS(half)
CLDEFS_VECTORS(float)
CLDEFS_VECTORS(double)


#undef CLDEFS_VECTORS


#define FLT_DIG	6
#define FLT_MANT_DIG	24
#define FLT_MAX_10_EXP	+38
#define FLT_MAX_EXP	+128
#define FLT_MIN_10_EXP	-37
#define FLT_MIN_EXP	-125
#define FLT_RADIX	2
#define FLT_MAX	0x1.fffffep127f
#define FLT_MIN	0x1.0p-126f
#define FLT_EPSILON	0x1.0p-23f
#define CHAR_BIT	8
#define INT_MAX	2147483647
#define INT_MIN	(-2147483647- 1)
#define LONG_MAX	0x7fffffffffffffffL
#define LONG_MIN	(-0x7fffffffffffffffL- 1)
#define SCHAR_MAX	127
#define SCHAR_MIN	(-127 - 1)
#define CHAR_MAX	SCHAR_MAX
#define CHAR_MIN	SCHAR_MIN
#define SHRT_MAX	32767
#define SHRT_MIN	(-32767- 1)
#define UCHAR_MAX	255
#define USHRT_MAX	65535
#define UINT_MAX	0xffffffff
#define ULONG_MAX	0xffffffffffffffffUL
#define DBL_DIG	15
#define DBL_MANT_DIG	53
#define DBL_MAX_10_EXP	+308
#define DBL_MAX_EXP	+1024
#define DBL_MIN_10_EXP	-307
#define DBL_MIN_EXP	-1021
#define DBL_MAX	0x1.fffffffffffffp1023
#define DBL_MIN	0x1.0p-1022
#define DBL_EPSILON	0x1.0p-52



typedef uint size_t;
typedef uint ptrdiff_t;
typedef uint intptr_t;
typedef uint uintptr_t;

typedef uint image1d_t;
typedef uint image1d_array_t;
typedef uint image1d_buffer_t;
typedef uint image2d_t;
typedef uint image2d_array_t;
typedef uint image3d_t;
typedef uint sampler_t;
typedef uint event_t;

#define complex
#define imaginary


//-----------------------------------------------------------------------------


static const int CLK_LOCAL_MEM_FENCE;
static const int CLK_GLOBAL_MEM_FENCE;

static const uint CLK_NORMALIZED_COORDS_FALSE;
static const uint CLK_NORMALIZED_COORDS_TRUE;
static const uint CLK_ADDRESS_NONE;
static const uint CLK_ADDRESS_CLAMP_TO_EDGE;
static const uint CLK_ADDRESS_REPEAT;
static const uint CLK_ADDRESS_CLAMP;
static const uint CLK_ADDRESS_MIRRORED_REPEAT;
static const uint CLK_FILTER_NEAREST;
static const uint CLK_FILTER_LINEAR;
static const uint CLK_R;
static const uint CLK_A;
static const uint CLK_RG;
static const uint CLK_RA;
static const uint CLK_RGB;
static const uint CLK_RGBA;
static const uint CLK_BGRA;
static const uint CLK_ARGB;
static const uint CLK_INTENSITY;
static const uint CLK_LUMINANCE;
static const uint CLK_Rx;
static const uint CLK_RGx;
static const uint CLK_RGBx;
static const uint CLK_SNORM_INT8;
static const uint CLK_SNORM_INT16;
static const uint CLK_UNORM_INT8;
static const uint CLK_UNORM_INT16;
static const uint CLK_UNORM_SHORT_565;
static const uint CLK_UNORM_SHORT_555;
static const uint CLK_UNORM_INT_101010;
static const uint CLK_SIGNED_INT8;
static const uint CLK_SIGNED_INT16;
static const uint CLK_SIGNED_INT32;
static const uint CLK_UNSIGNED_INT8;
static const uint CLK_UNSIGNED_INT16;
static const uint CLK_UNSIGNED_INT32;
static const uint CLK_HALF_FLOAT;
static const uint CLK_FLOAT;


//-----------------------------------------------------------------------------


// Define typedefs for gentypes
typedef void gentype;
typedef void gentypen;
typedef void sgentype;
typedef void ugentype;
typedef void charn;
typedef void ucharn;
typedef void shortn;
typedef void ushortn;
typedef void intn;
typedef void uintn;
typedef void longn;
typedef void ulongn;
typedef void floatn;
typedef void doublen;

// Fixed-type functions
uint get_work_dim();
size_t get_global_size(uint dimindx);
size_t get_global_id(uint dimindx);
size_t get_local_size(uint dimindx);
size_t get_local_id(uint dimindx);
size_t get_num_groups(uint dimindx);
size_t get_group_id(uint dimindx);
size_t get_global_offset(uint dimindx);
void barrier(int);
void mem_fence(int);
void read_mem_fence(int);
void write_mem_fence(int);

// Math functions
gentype acos(gentype);
gentype acosh(gentype);
gentype acospi(gentype);
gentype asin(gentype);
gentype asinh(gentype);
gentype asinpi(gentype);
gentype atan(gentype);
gentype atan2(gentype,gentype);
gentype atanh(gentype);
gentype atanpi(gentype);
gentype atan2pi(gentype,gentype);
gentype cbrt(gentype);
gentype ceil(gentype);
gentype copysign(gentype,gentype);
gentype cos(gentype);
gentype cosh(gentype);
gentype cospi(gentype);
gentype erf(gentype);
gentype erfc(gentype);
gentype exp(gentype);
gentype exp2(gentype);
gentype exp10(gentype);
gentype expm1(gentype);
gentype fabs(gentype);
gentype fdim(gentype,gentype);
gentype floor(gentype);
gentype fma(gentype,gentype,gentype);
gentype fmax(gentype,gentype);
gentype fmax(gentype,float);
gentype fmin(gentype,gentype);
gentype fmin(gentype,float);
gentype fmod(gentype,gentype);
gentype fract(gentype,__global gentype*);
gentype fract(gentype,__local gentype*);
gentype fract(gentype,__private gentype*);
gentype frexp(gentype,__global intn*);
gentype frexp(gentype,__local intn*);
gentype frexp(gentype,__private intn*);
gentype hypot(gentype,gentype);
intn ilogb(gentype);
gentype ldexp(gentype,intn);
gentype ldexp(gentype,int);
gentype lgamma(gentype);
gentype lgamma_r(gentype,__global intn*);
gentype lgamma_r(gentype,__local intn*);
gentype lgamma_r(gentype,__private intn*);
gentype log(gentype);
gentype log2(gentype);
gentype log10(gentype);
gentype log1p(gentype);
gentype logb(gentype);
gentype mad(gentype,gentype,gentype);
gentype maxmag(gentype,gentype);
gentype minmag(gentype,gentype);
gentype modf(gentype,__global gentype*);
gentype modf(gentype,__local gentype*);
gentype modf(gentype,__private gentype*);
floatn nan(uintn);
gentype nextafter(gentype,gentype);
gentype pow(gentype,gentype);
gentype pown(gentype,intn);
gentype powr(gentype,gentype);
gentype remainder(gentype,gentype);
gentype remquo(gentype,gentype,__global intn*);
gentype remquo(gentype,gentype,__local intn*);
gentype remquo(gentype,gentype,__private intn*);
gentype rint(gentype);
gentype rootn(gentype,intn);
gentype round(gentype);
gentype rsqrt(gentype);
gentype sin(gentype);
gentype sincos(gentype,__global gentype*);
gentype sincos(gentype,__local gentype*);
gentype sincos(gentype,__private gentype*);
gentype sinh(gentype);
gentype sinpi(gentype);
gentype sqrt(gentype);
gentype tan(gentype);
gentype tanh(gentype);
gentype tanpi(gentype);
gentype tgamma(gentype);
gentype trunc(gentype);

// half_ versions
gentype half_cos(gentype);
gentype half_divide(gentype,gentype);
gentype half_exp(gentype);
gentype half_exp2(gentype);
gentype half_exp10(gentype);
gentype half_log(gentype);
gentype half_log2(gentype);
gentype half_log10(gentype);
gentype half_powr(gentype,gentype);
gentype half_recip(gentype);
gentype half_rsqrt(gentype);
gentype half_sin(gentype);
gentype half_sqrt(gentype);
gentype half_tan(gentype);

// native_ versions
gentype native_cos(gentype);
gentype native_divide(gentype,gentype);
gentype native_exp(gentype);
gentype native_exp2(gentype);
gentype native_exp10(gentype);
gentype native_log(gentype);
gentype native_log2(gentype);
gentype native_log10(gentype);
gentype native_powr(gentype,gentype);
gentype native_recip(gentype);
gentype native_rsqrt(gentype);
gentype native_sin(gentype);
gentype native_sqrt(gentype);
gentype native_tan(gentype);

// Integer functions
ugentype abs(gentype);
ugentype abs_diff(gentype,gentype);
gentype add_sat(gentype,gentype);
gentype hadd(gentype,gentype);
gentype rhadd(gentype,gentype);
gentype clamp(gentype,gentype,gentype);
gentype clamp(gentype,sgentype,sgentype);
gentype clz(gentype);
gentype mad_hi(gentype,gentype,gentype);
gentype mad_sat(gentype,gentype,gentype);
gentype max(gentype,gentype);
gentype max(gentype,sgentype);
gentype min(gentype,gentype);
gentype min(gentype,sgentype);
gentype mul_hi(gentype,gentype);
gentype rotate(gentype,gentype);
gentype sub_sat(gentype,gentype);
short upsample(char,uchar);
ushort upsample(uchar,uchar);
shortn upsample(charn,ucharn);
ushortn upsample(ucharn,ucharn);
int upsample(short,ushort);
uint upsample(ushort,ushort);
intn upsample(shortn,ushortn);
uintn upsample(ushortn,ushortn);
long upsample(int,uint);
ulong upsample(uint,uint);
longn upsample(intn,uintn);
ulongn upsample(uintn,uintn);
gentype mad24(gentype,gentype,gentype);
gentype mul24(gentype,gentype,gentype);

// Common functions
gentype clamp(gentype,gentype,gentype);
gentype clamp(gentype,float,float);
gentype degrees(gentype);
gentype max(gentype,gentype);
gentype max(gentype,float);
gentype min(gentype,gentype);
gentype min(gentype,float);
gentype mix(gentype,gentype,gentype);
gentype mix(gentype,gentype,float);
gentype radians(gentype);
gentype step(gentype,gentype);
gentype step(float,gentype);
gentype smoothstep(gentype,gentype,gentype);
gentype step(float,float,gentype);
gentype sign(gentype);

// Geometric functions
float4 cross(float4,float4);
float3 cross(float3,float3);
float dot(floatn,floatn);
float distance(floatn,floatn);
float length(floatn);
floatn normalize(floatn);
float fast_distance(floatn,floatn);
float fast_length(floatn);
floatn fast_normalize(floatn);

// Relational functions
intn isequal(floatn,floatn);
intn isnotequal(floatn,floatn);
intn isgreater(floatn,floatn);
intn isgreaterequal(floatn,floatn);
intn isless(floatn,floatn);
intn islessequal(floatn,floatn);
intn islessgreater(floatn,floatn);
intn isfinite(floatn);
intn isinf(floatn);
intn isnan(floatn);
intn isnormal(floatn);
intn isordered(floatn,floatn);
intn isunordered(floatn,floatn);
intn signbit(floatn);
int any(gentype);
int all(gentype);
gentype bitselect(gentype,gentype,gentype);
gentype select(gentype,gentype,ugentype);
gentype select(gentype,gentype,igentype);


// Async Copies from Global to Local Memory, Local to Global Memory, and Prefetch
event_t async_work_group_copy(__global gentype*,const __local gentype*,size_t,event_t);
event_t async_work_group_copy(__local gentype*,const __global gentype*,size_t,event_t);
event_t async_work_group_strided_copy(__global gentype*,const __local gentype*,size_t,size_t,event_t);
event_t async_work_group_strided_copy(__local gentype*,const __global gentype*,size_t,size_t,event_t);
void wait_group_events(int num_events,event_t*);
void prefetch(const __global gentype*,size_t);

// Atomic Functions
int atomic_add(__global int*,int);
uint atomic_add(__global uint*,uint);
int __atomic_add_local(__local int*,int);
uint __atomic_add_local(__local uint*,uint);
int atomic_sub(__global int*,int);
uint atomic_sub(__global uint*,uint);
int __atomic_sub_local(__local int*,int);
uint __atomic_sub_local(__local uint*,uint);
int atomic_xchg(__global int*,int);
uint atomic_xchg(__global uint*,uint);
float atomic_xchg(__global float*,float);
int __atomic_xchg_local(__local int*,int);
uint __atomic_xchg_local(__local uint*,uint);
float __atomic_xchg_local(__local float*,float);
int atomic_inc(__global int*);
uint atomic_inc(__global uint*);
int __atomic_inc_local(__local int*);
uint __atomic_inc_local(__local uint*);
int atomic_dec(__global int*);
uint atomic_dec(__global uint*);
int __atomic_dec_local(__local int*);
uint __atomic_dec_local(__local uint*);
int atomic_cmpxchg(__global int*,int,int);
uint atomic_cmpxchg(__global uint*,uint,uint);
int __atomic_cmpxchg_local(__local int*,int,int);
uint __atomic_cmpxchg_local(__local uint*,uint,uint);
int atomic_min(__global int*,int);
uint atomic_min(__global uint*,uint);
int __atomic_min_local(__local int*,int);
uint __atomic_min_local(__local uint*,uint);
int atomic_max(__global int*,int);
uint atomic_max(__global uint*,uint);
int __atomic_max_local(__local int*,int);
uint __atomic_max_local(__local uint*,uint);
int atomic_and(__global int*,int);
uint atomic_and(__global uint*,uint);
int __atomic_and_local(__local int*,int);
uint __atomic_and_local(__local uint*,uint);
int atomic_or(__global int*,int);
uint atomic_or(__global uint*,uint);
int __atomic_or_local(__local int*,int);
uint __atomic_or_local(__local uint*,uint);
int atomic_xor(__global int*,int);
uint atomic_xor(__global uint*,uint);
int __atomic_xor_local(__local int*,int);
uint __atomic_xor_local(__local uint*,uint);

// Miscellaneous Vector Functions
int vec_step(gentype);
int vec_step(gentype);
int shuffle(gentype);
int shuffle2(gentype);

// Image read and write functions
float4 read_imagef(image2d_t,sampler_t,int2);
float4 read_imagef(image2d_t,sampler_t,float2);
int4 read_imagei(image2d_t,sampler_t,int2);
int4 read_imagei(image2d_t,sampler_t,float2);
uint4 read_imageui(image2d_t,sampler_t,int2);
uint4 read_imageui(image2d_t,sampler_t,float2);
void write_imagef(image2d_t,int2,float4);
void write_imagei(image2d_t,int2,int4);
void write_imageui(image2d_t,int2,uint4);
float4 read_imagef(image3d_t,sampler_t,int4);
float4 read_imagef(image3d_t,sampler_t,float4);
int4 read_imagei(image3d_t,sampler_t,int4);
int4 read_imagei(image3d_t,sampler_t,float4);
uint4 read_imageui(image3d_t,sampler_t,int4);
uint4 read_imageui(image3d_t,sampler_t,float4);
void write_imagef(image3d_t,int4,float4);
void write_imagei(image3d_t,int4,int4);
void write_imageui(image3d_t,int4,uint4);
int get_image_width(image2d_t);
int get_image_width(image3d_t);
int get_image_height(image2d_t);
int get_image_height(image3d_t);
int get_image_depth(image3d_t);
int get_image_channel_data_type(image2d_t);
int get_image_channel_data_type(image3d_t);
int get_image_channel_order(image2d_t);
int get_image_channel_order(image3d_t);
int2 get_image_dim(image2d_t);
int4 get_image_dim(image3d_t);

// as_type(n) functions
char as_char(gentype);
uchar as_uchar(gentype);
short as_short(gentype);
ushort as_ushort(gentype);
int as_int(gentype);
uint as_uint(gentype);
long as_long(gentype);
ulong as_ulong(gentype);
float as_float(gentype);
double as_double(gentype);
size_t as_size_t(gentype);
char2 as_char2(gentype);
uchar2 as_uchar2(gentype);
short2 as_short2(gentype);
ushort2 as_ushort2(gentype);
int2 as_int2(gentype);
uint2 as_uint2(gentype);
long2 as_long2(gentype);
ulong2 as_ulong2(gentype);
float2 as_float2(gentype);
double2 as_double2(gentype);
char3 as_char3(gentype);
uchar3 as_uchar3(gentype);
short3 as_short3(gentype);
ushort3 as_ushort3(gentype);
int3 as_int3(gentype);
uint3 as_uint3(gentype);
long3 as_long3(gentype);
ulong3 as_ulong3(gentype);
float3 as_float3(gentype);
double3 as_double3(gentype);
char3 as_char3(gentype);
uchar3 as_uchar3(gentype);
short3 as_short3(gentype);
ushort3 as_ushort3(gentype);
int3 as_int3(gentype);
uint3 as_uint3(gentype);
long3 as_long3(gentype);
ulong3 as_ulong3(gentype);
float3 as_float3(gentype);
double3 as_double3(gentype);
char4 as_char4(gentype);
uchar4 as_uchar4(gentype);
short4 as_short4(gentype);
ushort4 as_ushort4(gentype);
int4 as_int4(gentype);
uint4 as_uint4(gentype);
long4 as_long4(gentype);
ulong4 as_ulong4(gentype);
float4 as_float4(gentype);
double4 as_double4(gentype);
char8 as_char8(gentype);
uchar8 as_uchar8(gentype);
short8 as_short8(gentype);
ushort8 as_ushort8(gentype);
int8 as_int8(gentype);
uint8 as_uint8(gentype);
long8 as_long8(gentype);
ulong8 as_ulong8(gentype);
float8 as_float8(gentype);
double8 as_double8(gentype);
char16 as_char16(gentype);
uchar16 as_uchar16(gentype);
short16 as_short16(gentype);
ushort16 as_ushort16(gentype);
int16 as_int16(gentype);
uint16 as_uint16(gentype);
long16 as_long16(gentype);
ulong16 as_ulong16(gentype);
float16 as_float16(gentype);
double16 as_double16(gentype);

// vector data load and store functions
// TODO ...

// Conversion functions
// Default versions
char convert_char(gentype);
uchar convert_uchar(gentype);
short convert_short(gentype);
ushort convert_ushort(gentype);
int convert_int(gentype);
uint convert_uint(gentype);
long convert_long(gentype);
ulong convert_ulong(gentype);
float convert_float(gentype);
double convert_double(gentype);

char2 convert_char2(gentype);
uchar2 convert_uchar2(gentype);
short2 convert_short2(gentype);
ushort2 convert_ushort2(gentype);
int2 convert_int2(gentype);
uint2 convert_uint2(gentype);
long2 convert_long2(gentype);
ulong2 convert_ulong2(gentype);
float2 convert_float2(gentype);
double2 convert_double2(gentype);

char3 convert_char3(gentype);
uchar3 convert_uchar3(gentype);
short3 convert_short3(gentype);
ushort3 convert_ushort3(gentype);
int3 convert_int3(gentype);
uint3 convert_uint3(gentype);
long3 convert_long3(gentype);
ulong3 convert_ulong3(gentype);
float3 convert_float3(gentype);
double3 convert_double3(gentype);

char4 convert_char4(gentype);
uchar4 convert_uchar4(gentype);
short4 convert_short4(gentype);
ushort4 convert_ushort4(gentype);
int4 convert_int4(gentype);
uint4 convert_uint4(gentype);
long4 convert_long4(gentype);
ulong4 convert_ulong4(gentype);
float4 convert_float4(gentype);
double4 convert_double4(gentype);

char8 convert_char8(gentype);
uchar8 convert_uchar8(gentype);
short8 convert_short8(gentype);
ushort8 convert_ushort8(gentype);
int8 convert_int8(gentype);
uint8 convert_uint8(gentype);
long8 convert_long8(gentype);
ulong8 convert_ulong8(gentype);
float8 convert_float8(gentype);
double8 convert_double8(gentype);

char16 convert_char16(gentype);
uchar16 convert_uchar16(gentype);
short16 convert_short16(gentype);
ushort16 convert_ushort16(gentype);
int16 convert_int16(gentype);
uint16 convert_uint16(gentype);
long16 convert_long16(gentype);
ulong16 convert_ulong16(gentype);
float16 convert_float16(gentype);
double16 convert_double16(gentype);

// Saturated versions
// Default versions
char convert_char_sat(gentype);
uchar convert_uchar_sat(gentype);
short convert_short_sat(gentype);
ushort convert_ushort_sat(gentype);
int convert_int_sat(gentype);
uint convert_uint_sat(gentype);
long convert_long_sat(gentype);
ulong convert_ulong_sat(gentype);

char2 convert_char2_sat(gentype);
uchar2 convert_uchar2_sat(gentype);
short2 convert_short2_sat(gentype);
ushort2 convert_ushort2_sat(gentype);
int2 convert_int2_sat(gentype);
uint2 convert_uint2_sat(gentype);
long2 convert_long2_sat(gentype);
ulong2 convert_ulong2_sat(gentype);

char3 convert_char3_sat(gentype);
uchar3 convert_uchar3_sat(gentype);
short3 convert_short3_sat(gentype);
ushort3 convert_ushort3_sat(gentype);
int3 convert_int3_sat(gentype);
uint3 convert_uint3_sat(gentype);
long3 convert_long3_sat(gentype);
ulong3 convert_ulong3_sat(gentype);

char4 convert_char4_sat(gentype);
uchar4 convert_uchar4_sat(gentype);
short4 convert_short4_sat(gentype);
ushort4 convert_ushort4_sat(gentype);
int4 convert_int4_sat(gentype);
uint4 convert_uint4_sat(gentype);
long4 convert_long4_sat(gentype);
ulong4 convert_ulong4_sat(gentype);

char8 convert_char8_sat(gentype);
uchar8 convert_uchar8_sat(gentype);
short8 convert_short8_sat(gentype);
ushort8 convert_ushort8_sat(gentype);
int8 convert_int8_sat(gentype);
uint8 convert_uint8_sat(gentype);
long8 convert_long8_sat(gentype);
ulong8 convert_ulong8_sat(gentype);

char16 convert_char16_sat(gentype);
uchar16 convert_uchar16_sat(gentype);
short16 convert_short16_sat(gentype);
ushort16 convert_ushort16_sat(gentype);
int16 convert_int16_sat(gentype);
uint16 convert_uint16_sat(gentype);
long16 convert_long16_sat(gentype);
ulong16 convert_ulong16_sat(gentype);

// Functions provided by the cl_khr_fp64 extension (double support)
// Math functions
gentype acos(gentype);
gentype acosh(gentype);
gentype acospi(gentype);
gentype asin(gentype);
gentype asinh(gentype);
gentype asinpi(gentype);
gentype atan(gentype);
gentype atan2(gentype,gentype);
gentype atanh(gentype);
gentype atanpi(gentype);
gentype atan2pi(gentype,gentype);
gentype cbrt(gentype);
gentype ceil(gentype);
gentype copysign(gentype,gentype);
gentype cos(gentype);
gentype cosh(gentype);
gentype cospi(gentype);
gentype erf(gentype);
gentype erfc(gentype);
gentype exp(gentype);
gentype exp2(gentype);
gentype exp10(gentype);
gentype expm1(gentype);
gentype fabs(gentype);
gentype fdim(gentype,gentype);
gentype floor(gentype);
gentype fma(gentype,gentype,gentype);
gentype fmax(gentype,gentype);
gentype fmax(gentype,double);
gentype fmin(gentype,gentype);
gentype fmin(gentype,double);
gentype fmod(gentype,gentype);
gentype fract(gentype,__global gentype*);
gentype fract(gentype,__local gentype*);
gentype fract(gentype,__private gentype*);
gentype frexp(gentype,__global intn*);
gentype frexp(gentype,__local intn*);
gentype frexp(gentype,__private intn*);
gentype hypot(gentype,gentype);
intn ilogb(gentype);
gentype ldexp(gentype,intn);
gentype ldexp(gentype,int);
gentype lgamma(gentype);
gentype lgamma_r(gentype,__global intn*);
gentype lgamma_r(gentype,__local intn*);
gentype lgamma_r(gentype,__private intn*);
gentype log(gentype);
gentype log2(gentype);
gentype log10(gentype);
gentype log1p(gentype);
gentype logb(gentype);
gentype mad(gentype,gentype,gentype);
gentype maxmag(gentype,gentype);
gentype minmag(gentype,gentype);
gentype modf(gentype,__global gentype*);
gentype modf(gentype,__local gentype*);
gentype modf(gentype,__private gentype*);
floatn nan(uintn);
gentype nextafter(gentype,gentype);
gentype pow(gentype,gentype);
gentype pown(gentype,intn);
gentype powr(gentype,gentype);
gentype remainder(gentype,gentype);
gentype remquo(gentype,gentype,__global intn*);
gentype remquo(gentype,gentype,__local intn*);
gentype remquo(gentype,gentype,__private intn*);
gentype rint(gentype);
gentype rootn(gentype,intn);
gentype round(gentype);
gentype rsqrt(gentype);
gentype sin(gentype);
gentype sincos(gentype,__global gentype*);
gentype sincos(gentype,__local gentype*);
gentype sincos(gentype,__private gentype*);
gentype sinh(gentype);
gentype sinpi(gentype);
gentype sqrt(gentype);
gentype tan(gentype);
gentype tanh(gentype);
gentype tanpi(gentype);
gentype tgamma(gentype);
gentype trunc(gentype);

// Common functions
gentype clamp(gentype,gentype,gentype);
gentype clamp(gentype,double,double);
gentype degrees(gentype);
gentype max(gentype,gentype);
gentype max(gentype,double);
gentype min(gentype,gentype);
gentype min(gentype,double);
gentype mix(gentype,gentype,gentype);
gentype mix(gentype,gentype,double);
gentype radians(gentype);
gentype step(gentype,gentype);
gentype step(double,gentype);
gentype smoothstep(gentype,gentype,gentype);
gentype step(double,double,gentype);
gentype sign(gentype);

// Geometric functions
double4 cross(double4,double4);
double3 cross(double3,double3);
double dot(doublen,doublen);
double distance(doublen,doublen);
double length(doublen);
doublen normalize(doublen);

// Relational functions
longn isequal(doublen,doublen);
longn isnotequal(doublen,doublen);
longn isgreater(doublen,doublen);
longn isgreaterequal(doublen,doublen);
longn isless(doublen,doublen);
longn islessequal(doublen,doublen);
longn islessgreater(doublen,doublen);
longn isfinite(doublen);
longn isinf(doublen);
longn isnan(doublen);
longn isnormal(doublen);
longn isordered(doublen,doublen);
longn isunordered(doublen,doublen);
longn signbit(doublen);

int isequal(double,double);
int isnotequal(double,double);
int isgreater(double,double);
int isgreaterequal(double,double);
int isless(double,double);
int islessequal(double,double);
int islessgreater(double,double);
int isfinite(double);
int isinf(double);
int isnan(double);
int isnormal(double);
int isordered(double,double);
int isunordered(double,double);
int signbit(double);

// Functions provided by the cl_*_int32_*_atomics extensions
// Atomic Functions
int atom_add(__global int*,int);
uint atom_add(__global uint*,uint);
int __atom_add_local(__local int*,int);
uint __atom_add_local(__local uint*,uint);
int atom_sub(__global int*,int);
uint atom_sub(__global uint*,uint);
int __atom_sub_local(__local int*,int);
uint __atom_sub_local(__local uint*,uint);
int atom_xchg(__global int*,int);
uint atom_xchg(__global uint*,uint);
int __atom_xchg_local(__local int*,int);
uint __atom_xchg_local(__local uint*,uint);
int atom_inc(__global int*);
uint atom_inc(__global uint*);
int __atom_inc_local(__local int*);
uint __atom_inc_local(__local uint*);
int atom_dec(__global int*);
uint atom_dec(__global uint*);
int __atom_dec_local(__local int*);
uint __atom_dec_local(__local uint*);
int atom_cmpxchg(__global int*,int,int);
uint atom_cmpxchg(__global uint*,uint,uint);
int __atom_cmpxchg_local(__local int*,int,int);
uint __atom_cmpxchg_local(__local uint*,uint,uint);
int atom_min(__global int*,int);
uint atom_min(__global uint*,uint);
int __atom_min_local(__local int*,int);
uint __atom_min_local(__local uint*,uint);
int atom_max(__global int*,int);
uint atom_max(__global uint*,uint);
int __atom_max_local(__local int*,int);
uint __atom_max_local(__local uint*,uint);
int atom_and(__global int*,int);
uint atom_and(__global uint*,uint);
int __atom_and_local(__local int*,int);
uint __atom_and_local(__local uint*,uint);
int atom_or(__global int*,int);
uint atom_or(__global uint*,uint);
int __atom_or_local(__local int*,int);
uint __atom_or_local(__local uint*,uint);
int atom_xor(__global int*,int);
uint atom_xor(__global uint*,uint);
int __atom_xor_local(__local int*,int);
uint __atom_xor_local(__local uint*,uint);

// Functions provided by the cl_*_int64_*_atomics extensions
// Atomic Functions
long atom_add(__global long*,long);
ulong atom_add(__global ulong*,ulong);
long __atom_add_local(__local long*,long);
ulong __atom_add_local(__local ulong*,ulong);
long atom_sub(__global long*,long);
ulong atom_sub(__global ulong*,ulong);
long __atom_sub_local(__local long*,long);
ulong __atom_sub_local(__local ulong*,ulong);
long atom_xchg(__global long*,long);
ulong atom_xchg(__global ulong*,ulong);
long __atom_xchg_local(__local long*,long);
ulong __atom_xchg_local(__local ulong*,ulong);
long atom_inc(__global long*);
ulong atom_inc(__global ulong*);
long __atom_inc_local(__local long*);
ulong __atom_inc_local(__local ulong*);
long atom_dec(__global long*);
ulong atom_dec(__global ulong*);
long __atom_dec_local(__local long*);
ulong __atom_dec_local(__local ulong*);
long atom_cmpxchg(__global long*,long,long);
ulong atom_cmpxchg(__global ulong*,ulong,ulong);
long __atom_cmpxchg_local(__local long*,long,long);
ulong __atom_cmpxchg_local(__local ulong*,ulong,ulong);
long atom_min(__global long*,long);
ulong atom_min(__global ulong*,ulong);
long __atom_min_local(__local long*,long);
ulong __atom_min_local(__local ulong*,ulong);
long atom_max(__global long*,long);
ulong atom_max(__global ulong*,ulong);
long __atom_max_local(__local long*,long);
ulong __atom_max_local(__local ulong*,ulong);
long atom_and(__global long*,long);
ulong atom_and(__global ulong*,ulong);
long __atom_and_local(__local long*,long);
ulong __atom_and_local(__local ulong*,ulong);
long atom_or(__global long*,long);
ulong atom_or(__global ulong*,ulong);
long __atom_or_local(__local long*,long);
ulong __atom_or_local(__local ulong*,ulong);
long atom_xor(__global long*,long);
ulong atom_xor(__global ulong*,ulong);
long __atom_xor_local(__local long*,long);
ulong __atom_xor_local(__local ulong*,ulong);

// OpenCL 1.2 image functions
// Integer functions
gentype popcount(gentype);

// Image functions
float4 read_imagef(image1d_t,sampler_t,int);
float4 read_imagef(image1d_t,sampler_t,float);
int4 read_imagei(image1d_t,sampler_t,int);
int4 read_imagei(image1d_t,sampler_t,float);
uint4 read_imageui(image1d_t,sampler_t,int);
uint4 read_imageui(image1d_t,sampler_t,float);
void write_imagef(image1d_t,int,float4);
void write_imagei(image1d_t,int,int4);
void write_imageui(image1d_t,int,uint4);
float4 read_imagef(image1d_t,int);
int4 read_imagei(image1d_t,int);
uint4 read_imageui(image1d_t,int);
int get_image_width(image1d_t);

float4 read_imagef(image1d_buffer_t,int);
int4 read_imagei(image1d_buffer_t,int);
uint4 read_imageui(image1d_buffer_t,int);
void write_imagef(image1d_buffer_t,int,float4);
void write_imagei(image1d_buffer_t,int,int4);
void write_imageui(image1d_buffer_t,int,uint4);
int get_image_width(image1d_buffer_t);

float4 read_imagef(image1d_array_t,sampler_t,int2);
float4 read_imagef(image1d_array_t,sampler_t,float2);
int4 read_imagei(image1d_array_t,sampler_t,int2);
int4 read_imagei(image1d_array_t,sampler_t,float2);
uint4 read_imageui(image1d_array_t,sampler_t,int2);
uint4 read_imageui(image1d_array_t,sampler_t,float2);
void write_imagef(image1d_array_t,int2,float4);
void write_imagei(image1d_array_t,int2,int4);
void write_imageui(image1d_array_t,int2,uint4);
float4 read_imagef(image1d_array_t,int2);
int4 read_imagei(image1d_array_t,int2);
uint4 read_imageui(image1d_array_t,int2);
int get_image_width(image1d_array_t);
size_t get_image_array_size(image1d_array_t);

float4 read_imagef(image2d_array_t,sampler_t,int4);
float4 read_imagef(image2d_array_t,sampler_t,float4);
int4 read_imagei(image2d_array_t,sampler_t,int4);
int4 read_imagei(image2d_array_t,sampler_t,float4);
uint4 read_imageui(image2d_array_t,sampler_t,int4);
uint4 read_imageui(image2d_array_t,sampler_t,float4);
void write_imagef(image2d_array_t,int4,float4);
void write_imagei(image2d_array_t,int4,int4);
void write_imageui(image2d_array_t,int4,uint4);
float4 read_imagef(image2d_array_t,int4);
int4 read_imagei(image2d_array_t,int4);
uint4 read_imageui(image2d_array_t,int4);
int get_image_width(image2d_array_t);
int get_image_height(image2d_array_t);
size_t get_image_array_size(image2d_array_t);

float4 read_imagef(image2d_t,int2);
int4 read_imagei(image2d_t,int2);
uint4 read_imageui(image2d_t,int2);
float4 read_imagef(image3d_t,int);
int4 read_imagei(image3d_t,int);
uint4 read_imageui(image3d_t,int);


#endif
