#include "cldefs.h"

#define RAND_MAX UINT_MAX


// [0..RAND_MAX]
uint rand(long seed) {
    return (uint)(seed * 0x5DEECE66DL + 0xBL);
}

// [0..1] (the 1 is inclusive)
float frand(long seed) {
    return (float)rand(seed) / (float)RAND_MAX;
}

// [0..1) (the 1 is exclusive)
float frandexcl(long seed) {
    return (float)rand(seed) / ((float)RAND_MAX + 1.0);
}

