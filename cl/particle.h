#ifndef PARTICLE_H
#define PARTICLE_H

#include "simulationconstants.h"

#include "begin_shared.h"

#ifdef DEVICE
#include "grid.h"
#endif


// The size of a float unit in the integer representation for particle positions
#define INTEGER_FACTOR      65536


// Definition of the types
typedef struct particleBasic_t {
    uint id;
    uint posX, posY, posZ;  // higher two bytes: cell number / lower two bytes: coordinate within cell
} ParticleBasic;
typedef struct particleVelocity_t {
    float velX, velY, velZ;
    uchar4 color;
} ParticleVelocity;


// Host/Device shared functions
inline uint ParticleBasic_getID(const ParticleBasic *particle) {
    return particle->id;
}

inline uint3 ParticleBasic_getPositionInt(const ParticleBasic *particle) {
    uint3 pos = { particle->posX, particle->posY, particle->posZ };
    return pos;
}
inline void ParticleBasic_setPositionInt(ParticleBasic *particle, uint3 pos) {
#ifdef HOST
    particle->posX = pos.s[0];  particle->posY = pos.s[1];  particle->posZ = pos.s[2];
#else
    particle->posX = pos.x;     particle->posY = pos.y;     particle->posZ = pos.z;
#endif
}
inline float3 ParticleVelocity_getVelocity(const ParticleVelocity *particle) {
    float3 pos = { particle->velX, particle->velY, particle->velZ };
    return pos;
}
inline void ParticleVelocity_setVelocity(ParticleVelocity *particle, float3 vel) {
#ifdef HOST
    particle->velX = vel.s[0];  particle->velY = vel.s[1];  particle->velZ = vel.s[2];
#else
    particle->velX = vel.x;     particle->velY = vel.y;     particle->velZ = vel.z;
#endif
}

inline uchar4 ParticleVelocity_getColor(const ParticleVelocity *particle) {
    return particle->color;
}
inline void ParticleVelocity_setColor(ParticleVelocity *particle, uchar4 color) {
    particle->color = color;
}


// Device-only functions
#ifdef DEVICE
inline float3 ParticleBasic_getPosition(const ParticleBasic *particle) {
    float3 pos = convert_float3(ParticleBasic_getPositionInt(particle));
    return pos / INTEGER_FACTOR;
}
inline void ParticleBasic_setPosition(ParticleBasic *particle, float3 pos) {
    uint x = (uint)(int)round(pos.x * INTEGER_FACTOR) % (GRID_SIZE * INTEGER_FACTOR);
    uint y = (uint)(int)round(pos.y * INTEGER_FACTOR) % (GRID_SIZE * INTEGER_FACTOR);
    uint z = (uint)(int)round(pos.z * INTEGER_FACTOR) % (GRID_SIZE * INTEGER_FACTOR);
    ParticleBasic_setPositionInt(particle, (uint3)(x,y,z));
}
inline uint ParticleBasic_getCell24(const ParticleBasic *particle) {
    // The cell number contains 3 bits (triplets) for each level of a hierarchic binary grid (like a "full octree").
    // The most significant triplet indicates the child of the octree root.
    // The least significant triplet indicates the leaf.
    // The cell number covers 8 octree levels, i.e. is 24 bits large, hence the name of the function.
    // Each triplet itself encodes the X coordinate as its least and the Z coordinate as the most significant bit.
    return ((particle->posX >> 16) & 1)
        | (((particle->posY >> 16) & 1) << 1)
        | (((particle->posZ >> 16) & 1) << 2)
        | (((particle->posX >> 17) & 1) << 3)
        | (((particle->posY >> 17) & 1) << 4)
        | (((particle->posZ >> 17) & 1) << 5)
        | (((particle->posX >> 18) & 1) << 6)
        | (((particle->posY >> 18) & 1) << 7)
        | (((particle->posZ >> 18) & 1) << 8)
        | (((particle->posX >> 19) & 1) << 9)
        | (((particle->posY >> 19) & 1) << 10)
        | (((particle->posZ >> 19) & 1) << 11)
        | (((particle->posX >> 20) & 1) << 12)
        | (((particle->posY >> 20) & 1) << 13)
        | (((particle->posZ >> 20) & 1) << 14)
        | (((particle->posX >> 21) & 1) << 15)
        | (((particle->posY >> 21) & 1) << 16)
        | (((particle->posZ >> 21) & 1) << 17)
        | (((particle->posX >> 22) & 1) << 18)
        | (((particle->posY >> 22) & 1) << 19)
        | (((particle->posZ >> 22) & 1) << 20)
        | (((particle->posX >> 23) & 1) << 21)
        | (((particle->posY >> 23) & 1) << 22)
        | (((particle->posZ >> 23) & 1) << 23);
}
#endif





typedef struct spring_t {
    uint otherParticle;
    half restLength;
    half weight;
} Spring;

typedef struct particleSprings_t {
    Spring springs[32];
} ParticleSprings;

typedef struct particleRender_t {
    uchar color[3];
    uchar flags;
} ParticleRender;


#include "end_shared.h"

#endif
