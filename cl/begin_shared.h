#ifdef __OPENCL_CL_H

#define HOST

#define char cl_char
#define char2 cl_char2
#define char3 cl_char4
#define char4 cl_char4
#define char8 cl_char8
#define char16 cl_char16

#define uchar cl_uchar
#define uchar2 cl_uchar2
#define uchar3 cl_uchar4
#define uchar4 cl_uchar4
#define uchar8 cl_uchar8
#define uchar16 cl_uchar16

#define short cl_short
#define short2 cl_short2
#define short3 cl_short4
#define short4 cl_short4
#define short8 cl_short8
#define short16 cl_short16

#define ushort cl_ushort
#define ushort2 cl_ushort2
#define ushort3 cl_ushort4
#define ushort4 cl_ushort4
#define ushort8 cl_ushort8
#define ushort16 cl_ushort16

#define int cl_int
#define int2 cl_int2
#define int3 cl_int4
#define int4 cl_int4
#define int8 cl_int8
#define int16 cl_int16

#define uint cl_uint
#define uint2 cl_uint2
#define uint3 cl_uint4
#define uint4 cl_uint4
#define uint8 cl_uint8
#define uint16 cl_uint16

#define long cl_long
#define long2 cl_long2
#define long3 cl_long4
#define long4 cl_long4
#define long8 cl_long8
#define long16 cl_long16

#define ulong cl_ulong
#define ulong2 cl_ulong2
#define ulong3 cl_ulong4
#define ulong4 cl_ulong4
#define ulong8 cl_ulong8
#define ulong16 cl_ulong16

#define float cl_float
#define float2 cl_float2
#define float3 cl_float4
#define float4 cl_float4
#define float8 cl_float8
#define float16 cl_float16

#define double cl_double
#define double2 cl_double2
#define double3 cl_double4
#define double4 cl_double4
#define double8 cl_double8
#define double16 cl_double16

#define half cl_half

#else

#define DEVICE
#include "cldefs.h"

#endif
