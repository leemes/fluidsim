#include "cldefs.h"
#include "particle.h"
#include "simulationconstants.h"
#include "grid.h"
#include "rigidbody.h"
#include "rand.h"
#define forever for(;;)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// KERNEL COMPILATION CONSTANTS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Local work size
#ifndef GROUPSIZE
#define GROUPSIZE 32
#endif

#define SCENE_MODE_LIMITED  1
#define SCENE_MODE_WRAPPED  2
#define SCENE_MODE          SCENE_MODE_LIMITED

#define SCENE_BORDER        .05

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#if GROUPSIZE <= 32
#define barrier(x) ((void)0)
#endif

#if GROUPSIZE < 32
#error GROUPSIZE has to be at least 32.
#endif

#if GROUPSIZE == 32
#define GROUPSIZE_LOG 5
#elif GROUPSIZE == 64
#define GROUPSIZE_LOG 6
#elif GROUPSIZE == 128
#define GROUPSIZE_LOG 7
#elif GROUPSIZE == 256
#define GROUPSIZE_LOG 8
#elif GROUPSIZE == 512
#define GROUPSIZE_LOG 9
#elif GROUPSIZE == 1024
#define GROUPSIZE_LOG 10
#else
#error Invalid group size. Only powers of two and values in the range 32...1024 are supported by this program.
#endif


void keepInScene(float3 *position, float3 *velocity)
{
    // They should bounce on the walls of the whole cube
    bool collision = false;
    if (position->x < SCENE_BORDER) {
        position->x = SCENE_BORDER;
        velocity->x *= -.75;
        collision = true;
    } else if (position->x > GRID_SIZE - SCENE_BORDER) {
        position->x = GRID_SIZE - SCENE_BORDER;
        velocity->x *= -.75;
        collision = true;
    }
    if (position->y < SCENE_BORDER) {
        position->y = SCENE_BORDER;
        velocity->y *= -.75;
        collision = true;
    } else if (position->y > GRID_SIZE - SCENE_BORDER) {
        position->y = GRID_SIZE - SCENE_BORDER;
        velocity->y *= -.75;
        collision = true;
    }
    if (position->z < SCENE_BORDER) {
        position->z = SCENE_BORDER;
        velocity->z *= -.75;
        collision = true;
    } else if (position->z > GRID_SIZE - SCENE_BORDER) {
        position->z = GRID_SIZE - SCENE_BORDER;
        velocity->z *= -.75;
        collision = true;
    }
    if (collision)
        *velocity *= .75;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

float3 externalImpulse() {
    return GRAVITY * DT;
}
float3 applyExternalForces(float3 velocity)
{
    return velocity + externalImpulse();
}
float3 advancePosition(float3 position, float3 velocity)
{
    return position + velocity * DT;
}

// Position and velocity should be set to the values computed in the simulation. They will be modified accordingly.
void resolveCollisions(uint particleID, float3 *position, float3 *velocity,
                       __global RigidBodyData *rigidBodies, uint numRigidBodies)
{
    // The distance this particle should have of rigid objects
    // (This value is positive and different for each particle)
    float dThresh = 0.1 * frand(particleID);

    // Iterate through all rigid bodies in the scene
    for (uint r = 0; r < numRigidBodies; ++r) {
        RigidBodyData rigidBody = rigidBodies[r];
        float d = RigidBodyData_distance(&rigidBody, *position);
        if (d < dThresh) {
            // Compute the normal of the distance field
            float ddx = RigidBodyData_distance(&rigidBody, *position + (float3)( RIGIDBODY_GRADIENT_EPSILON, 0, 0 )) - d;
            float ddy = RigidBodyData_distance(&rigidBody, *position + (float3)( 0, RIGIDBODY_GRADIENT_EPSILON, 0 )) - d;
            float ddz = RigidBodyData_distance(&rigidBody, *position + (float3)( 0, 0, RIGIDBODY_GRADIENT_EPSILON )) - d;
            float3 n = { ddx, ddy, ddz };
            n = normalize(n);

            // Reflect velocity (imperfect)
            float3 vNormal = dot(*velocity, n) * n;
            float3 vTangent = *velocity - vNormal;
            *velocity = (0.003921f * rigidBody.reflectT) * vTangent
                      - (0.003921f * rigidBody.reflectN) * vNormal;

            // Move particle outside of the object
            *position += n * (dThresh - d);
        }
    }
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// BASIC INTEGRATION (apply external forces, predict new position according to velocity)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__kernel
void integrate(
        __global ParticleBasic *particles,
        __global ParticleVelocity *particlesVelocity,
        uint numParticles)
{
    const uint g = get_global_id(0);
    if (g >= numParticles)
        return;

    // Load
    ParticleBasic pb = particles[g];
    ParticleVelocity pv = particlesVelocity[g];
    float3 position = ParticleBasic_getPosition(&pb);
    float3 velocity = ParticleVelocity_getVelocity(&pv);

    // Integrate
    position = advancePosition(position, velocity);
    velocity = applyExternalForces(velocity);

    // Save
    keepInScene(&position, &velocity);
    ParticleBasic_setPosition(&pb, position);
    ParticleVelocity_setVelocity(&pv, velocity);
    particles[g] = pb;
    particlesVelocity[g] = pv;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// RELAXATION OF FLUID CONSTRAINTS (viscosity, air drag, density, springs?)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void relaxParticle(
        __global const ParticleBasic *particlesSrc, __global const ParticleVelocity *particlesVelocitySrc,
        __global ParticleBasic *particlesDst, __global ParticleVelocity *particlesVelocityDst,
        uint particleIndex, uint particleCellEnd,
        __local uint adjacentCellStarts[27], __local uint adjacentCellCounts[27], uint adjacentCount,
        __global RigidBodyData *rigidBodies, uint numRigidBodies)
{
    const uint l = get_local_id(0);

    __local float localPosX[GROUPSIZE];
    __local float localPosY[GROUPSIZE];
    __local float localPosZ[GROUPSIZE];
    __local float localVelX[GROUPSIZE];
    __local float localVelY[GROUPSIZE];
    __local float localVelZ[GROUPSIZE];



    // LOAD

    ParticleBasic pb;
    ParticleVelocity pv;
    if (particleIndex < particleCellEnd) {
        pb = particlesSrc[particleIndex];
        pv = particlesVelocitySrc[particleIndex];
    }
    const float3 position = ParticleBasic_getPosition(&pb);
    const float3 velocity = ParticleVelocity_getVelocity(&pv);



    // ACCUMULATE NEIGHBORHOOD

    float density = 0.0f;                       // sum of: (1 - q)^2
    float densityNear = 0.0f;                   // sum of: (1 - q)^3
    float3 kernelDistances = 0.0f;              // sum of: normalize(r) * (1 - q)
    float3 kernelDistancesNear = 0.0f;          // sum of: normalize(r) * (1 - q)^2
    float3 viscosityImpulseLinear = 0.0f;       // sum of: normalize(r) * (1 - q) * u
    float3 viscosityImpulseQuadratic = 0.0f;    // sum of: normalize(r) * (1 - q) * u^2
    float3 totalVelocity = 0.0f;                // sum of: v
    float velocityVariance = 0.0f;              // sum of: u

    // These loops effectively iterate through adjacent cells and find relevant particles (i.e. with distance <= 1)
    uint currentCell = 0;
    uint currentOffsetInCell = l;
    for (uint groupStart = 0; groupStart < adjacentCount; groupStart += GROUPSIZE)
    {
        // Number of particles we load in this round. Only the very last round is not necessarily "full".
        uint bufferSize = min((uint)GROUPSIZE, adjacentCount - groupStart);

        // Find the cell from which this work item should load a particle
        while (currentCell < 27 && currentOffsetInCell >= adjacentCellCounts[currentCell]) {
            // jump to next cell, reduce the "offset within the cell" accordingly
            currentOffsetInCell -= adjacentCellCounts[currentCell];
            ++currentCell;
        }

        // Only load if this work item is within the buffer of this round
        if (l < bufferSize) {
            // Load data from global mem
            const uint p = adjacentCellStarts[currentCell] + currentOffsetInCell;
            const ParticleBasic pbOther = particlesSrc[p];
            const ParticleVelocity pvOther = particlesVelocitySrc[p];
            float3 positionOther = ParticleBasic_getPosition(&pbOther);
            float3 velocityOther = ParticleVelocity_getVelocity(&pvOther);

            // Store in local mem
            localPosX[l] = positionOther.x;
            localPosY[l] = positionOther.y;
            localPosZ[l] = positionOther.z;
            localVelX[l] = velocityOther.x;
            localVelY[l] = velocityOther.y;
            localVelZ[l] = velocityOther.z;
        }
        barrier(CLK_LOCAL_MEM_FENCE);

        // For the next round:
        currentOffsetInCell += GROUPSIZE;


        if (particleIndex < particleCellEnd)
        {
            for (uint j = 0; j < bufferSize; ++j)
            {
                // Load data from local mem
                const float3 positionOther = {localPosX[j], localPosY[j], localPosZ[j]};
                const float3 velocityOther = {localVelX[j], localVelY[j], localVelZ[j]};

                // Compute distance
                const float3 positionDiff = positionOther - position;
                const float rSquare = dot(positionDiff, positionDiff);

                // Is particle relevant?
                if (rSquare <= 1.0f && rSquare > 0.0f)
                {
                    // Compute distance kernel coefficients
                    const float r = native_sqrt(rSquare);
                    const float k1 = (1.0f - r);
                    const float k2 = k1 * k1;
                    const float k3 = k2 * k1;
                    const float3 positionDiffNormal = positionDiff / r;

                    // Density and near density
                    density += k2;
                    densityNear += k3;
                    kernelDistances += positionDiffNormal * k1;
                    kernelDistancesNear += positionDiffNormal * k2;

                    // Viscosity impulse
                    const float u = dot(velocity - velocityOther, positionDiffNormal);
                    const float3 impulseLinear = (k1 * u) * positionDiffNormal;
                    viscosityImpulseLinear += impulseLinear;
                    viscosityImpulseQuadratic += impulseLinear * u;

                    // Coefficients for hotness computation
                    totalVelocity += velocityOther;
                    velocityVariance += length(velocity - velocityOther);
                }
            }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }



    // COMPUTE IMPULSE

    // viscosity
    float3 force = -VISCOSITY_SIGMA * viscosityImpulseLinear
            - VISCOSITY_BETA * viscosityImpulseQuadratic;

    // air drag
    const float airInfluenceFactor = density ? dot(kernelDistancesNear, kernelDistancesNear) : 3.0f; // Free-standing particles need to be considered separately
    force -= VISCOSITY_AIR_DRAG * (.001f + airInfluenceFactor / (1.0f + airInfluenceFactor)) * (velocity - WIND);

    // density relaxation
    force -= DENSITY_K * (density - DENSITY_NOMINAL) * kernelDistances
            + DENSITY_K_NEAR * densityNear * kernelDistancesNear;

    float3 impulse = 0.5 * DT * force;



    // APPLY IMPULSE

    const float impulseSquared = dot(impulse, impulse);
    if (impulseSquared > IMPULSE_MAX * IMPULSE_MAX) {
        impulse *= IMPULSE_MAX / native_sqrt(impulseSquared);
    }
    float3 newVelocity = velocity + impulse;

    const float newVelocitySquared = dot(newVelocity, newVelocity);
    if (newVelocitySquared > VELOCITY_MAX * VELOCITY_MAX) {
        newVelocity *= VELOCITY_MAX / native_sqrt(newVelocitySquared);
        impulse = newVelocity - velocity;
    }
    float3 newPosition = position + impulse * DT;



    // PARTICLE-DEPENDANT JITTER

    uint id = ParticleBasic_getID(&pb);
    float3 jitter = {
        -1.0f + 2.0f * frand(id + position.x * 10000.f),
        -1.0f + 2.0f * frand(id + position.y * 10000.f),
        -1.0f + 2.0f * frand(id + position.z * 10000.f)
    };
    newPosition += jitter * JITTER;


    // COLLISION WITH RIGID OBJECTS

    resolveCollisions(id, &newPosition, &newVelocity, rigidBodies, numRigidBodies);
    keepInScene(&newPosition, &newVelocity);



    // COLOR

    uchar4 color;
    color = (uchar4)(
                (uchar)clamp(densityNear*15.0f, 0.0f, 255.0f),
                (uchar)clamp(densityNear*15.0f, 0.0f, 255.0f),
                (uchar)clamp(density*15.0f, 0.0f, 255.0f),
                60
            );



    // SAVE

    if (particleIndex < particleCellEnd)
    {
        ParticleBasic_setPosition(&pb, newPosition);
        ParticleVelocity_setVelocity(&pv, newVelocity);
        ParticleVelocity_setColor(&pv, color);
        particlesDst[particleIndex] = pb;
        particlesVelocityDst[particleIndex] = pv;
    }
}


void relaxCell(
        __global const ParticleBasic *particlesSrc, __global const ParticleVelocity *particlesVelocitySrc,
        __global ParticleBasic *particlesDst, __global ParticleVelocity *particlesVelocityDst,
        uint cellIndex, __global uint *gridCellStarts,
        __global RigidBodyData *rigidBodies, uint numRigidBodies)
{
    const uint l = get_local_id(0);

    // For the per-particle processing, every work item should enter the function.
    // So we round up the cell interval to a multiple of GROUPSIZE ("virtual interval").
    // If more particles are in a cell than GROUPSIZE, we have to do multiple iterations (very seldom)
    const uint thisCellBegin = gridCellStarts[cellIndex];
    const uint thisCellEnd = gridCellStarts[cellIndex + 1];

    // Collect cell intervals for the 3x3x3 block of cells
    __local uint adjacentCellStarts[27];
    __local uint adjacentCellCounts[27];

    lookupAdjacentCellsUsing27WorkItems(cellIndex, gridCellStarts, adjacentCellStarts, adjacentCellCounts);
    barrier(CLK_LOCAL_MEM_FENCE);

    // Count the total number of adjacent particles
    uint adjacentCount = 0;
    #pragma unroll
    for (uint c = 0; c < 27; ++c)
        adjacentCount += adjacentCellCounts[c];

    for (uint currentBase = thisCellBegin; currentBase < thisCellEnd; currentBase += GROUPSIZE)
    {
        uint thisIndex = currentBase + l;

        relaxParticle(particlesSrc, particlesVelocitySrc,
                      particlesDst, particlesVelocityDst,
                      thisIndex, thisCellEnd,
                      adjacentCellStarts, adjacentCellCounts, adjacentCount,
                      rigidBodies, numRigidBodies);
    }
}


__kernel
__attribute__((reqd_work_group_size(GROUPSIZE, 1, 1)))
void relax(
        __global const ParticleBasic *particlesSrc, __global const ParticleVelocity *particlesVelocitySrc,
        __global ParticleBasic *particlesDst, __global ParticleVelocity *particlesVelocityDst,
        __global uint *gridCellStarts, __global uint *usedGridCells,
        __global RigidBodyData *rigidBodies, uint numRigidBodies)
{
    uint cellGroupIndex = get_group_id(0);

    // first cell for this work group
    uint cellIndex = usedGridCells[cellGroupIndex];

    while (cellIndex != GRID_CELL_EMPTY)
    {
        relaxCell(particlesSrc, particlesVelocitySrc,
                  particlesDst, particlesVelocityDst,
                  cellIndex, gridCellStarts,
                  rigidBodies, numRigidBodies);

        // Fetch the next cell group:
        cellGroupIndex += get_num_groups(0);
        cellIndex = usedGridCells[cellGroupIndex];
    }
}

