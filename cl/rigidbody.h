#ifndef CL_RIGIDBODY_H
#define CL_RIGIDBODY_H

#include "begin_shared.h"

#define RIGIDBODY_TYPE_BOX      1
#define RIGIDBODY_TYPE_SPHERE   2

#define RIGIDBODY_GRADIENT_EPSILON .01


// Definition of the types

// Type extensions. They are put in a union in RigidBodyData
typedef struct RigidBodyDataExBox_t {
    // Size in both directions measured from the origin (so the actual size of the box is twice this value)
    float3 size;
} RigidBodyDataExBox;
typedef struct RigidBodyDataExSphere_t {
    // Sphere radius
    float radius;
} RigidBodyDataExSphere;

typedef struct RigidBodyData_t {
    // Origin of the rigid body
    float origin[3];

    // If inverted = 1: The inner and outer parts are swapped
    uchar inverted;

    // Surface reflection property (friction and elasticity)
    uchar reflectT; // 0: full friction    ... 255: slippery
    uchar reflectN; // 0: stick to surface ... 255: perfect bounce in normal direction

    // Type description for the following union
    uchar type;
    // Type-specific data
    union {
        RigidBodyDataExBox box;
        RigidBodyDataExSphere sphere;
    } ex;
} RigidBodyData;




#ifdef DEVICE

// DISTANCE FUNCTIONS for rigid bodies.
// Positive numbers are outside, negative numbers inside of the object.
// Source: http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm

inline float RigidBodyData_distanceBox(const RigidBodyDataExBox *box, float3 p)
{
    float3 d = fabs(p) - box->size;
    return min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));
}
inline float RigidBodyData_distanceSphere(const RigidBodyDataExSphere *sphere, float3 p)
{
    return length(p) - sphere->radius;
}

// General distance function. Chooses the correct function depending on the object type.
inline float RigidBodyData_distance(const RigidBodyData *object, float3 sample)
{
    // Translation
    sample -= (float3)(object->origin[0], object->origin[1], object->origin[2]);

    // Forward to correct distance function, depending on the type
    float d;
    switch (object->type) {
    case RIGIDBODY_TYPE_BOX:    d = RigidBodyData_distanceBox   (&object->ex.box   , sample); break;
    case RIGIDBODY_TYPE_SPHERE: d = RigidBodyData_distanceSphere(&object->ex.sphere, sample); break;
    default: d = 0.0f;
    }

    // Maybe invert the result
    return object->inverted ? -d : d;
}

#endif // DEVICE

#include "end_shared.h"

#endif // CL_RIGIDBODY_H
