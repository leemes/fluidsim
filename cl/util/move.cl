#include "../cldefs.h"

#define MODE_OLDNEW 0
#define MODE_NEWOLD 1

#ifndef GROUPSIZE
#define GROUPSIZE 256
#endif
#ifndef T_IN
#define T_IN uint
#endif
#ifndef T_OUT
#define T_OUT uint
#endif
#ifndef MODE
#define MODE MODE_OLDNEW
#endif

#ifndef OUTPUT
#define OUTPUT(element,index) element
#endif



inline T_OUT getOutput(__global const T_IN *input, const uint index) {
#define element     (input[index])
    return OUTPUT(element, index);
#undef element
}


__kernel __attribute__((reqd_work_group_size(GROUPSIZE, 1, 1)))
void move(
        __global const T_IN *in,
        __global T_OUT *out,
        uint numElements,
        __global uint *moveTable,
        int oldIndexDelta,
        int newIndexDelta)
{
    const uint g = get_global_id(0);

    if (g >= numElements)
        return;

    const uint t = moveTable[g];

#ifdef ONLY_IF_LEFT_DIFFERS
    if (g && moveTable[g-1] == t)
        return;
#endif

#if MODE == MODE_OLDNEW
    const int oldIndex = g + oldIndexDelta;
    const int newIndex = t + newIndexDelta;
#else
    const int oldIndex = t + oldIndexDelta;
    const int newIndex = g + newIndexDelta;
#endif

    if (oldIndex >= 0 && newIndex >= 0)
        out[newIndex] = getOutput(in, oldIndex);
}
