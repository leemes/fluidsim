#include "../cldefs.h"

#ifndef T
#define T uint
#endif

#if T == char
#define T_MIN CHAR_MIN
#define T_MAX CHAR_MAX
#elif T == uchar
#define T_MIN 0
#define T_MAX UCHAR_MAX
#elif T == short
#define T_MIN SHRT_MIN
#define T_MAX SHRT_MAX
#elif T == ushort
#define T_MIN 0
#define T_MAX USHRT_MAX
#elif T == int
#define T_MIN INT_MIN
#define T_MAX INT_MAX
#elif T == uint
#define T_MIN 0
#define T_MAX UINT_MAX
#elif T == long
#define T_MIN LONG_MIN
#define T_MAX LONG_MAX
#elif T == ulong
#define T_MIN 0
#define T_MAX ULONG_MAX
#endif

#ifndef REDUCE
#define REDUCE(a,b) (a + b)
#endif

#ifndef NEUTRAL_ELEMENT
#define NEUTRAL_ELEMENT 0
#endif

// Reduction width of a single item
#ifndef W
#define W 16
#endif

#define CONCAT_(A,B) A##B
#define CONCAT(A,B) CONCAT_(A,B)
#define T2 CONCAT(T,2)
#define T4 CONCAT(T,4)
#define T8 CONCAT(T,8)
#define T16 CONCAT(T,16)
#define TW CONCAT(T,W)




inline T reduce2(const T a, const T b) {
    return REDUCE(a, b);
}

inline T vecReduce2(const T2 v) {
    return reduce2(v.x, v.y);
}
inline T vecReduce4(const T4 v) {
    return reduce2(vecReduce2(v.lo), vecReduce2(v.hi));
}
inline T vecReduce8(const T8 v) {
    return reduce2(vecReduce4(v.lo), vecReduce4(v.hi));
}
inline T vecReduce16(const T16 v) {
    return reduce2(vecReduce8(v.lo), vecReduce8(v.hi));
}

inline T vecReduce(const TW v) {
    return CONCAT(vecReduce,W)(v);
}



#define READ(i) \
    ((i) < numElements ? in[i] : NEUTRAL_ELEMENT)

#define REDUCTIONSTEP \
    stride = currentSize / 2; \
    if (l + stride < currentSize) \
        localBlock[l] = reduce2(localBlock[l], localBlock[l + stride]); \
    currentSize /= 2;


__kernel void reduce(
        const __global T* in,
        __global T* out,
        uint numElements,
        __local T* localBlock)
{
    const uint g = get_global_id(0);
    const uint l = get_local_id(0);
    const uint ls = get_local_size(0);
    const uint gr = get_group_id(0);

    // Read W elements from the input
    const uint base = W*gr*ls + l;
    uint stride = ls;
    TW val = {
        READ(base)
        ,READ(base + stride)
#if W > 2
        ,READ(base + 2 * stride)
        ,READ(base + 3 * stride)
#if W > 4
        ,READ(base + 4 * stride)
        ,READ(base + 5 * stride)
        ,READ(base + 6 * stride)
        ,READ(base + 7 * stride)
#if W > 8
        ,READ(base + 8 * stride)
        ,READ(base + 9 * stride)
        ,READ(base + 10 * stride)
        ,READ(base + 11 * stride)
        ,READ(base + 12 * stride)
        ,READ(base + 13 * stride)
        ,READ(base + 14 * stride)
        ,READ(base + 15 * stride)
#endif
#endif
#endif
    };

    // Reduce the first step and write to local memory:
    localBlock[l] = vecReduce(val);

    // Sync threads
    barrier(CLK_LOCAL_MEM_FENCE);

    // The number of elements to be reduced in this work group equals its size
    uint currentSize = ls;

    switch (currentSize) {
    case 16384: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case  8192: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case  4096: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case  2048: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case  1024: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case   512: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case   256: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case   128: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case    64: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case    32: REDUCTIONSTEP   barrier(CLK_LOCAL_MEM_FENCE);
    case    16: REDUCTIONSTEP
    case     8: REDUCTIONSTEP
    case     4: REDUCTIONSTEP
    case     2: REDUCTIONSTEP
    }

    if (l == 0)
        out[gr] = localBlock[0];
}
