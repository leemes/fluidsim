#include "../cldefs.h"

#define MODE_INCLUSIVE 1
#define MODE_EXCLUSIVE 2

#ifndef T
#define T uint
#endif
#ifndef MODE
#define MODE MODE_INCLUSIVE
#endif


// Reduction width of a single item
#ifndef W
#define W 16
#endif

#define CONCAT_(A,B) A##B
#define CONCAT(A,B) CONCAT_(A,B)
#define TW CONCAT(T,W)


// Aviod bank conflicts
#define OFFSET(A) ((A) + (A) / 32)


inline T vecLast(TW v) {
#if W == 2
    return v.s1;
#elif W == 4
    return v.s3;
#elif W == 8
    return v.s7;
#elif W == 16
    return v.sF;
#endif
}

inline TW vecScan(TW v) {
    return (TW)(v.s0
               ,v.s0+v.s1
            #if W > 2
               ,v.s0+v.s1+v.s2
               ,v.s0+v.s1+v.s2+v.s3
            #if W > 4
               ,v.s0+v.s1+v.s2+v.s3+v.s4
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7
            #if W > 8
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC+v.sD
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC+v.sD+v.sE
               ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC+v.sD+v.sE+v.sF
            #endif
            #endif
            #endif
    );
}


void upStep(__private uint *stride, __private uint *busy, __local T *localBlock)
{
    uint l = get_local_id(0);
    uint off = (2 * (l + 1)) * (*stride) - 1;
    if (off < get_local_size(0))
        localBlock[OFFSET(off)] += localBlock[OFFSET(off - *stride)];
    *stride *= 2;
    *busy /= 2;
}

void downStep(__private uint *stride, __private uint *busy, __local T *localBlock)
{
    uint l = get_local_id(0);
    uint off = (2 * (l + 1)) * (*stride) - 1;
    if (off < get_local_size(0)) {
        uint rOff = off;
        uint lOff = rOff - *stride;
        T lVal = localBlock[OFFSET(lOff)];
        T rVal = localBlock[OFFSET(rOff)];
        localBlock[OFFSET(lOff)] = rVal;
        localBlock[OFFSET(rOff)] = lVal + rVal;
    }
    *stride /= 2;
    *busy *= 2;
}


__kernel
void scan(
        __global TW *in,
        __global TW *out,
        //uint numElements,
        __global T *higherLevelArray,
        __local T* localBlock)
{
    const uint g = get_global_id(0);
    const uint l = get_local_id(0);
    const uint ls = get_local_size(0);
    const uint gr = get_group_id(0);

    // Initial sum (W:1)
    const TW val = in[g];
    const TW valScanned = vecScan(val);
    localBlock[OFFSET(l)] = vecLast(valScanned); // last element of the vector scan is the sum of all (original) vector fields, which we use to calculate the local PPS

    // Current offset between the children (offset per work item is twice as large)
    uint stride = 1;
    // Current number of busy work items
    // FIXME: Get rid of "busy"
    uint busy = ls;

    // Up-sweep (do up to the last 5 steps without (pre-)barrier)
    switch(busy) {
    case 1<<10: barrier(CLK_LOCAL_MEM_FENCE); upStep(&stride, &busy, localBlock);
    case 1<< 9: barrier(CLK_LOCAL_MEM_FENCE); upStep(&stride, &busy, localBlock);
    case 1<< 8: barrier(CLK_LOCAL_MEM_FENCE); upStep(&stride, &busy, localBlock);
    case 1<< 7: barrier(CLK_LOCAL_MEM_FENCE); upStep(&stride, &busy, localBlock);
    case 1<< 6: barrier(CLK_LOCAL_MEM_FENCE); upStep(&stride, &busy, localBlock);
    case 1<< 5:                               upStep(&stride, &busy, localBlock);
    case 1<< 4:                               upStep(&stride, &busy, localBlock);
    case 1<< 3:                               upStep(&stride, &busy, localBlock);
    case 1<< 2:                               upStep(&stride, &busy, localBlock);
    case 1<< 1:                               upStep(&stride, &busy, localBlock);
    }

    // Set last element to 0
    T total = 0;
    if (l == 0) {
        total = localBlock[OFFSET(ls - 1)];
        localBlock[OFFSET(ls - 1)] = 0;
    }

    // Down-sweep (do up to the first 4 steps without (post-)barrier, but after the 5th insert a barrier)
    stride = ls / 2;
    switch(stride) {
    default:    downStep(&stride, &busy, localBlock);
    case 1<< 2: downStep(&stride, &busy, localBlock);
    case 1<< 1: downStep(&stride, &busy, localBlock);
    case 1<< 0: downStep(&stride, &busy, localBlock);
    }
    switch(stride) {
    case 1<< 5: downStep(&stride, &busy, localBlock); barrier(CLK_LOCAL_MEM_FENCE);
    case 1<< 4: downStep(&stride, &busy, localBlock); barrier(CLK_LOCAL_MEM_FENCE);
    case 1<< 3: downStep(&stride, &busy, localBlock); barrier(CLK_LOCAL_MEM_FENCE);
    case 1<< 2: downStep(&stride, &busy, localBlock); barrier(CLK_LOCAL_MEM_FENCE);
    case 1<< 1: downStep(&stride, &busy, localBlock); barrier(CLK_LOCAL_MEM_FENCE);
    case 1<< 0: downStep(&stride, &busy, localBlock); barrier(CLK_LOCAL_MEM_FENCE);
    }

    const T myLocal = localBlock[OFFSET(l)];

    // Write back
#if MODE == MODE_INCLUSIVE
    out[g] = valScanned + (TW)(myLocal);
#else
    out[g] = valScanned + (TW)(myLocal) - val;
#endif

    // The first item writes into the higher level array
    if (higherLevelArray && l == 0) {
        higherLevelArray[gr] = total;
    }
}


__kernel
void scanAdd(
        __global TW* array,
        __global T* higherLevelArray)
{
    const uint g = get_global_id(0);
    const uint gr = get_group_id(0);

    // We need to add the "exclusively scanned" higherLevelArray. But it is always scanned in the same mode than the first level.
    // So we simply "undo" the inclusion when scanned inclusively to get an equivalent of an exclusive scan.
    // To undo the inclusion, we shift the higherLevelArray by one position.
    // The first element of the exclusive scan is always zero and thus never needed, since we only add sth to non-zero group IDs.
    if (gr > 0)
    {
#if MODE == MODE_INCLUSIVE
        const T add = higherLevelArray[gr - 1];
#else
        const T add = higherLevelArray[gr];
#endif
        array[g] += (TW)(add);
    }
}
