#include "../cldefs.h"

#ifndef T
#define T int
#endif
#ifndef KEY
#define KEY(element) element
#endif



inline uint getKey(const T element) {
    return KEY(element);
}


__kernel
void clearPartitions(
        __global uint *partitions,
        uint numPartitions)
{
    const uint g = get_global_id(0);

    if (g >= numPartitions)
        return;

    partitions[g] = 0;
}


__kernel
void findPartitionEnds(
        __global T *elements,
        uint numElements,
        __global uint *partitions)
{
    const uint g = get_global_id(0);

    if (g >= numElements)
        return;

    const uint thisPartition = getKey(elements[g]);
    bool isPartitionEnd;
    if (g == numElements - 1) {
        isPartitionEnd = true;
    }
    else {
        const uint nextPartition = getKey(elements[g + 1]);
        isPartitionEnd = (nextPartition != thisPartition);
    }

    // If this item is a partition end (i.e. the next item is in a different partition):
    // Write the partition end (add one, so it's a pointer to one element past the end of the partition)
    if (isPartitionEnd)
        partitions[thisPartition] = g + 1;
}


__kernel
void subtractPartitionStarts(
        __global T *elements,
        uint numElements,
        __global uint *partitions)
{
    const uint g = get_global_id(0);

    if (g >= numElements)
        return;

    const uint thisPartition = getKey(elements[g]);
    bool isPartitionStart;
    if (g == 0) {
        isPartitionStart = true;
    }
    else {
        const uint prevPartition = getKey(elements[g - 1]);
        isPartitionStart = (prevPartition != thisPartition);
    }

    // If this item is a partition start (i.e. the prev item is in a different partition):
    // Subtract the partition start from the previously written partition end
    if (isPartitionStart)
        partitions[thisPartition] -= g;
}
