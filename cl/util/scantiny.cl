#include "../cldefs.h"

#ifndef T
#define T uint
#endif

#ifndef W
#define W 16
#endif

#ifndef TW
#define TW uint16
#endif

#define CONCAT_(A,B) A##B
#define CONCAT(A,B) CONCAT_(A,B)
#define TW CONCAT(T,W)


inline T vecLast(TW v) {
#if W == 2
    return v.s1;
#elif W == 4
    return v.s3;
#elif W == 8
    return v.s7;
#elif W == 16
    return v.sF;
#endif
}

inline TW vecScanInclusive(TW v) {
    return (TW)(v.s0
                ,v.s0+v.s1
            #if W > 2
                ,v.s0+v.s1+v.s2
                ,v.s0+v.s1+v.s2+v.s3
            #if W > 4
                ,v.s0+v.s1+v.s2+v.s3+v.s4
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7
            #if W > 8
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC+v.sD
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC+v.sD+v.sE
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC+v.sD+v.sE+v.sF
            #endif
            #endif
            #endif
    );
}

inline TW vecScanExclusive(TW v) {
    return (TW)(0
                ,v.s0
            #if W > 2
                ,v.s0+v.s1
                ,v.s0+v.s1+v.s2
            #if W > 4
                ,v.s0+v.s1+v.s2+v.s3
                ,v.s0+v.s1+v.s2+v.s3+v.s4
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6
            #if W > 8
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC+v.sD
                ,v.s0+v.s1+v.s2+v.s3+v.s4+v.s5+v.s6+v.s7+v.s8+v.s9+v.sA+v.sB+v.sC+v.sD+v.sE
            #endif
            #endif
            #endif
    );
}

inline T vecW_last(TW v) {
#if W == 2
    return v.s1;
#elif W == 4
    return v.s3;
#elif W == 8
    return v.s7;
#elif W == 16
    return v.sF;
#endif
}


/// Scans a buffer of type "T" and size "numElements" from "in" to "out".
/// "numElements" must be a multiple of "W".
/// "valueOffset" will be added to the result.
/// Supports inplace editing, i.e. "in" can be equal to "out".
__kernel __attribute__((reqd_work_group_size(1, 1, 1)))
void inclusiveScanTiny(
        __global TW *in,
        __global TW *out,
        uint numElements,
        T valueOffset)
{
    numElements /= W;

    for (uint i = 0; i < numElements; ++i) {
        TW vec = vecScanInclusive(in[i]) + (TW)(valueOffset);
        valueOffset = vecW_last(vec);
        out[i] = vec;
    }
}

__kernel __attribute__((reqd_work_group_size(1, 1, 1)))
void exclusiveScanTiny(
        __global TW *in,
        __global TW *out,
        uint numElements,
        T globalOffset)
{
    numElements /= W;

    for (uint i = 0; i < numElements; ++i) {
        TW orig = in[i];
        TW vec = vecScanExclusive(orig) + (TW)(globalOffset);
        globalOffset = vecW_last(vec) + vecW_last(orig);
        out[i] = vec;
    }
}
