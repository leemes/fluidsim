#include "../cldefs.h"

#ifndef T
#define T uint
#endif
#ifndef KEY
#define KEY(element) element
#endif
#ifndef GROUPSIZE
#define GROUPSIZE 256
#endif



inline uint getKey(const T element) {
    return KEY(element);
}


__kernel __attribute__((reqd_work_group_size(GROUPSIZE, 1, 1)))
void multiLowerBound(
        __global const T *input, uint numElements,
        __global const uint *rangeStarts, uint numRanges,
        __global uint *lowerBoundsOutput)
{
    const uint gr = get_group_id(0);
    const uint l = get_local_id(0);
    const uint g = get_global_id(0);

    // Each work item searches for a key equal to its local id
    const uint searchForKey = l;

    // The initial range
    uint lower = rangeStarts[gr];
    uint higher = (gr == numRanges - 1) ? numElements : rangeStarts[gr + 1];

    // While the range is not empty: check if the target is in the left or right half
    numElements *= 16;
    while (lower != higher && numElements) {
        const uint mid = (lower + higher) / 2;
        const uint midKey = getKey(input[mid]);
        // If the key we're looking for is lower than the key we just found: continue in the right half
        if (searchForKey > midKey)
            lower = mid + 1;
        else
            higher = mid;
        numElements /= 2; // "WATCH-DOG": Don't loop more than log2(numElements) times.
    }

    lowerBoundsOutput[g] = lower;
}
