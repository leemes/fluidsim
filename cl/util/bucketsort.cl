#include "../cldefs.h"

#ifndef T_IN
#define T_IN int
#endif
#ifndef T_OUT
#define T_OUT int
#endif
#ifndef KEY
#define KEY(element) element
#endif
#ifndef OUTPUT
#define OUTPUT(element,index) element
#endif
#ifndef NUM_BUCKETS
#define NUM_BUCKETS 64
#endif

// Local work group size for initial and final step. Has to be greater or equal NUM_BUCKETS.
// Recommended group size is NUM_BUCKETS, because part of the kernel only utilizes that many work items.
#ifndef GROUPSIZE
#define GROUPSIZE NUM_BUCKETS
#endif


// For up to 256 threads per group we can use uchar for the locally scanned counts.
// It *might* happen in rare cases that all 256 elements go in the same bucket,
// which is the "worst case", but still: all numbers are <256 (the last one is 255 max).
#if GROUPSIZE <= UCHAR_MAX+1
typedef uchar loff_t;
#else
typedef ushort loff_t;
#endif


inline uint getKey(const T_IN element) {
    return KEY(element);
}
inline T_OUT getOutput(const T_IN element, const uint index) {
    return OUTPUT(element, index);
}


__kernel __attribute__((reqd_work_group_size(GROUPSIZE, 1, 1)))
void initialStep(
        const __global T_IN *in,
        __global uint *intermediate,
        const uint numElements,
        uint intermediatePitch)
{
    const uint g = get_global_id(0);
    const uint l = get_local_id(0);
    const uint gr = get_group_id(0);

    // The localKeys block stores the keys of the input processed by this work group
    __local uint localKeys[GROUPSIZE];
    localKeys[l] = g < numElements ? getKey(in[g]) : UINT_MAX;

    // Sync threads
    barrier(CLK_LOCAL_MEM_FENCE);

    // Compute NUM_BUCKETS sums of implicit predicates `i == key`.
    // For this, only NUM_BUCKETS threads are utilized. (GROUPSIZE >= NUM_BUCKETS has to be true)
    // FIXME: Can we utilize all threads to speed this up?
    if (l < NUM_BUCKETS) {
        uint sum = 0;
        #pragma unroll
        for (uint i = 0; i < GROUPSIZE; ++i) {
            sum += (localKeys[i] == l);
        }
        intermediate[intermediatePitch * l + gr] = sum;
    }
}



//-----------------------------------------------------------------------------

// Aviod bank conflicts
#if NUM_BUCKETS > 32
#define OFFSET(A) ((A) + (A) / NUM_BUCKETS)
#else
#define OFFSET(A) ((A) + (A) / 32)
#endif

__kernel __attribute__((reqd_work_group_size(GROUPSIZE, 1, 1)))
void finalStep(
        const __global T_IN *in,
        __global T_OUT *out,
        __global uint *intermediateTransposed,
        const uint numElements)
{
    const uint g = get_global_id(0);
    const uint l = get_local_id(0);
    const uint gr = get_group_id(0);

    __local uint   localStarts[OFFSET(NUM_BUCKETS)];
    __local uint   localKeys[OFFSET(GROUPSIZE)];
    __local loff_t localOffsets[OFFSET(NUM_BUCKETS * GROUPSIZE)];

    // Load local starts
    if (l < NUM_BUCKETS) {
        localStarts[OFFSET(l)] = intermediateTransposed[l + NUM_BUCKETS * gr];
    }
    // Init local offsets with the predicates
    const uchar myKey = g < numElements ? getKey(in[g]) : UINT_MAX;
    localKeys[OFFSET(l)] = myKey;

    // Sync threads
    barrier(CLK_LOCAL_MEM_FENCE);

    // Scan implicit predicates (NUM_BUCKETS sequential scans):
    if (l < NUM_BUCKETS) {
        uint sum = 0;
        #pragma unroll
        for (uint i = 0; i < GROUPSIZE; ++i) {
            bool pred = localKeys[OFFSET(i)] == l;
            if (pred)
                localOffsets[OFFSET(l * GROUPSIZE + i)] = sum;
            sum += pred;
        }
    }


    // Sync threads
    barrier(CLK_LOCAL_MEM_FENCE);

    // Write to target index
    if (g < numElements) {
        const uint targetIndex = localStarts[OFFSET(myKey)]
                              + localOffsets[OFFSET(myKey * GROUPSIZE + l)];
        out[targetIndex] = getOutput(in[g], g);
    }
}
