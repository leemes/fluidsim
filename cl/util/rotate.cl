#include "../cldefs.h"

#define MODE_ROTATE_CW  0
#define MODE_ROTATE_CCW 1
#define MODE_TRANSPOSE  2

#ifndef GROUPSIZE_X
#define GROUPSIZE_X 16
#endif
#ifndef GROUPSIZE_Y
#define GROUPSIZE_Y 16
#endif
#ifndef T
#define T uint
#endif
#ifndef MODE
#define MODE MODE_ROTATE_CW
#endif


#define OFFSET(A) ((A) + (A) / 32)


inline uint2 transpose(uint2 coord) {
    return (uint2)(coord.y, coord.x);
}

__kernel __attribute__((reqd_work_group_size(GROUPSIZE_X, GROUPSIZE_Y, 1)))
void rotateMemory(
        __global const T *in,
        __global T *out,
        uint2 size)
{
    const uint2 g  = {get_global_id (0), get_global_id (1)};
    const uint2 l  = {get_local_id  (0), get_local_id  (1)};
    const uint2 ls = {GROUPSIZE_X      , GROUPSIZE_Y      };

    const uint2 gOff = g - l;              // group offset (within the global NDRange)
    const uint2 sizeT = transpose(size);   // transposed (target) matrix size

    __local T localBlock[OFFSET(GROUPSIZE_X * GROUPSIZE_Y)];

    // Copy block into local buffer (unmodified)
    if (all(g < size)) {
        const uint index = l.y * ls.x + l.x;
        localBlock[OFFSET(index)] = in[g.y * size.x + g.x];
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    // First transpose the local buffer, then mirror when writing in MR.
    // Coalescing still works, since we write "horizontally" to adjacent addresses.
    // The code is a bit simpler, since we only have to consider the direction once
    // (when mirroring), but it's independent when dealing with the "tiles".

    // "Re-wrap" the local index within the block, so it's wrapped after LS.y pixels instead of LS.x:
    const uint index = l.y * ls.x + l.x;
    const uint2 rewrapped = (uint2)(index % ls.y, index / ls.y);

    // Separate (internal) transposition of a block and re-arranging the blocks in the whole array.
    const uint2 targetBlockPos = transpose(gOff);               // These two lines effectively transpose the matrix.
    const uint2 sourcePosWithinBlock = transpose(rewrapped);    // These two lines effectively transpose the matrix.
    const uint2 targetPosWithinBlock = rewrapped;
    int2 targetPos = convert_int2(targetBlockPos + targetPosWithinBlock);

    // Mirror one axis, depending on the direction of the rotation (for transposition, don't do anything, as we already transposed the coordinates)
#if MODE == MODE_ROTATE_CW
    targetPos.x = sizeT.x - targetPos.x - 1; // mirror X
#elif MODE == MODE_ROTATE_CCW
    targetPos.y = sizeT.y - targetPos.y - 1; // mirror Y
#endif

    // Write the results
    if (all(targetPos < sizeT) && all(targetPos >= 0)) {
        const uint index = sourcePosWithinBlock.y * ls.x + sourcePosWithinBlock.x;
        out[targetPos.y * sizeT.x + targetPos.x] = localBlock[OFFSET(index)];
    }
}

