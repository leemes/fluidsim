#include "../cldefs.h"

#ifndef T_IN
#define T_IN int
#endif
#ifndef T_OUT
#define T_OUT int
#endif
#ifndef OUTPUT
#define OUTPUT(input, index, numElements) input[index]
#endif
#ifndef INPUT_STRIDE
#define INPUT_STRIDE 1
#endif

#ifndef GROUPSIZE
#define GROUPSIZE 64
#endif



inline T_OUT getOutput(__global const T_IN *input, const uint index, const uint numElements)
{
    return OUTPUT(input, index, numElements);
}


__kernel __attribute__((reqd_work_group_size(GROUPSIZE, 1, 1)))
void transform(
        __global const T_IN *in,
        __global T_OUT *out,
        uint numElements)
{
    const uint g = get_global_id(0);

    if (g >= numElements)
        return;

    out[g] = getOutput(in, g * INPUT_STRIDE, numElements);
}
