#include "../cldefs.h"

#ifndef T
#define T int
#endif
#ifndef OUTPUT
#define OUTPUT(element,index) -1
#endif



inline T getOutput(__global const T *input, const uint index) {
#define element     (input[index])
    return OUTPUT(element, index);
#undef element
}


__kernel
void fill(
        __global T *array,
        uint numElements)
{
    const uint g = get_global_id(0);

    if (g >= numElements)
        return;

    array[g] = getOutput(array, g);
}
