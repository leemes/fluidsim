#include "cldefs.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// KERNEL COMPILATION CONSTANTS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Number of grid cells per dimension (max: 256)
#ifndef GRID_SIZE
#define GRID_SIZE 64
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This number is written in the gridCells buffer for any empty cell in the scene.
// We stop the computation once we find such a value in it.
#define GRID_CELL_EMPTY    UINT_MAX

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

uchar3 mortonDecode(uint mortonCode)
{
    uchar x = (((mortonCode      ) & 1)     )
            | (((mortonCode >>  3) & 1) << 1)
            | (((mortonCode >>  6) & 1) << 2)
            | (((mortonCode >>  9) & 1) << 3)
            | (((mortonCode >> 12) & 1) << 4)
            | (((mortonCode >> 15) & 1) << 5)
            | (((mortonCode >> 18) & 1) << 6)
            | (((mortonCode >> 21) & 1) << 7);

    uchar y = (((mortonCode >>  1) & 1)     )
            | (((mortonCode >>  4) & 1) << 1)
            | (((mortonCode >>  7) & 1) << 2)
            | (((mortonCode >> 10) & 1) << 3)
            | (((mortonCode >> 13) & 1) << 4)
            | (((mortonCode >> 16) & 1) << 5)
            | (((mortonCode >> 19) & 1) << 6)
            | (((mortonCode >> 22) & 1) << 7);

    uchar z = (((mortonCode >>  2) & 1)     )
            | (((mortonCode >>  5) & 1) << 1)
            | (((mortonCode >>  8) & 1) << 2)
            | (((mortonCode >> 11) & 1) << 3)
            | (((mortonCode >> 14) & 1) << 4)
            | (((mortonCode >> 17) & 1) << 5)
            | (((mortonCode >> 20) & 1) << 6)
            | (((mortonCode >> 23) & 1) << 7);

    return (uchar3)(x, y, z);
}

uint mortonEncode(uchar3 cellCoord)
{
    return ((uint)(cellCoord.x     ) & 1)
        | (((uint)(cellCoord.y     ) & 1) << 1)
        | (((uint)(cellCoord.z     ) & 1) << 2)

        | (((uint)(cellCoord.x >> 1) & 1) << 3)
        | (((uint)(cellCoord.y >> 1) & 1) << 4)
        | (((uint)(cellCoord.z >> 1) & 1) << 5)

        | (((uint)(cellCoord.x >> 2) & 1) << 6)
        | (((uint)(cellCoord.y >> 2) & 1) << 7)
        | (((uint)(cellCoord.z >> 2) & 1) << 8)

        | (((uint)(cellCoord.x >> 3) & 1) << 9)
        | (((uint)(cellCoord.y >> 3) & 1) << 10)
        | (((uint)(cellCoord.z >> 3) & 1) << 11)

        | (((uint)(cellCoord.x >> 4) & 1) << 12)
        | (((uint)(cellCoord.y >> 4) & 1) << 13)
        | (((uint)(cellCoord.z >> 4) & 1) << 14)

        | (((uint)(cellCoord.x >> 5) & 1) << 15)
        | (((uint)(cellCoord.y >> 5) & 1) << 16)
        | (((uint)(cellCoord.z >> 5) & 1) << 17)

        | (((uint)(cellCoord.x >> 6) & 1) << 18)
        | (((uint)(cellCoord.y >> 6) & 1) << 19)
        | (((uint)(cellCoord.z >> 6) & 1) << 20)

        | (((uint)(cellCoord.x >> 7) & 1) << 21)
        | (((uint)(cellCoord.y >> 7) & 1) << 22)
        | (((uint)(cellCoord.z >> 7) & 1) << 23);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lookupAdjacentCellsUsing27WorkItems(uint cellIndex, __global uint *gridCellStarts,
                                         __local uint adjacentCellStarts[27],
                                         __local uint adjacentCellCounts[27])
{
    const uint l = get_local_id(0);

    const int3 cellCoord = convert_int3(mortonDecode(cellIndex));

    if (l < 27) {
        const int dx = (l % 3) - 1;
        const int dy = ((l / 3) % 3) - 1;
        const int dz = ((l / 9) % 3) - 1;
        int3 adjacentCellCoord = cellCoord + (int3)(dx, dy, dz);
        if (all(adjacentCellCoord >= 0) && all(adjacentCellCoord < GRID_SIZE)) {
            const uint adjacentCellIndex = mortonEncode(convert_uchar3(adjacentCellCoord));
            const uint thisStart = adjacentCellStarts[l] = gridCellStarts[adjacentCellIndex];
            adjacentCellCounts[l] = gridCellStarts[adjacentCellIndex + 1] - thisStart;
        } else {
            adjacentCellStarts[l] = UINT_MAX;
            adjacentCellCounts[l] = 0;
        }
    }
}
