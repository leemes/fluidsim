#ifndef DEBUG_H
#define DEBUG_H

#include <qclbuffer.h>
#include <iostream>
#include <iomanip>


template<typename T>
inline T forceNumber(T value) {
    return value;
}
inline signed int forceNumber(signed char value) {
    return value;
}
inline unsigned int forceNumber(unsigned char value) {
    return value;
}


template <typename T, int elementsPerLine = 32, int printWidth = 4>
void debug(const T *data, int count, std::ostream & out = std::cout)
{
    if (!data) {
        out << "Null-Pointer" << std::endl;
    } else {
        for(int i = 0; i < count; ++i) {
            if (i % elementsPerLine == 0 && i)
                out << std::endl;
            if (i % elementsPerLine == 0)
                out << "[" << std::setw(6) << i << "] ";
            static_assert(sizeof(decltype(forceNumber(data[i]))) > 1, "I won't print single bytes!");
            out << std::setw(printWidth) << forceNumber(data[i]);
        }
        out << std::endl;
    }
}

template <typename T, int elementsPerLine = 32, int printWidth = 4>
void debug(QCLBuffer &buffer, int count = -1, std::ostream & out = std::cout)
{
    if (buffer.isNull()) {
        out << "Null QCLBuffer" << std::endl;
    } else {
        const void *mapped = buffer.map(QCLMemoryObject::ReadOnly);
        const T *data = reinterpret_cast<const T*>(mapped);
        if (count == -1)
            count = buffer.size() / sizeof(T);
        debug<T,elementsPerLine,printWidth>(data, count);
        buffer.unmap(const_cast<void*>(mapped));
    }
}

#endif // DEBUG_H
