#include "cleventtree.h"
#include <iomanip>
#include <QModelIndex>

static const int indentation = 2;

CLEventTree::CLEventTree(const QString &name) :
    m_name(name)
{
}

CLEventTree::CLEventTree(const QCLEvent &singleEvent, const QString &name) :
    m_event(singleEvent),
    m_name(name)
{
}

CLEventTree::CLEventTree(const CLEventTree &singleEvent, const QString &name) :
    m_name(name),
    m_children({singleEvent})
{
}

CLEventTree & CLEventTree::operator <<(const CLEventTree &subTree)
{
    m_children.push_back(subTree);
    return *this;
}

double CLEventTree::milliSeconds() const
{
    double sum = 0.0;
    // This node
    if (!m_event.isNull())
        sum += (double)(m_event.finishTime() - m_event.runTime()) / 1e6;
    // Children
    for (const CLEventTree & child : m_children)
        sum += child.milliSeconds();
    return sum;
}

void CLEventTree::print(std::ostream &stream, int maxDepth, int recursion, int treeWidth, double total) const
{
    // Initialize (will not be executed in nested calls of the recursion)
    if (recursion == 0) {
        stream << "Profiling results for \"" << m_name.toStdString() << "\":" << std::endl;
        treeWidth = this->treeWidth();
        total = this->milliSeconds();
    }

    // indent
    for (int i = 0; i < recursion*indentation; ++i)
        stream << ' ';

    // name
    stream << "+ ";
    stream << m_name.toStdString();

    // time
    double time = milliSeconds();
    for (int i = 0; i < treeWidth - 2 - recursion*indentation - m_name.length(); ++i)
        stream << ' ';
    stream << std::setw(10 + qMax(0,(int)log10(total))) << std::setprecision(5) << std::fixed << time << " ms";

    // percent
    double relative = time / total;
    stream << std::setw(8) << std::setprecision(1) << std::fixed << relative * 100.0 << " %";

    // bar
    const uint barWidth = 50;
    stream << "  ";
    int filledChars = std::round(relative * barWidth);
    for (int i = 0; i < filledChars; ++i)
        stream << '#';

    // line break
    stream << std::endl;

    // children
    if (maxDepth)
        for (const CLEventTree & child : m_children)
            child.print(stream, maxDepth - 1, recursion + 1, treeWidth, total);
}

void CLEventTree::addToModel(QAbstractItemModel *model, const QModelIndex &parent, double total) const
{
    // Initialize (will not be executed in nested calls of the recursion)
    if (total < 0) {
        total = this->milliSeconds();
    }

    if (model->columnCount(parent) == 0) {
        model->insertColumns(0, 3, parent);
    }
    Q_ASSERT(model->columnCount(parent) == 3);

    int row = model->rowCount(parent);
    bool ok = model->insertRow(model->rowCount(parent), parent);
    if (ok) {
        double time = milliSeconds();
        double relative = time / total;

        // set model data
        QModelIndex nameIndex = model->index(row, 0, parent);
        model->setData(nameIndex, m_name, Qt::DisplayRole);

        QModelIndex timeIndex = model->index(row, 1, parent);
        model->setData(timeIndex, QString::number(time, 'f') + " ms", Qt::DisplayRole);
        model->setData(timeIndex, Qt::AlignRight, Qt::TextAlignmentRole);

        QModelIndex percIndex = model->index(row, 2, parent);
        model->setData(percIndex, QString::number(relative * 100.0, 'f', 1) + " %", Qt::DisplayRole);
        model->setData(percIndex, Qt::AlignRight, Qt::TextAlignmentRole);

        // children
        for (const CLEventTree & child : m_children)
            child.addToModel(model, nameIndex, total);
    }
}

int CLEventTree::treeWidth(int recursion) const
{
    int width = recursion*indentation + 2 + m_name.length();
    for (const CLEventTree & child : m_children)
        width = qMax(width, child.treeWidth(recursion + 1));
    return width;
}

std::ostream &operator <<(std::ostream &stream, const CLEventTree &tree)
{
    tree.print(stream);
    return stream;
}
