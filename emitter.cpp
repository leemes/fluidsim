#include "emitter.h"

/*
void Simulation::emitParticles()
{
    QVector<ParticleBasic> addParticles;
    for (const Emitter & emitter : m_emitters) {
        if (m_iteration % emitter.interval == 0) {
            for (uint k = 0; k < emitter.rate; ++k) {
                float v[10];
                for (uint i = 0; i < 10; ++i)
                    v[i] = ((float)rand() / RAND_MAX * 2 - 1);
                QVector3D position    = emitter.pos      + QVector3D{v[0], v[1], v[2]} * emitter.posVariance;
                QVector3D velocity    = emitter.velocity + QVector3D{v[3], v[4], v[5]} * emitter.velocityVariance;
                QVector3D color       = emitter.color    + QVector3D{v[6], v[7], v[8]} * emitter.colorVariance;
                float     temperature = emitter.temp     + v[9] * emitter.tempVariance;

                ParticleBasic p;
                position += QVector3D(SCENE_CENTER_FLOAT, SCENE_CENTER_FLOAT, SCENE_CENTER_FLOAT);
                position *= INTEGER_FACTOR;
                p.posX = position.x(); p.posY = position.y(); p.posZ = position.z();
                p.velX = velocity.x(); p.velY = velocity.y(); p.velZ = velocity.z();
                p.temperature = temperature;
                p.id = m_numParticles;

                addParticles << p;
            }
        }
    }

    if (addParticles.isEmpty())
        return;

    uint numAddParticles = addParticles.count();
    uint newNumParticles = m_numParticles + numAddParticles;
    if (newNumParticles > m_allocParticles) {
        qWarning("Particle buffer full. I will not add any more particles.");
        return;
        // Reallocate other buffer, swap buffers
        //m_allocParticles *= 2;
        //m_particlesBuffer[otherBuffer()] = m_context.createVector<Particle>(m_allocParticles);
        // TODO: COPY
        //swapBuffers();
        //m_particlesBuffer[otherBuffer()] = m_context.createVector<Particle>(m_allocParticles);
    }

    QCLBuffer addParticlesBuffer = m_context.createBufferCopy(
                &addParticles.first(), sizeof(ParticleBasic) * numAddParticles, QCLBuffer::ReadOnly);

    m_kernelAddParticles.setRoundedGlobalWorkSize(numAddParticles);
    m_kernelAddParticles(m_particleBasicBuffer[m_currentBuffer], m_numParticles,
                         addParticlesBuffer, numAddParticles);

    m_numParticles = newNumParticles;
}
*/


int Emitter::emitCount(float dt)
{
    m_keptBack += m_rate * dt;
    int e = m_keptBack;
    m_keptBack -= e;
    return e;
}
