import QtQuick 2.0
import "../components"
import "../template"
import ".."
import "."

Group {
    default property alias content: content.content

    Do { text: "<b>else</b>:" }
    Scope { id: content }
}
