import QtQuick 2.0
import "../components"
import "../template"
import ".."
import "."

Item {
    id: scope
    property real fontSize: (parent && parent.fontSize > 0) ? parent.fontSize : 10
    default property alias content: content.children

    width: parent.width
    height: decoration.height // decoration is slightly higher than the content

    Item {
        id: decoration
        anchors.left: decoration.right
        width: fontSize * 1.6
        height: content.implicitHeight + .3 * parent.fontSize
        property real lineWidth: .1 * parent.fontSize
        property string lineColor: "gray"

        Rectangle {
            id: verticalLine
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.topMargin: -.2 * parent.width
            anchors.leftMargin: .4 * parent.width
            width: parent.lineWidth
            color: parent.lineColor
        }

        Rectangle {
            id: horizontalLine
            anchors.bottom: parent.bottom
            anchors.left: verticalLine.right
            anchors.right: parent.right
            anchors.rightMargin: .1 * parent.width
            height: parent.lineWidth
            color: parent.lineColor
        }
    }

    Algorithm {
        id: content
        anchors.left: decoration.right
        anchors.right: scope.right
    }
}
