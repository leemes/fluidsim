import QtQuick 2.0
import "../components"
import "../template"
import ".."
import "."

Group {
    property string text
    default property alias content: content.content

    Do { text: "<b>for each</b> " + parent.text + ":" }
    Scope { id: content }
}
