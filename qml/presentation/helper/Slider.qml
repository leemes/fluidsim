import QtQuick 2.0

Rectangle {
    id: slider

    width: 200
    height: 20
    radius: height / 2
    color: "white"
    border { width: radius / 6; color: "gray" }

    property real minimum: 0
    property real maximum: 100
    readonly property real value: minimum + (maximum - minimum) * (handle.x / (slider.width - handle.width))

    Item {
        id: handle

        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: height

        Drag.active: dragArea.drag.active
        Drag.hotSpot.x: width / 2
        Drag.hotSpot.y: height / 2

        Rectangle {
            anchors.centerIn: parent
            width: parent.height * .6
            height: width
            radius: height / 2
            color: "gray"
        }

        MouseArea {
            id: dragArea
            anchors.fill: parent

            drag.target: handle
            drag.minimumX: 0
            drag.maximumX: slider.width - handle.width

            hoverEnabled: true
            onEntered: mouseNavigationDisabled++
            onExited: mouseNavigationDisabled--
        }
    }
}
