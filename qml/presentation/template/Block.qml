import QtQuick 2.0
import "../components" as Original
import "../components"
import "."

Column {
    id: block

    property real fontSize: (parent && parent.fontSize > 0) ? parent.fontSize : 10

    property alias title: title.children
    default property alias content: body.children

    property color titleBackgroundColor: Qt.rgba(0,.59,.51,1)
    property color titleTextColor: "white"
    property color bodyBackgroundColor: Qt.rgba(.85,.93,.93,1)
    property color bodyTextColor: "black"

    property int horizontalMargins: fontSize * 0.7
    property int verticalMargins: fontSize * 0.3
    property int innerMargins: fontSize * 0.15  // between title text and gradient == between gradient and body text
    property int gradientHeight: fontSize * 0.25

    property real radius: fontSize * 0.5

    width: parent.width

    Item {
        property real fontSize: block.fontSize;

        width: parent.width
        height: title.height + verticalMargins + innerMargins

        Rectangle {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.margins: .5
            radius: block.radius
            width: radius * 2
            height: radius * 2
            color: titleBackgroundColor
        }
        Rectangle {
            anchors.fill: parent
            anchors.rightMargin: block.radius
            color: titleBackgroundColor
        }
        Rectangle {
            anchors.fill: parent
            anchors.topMargin: block.radius
            color: titleBackgroundColor
        }

        Group {
            id: title
            width: parent.width - 2 * horizontalMargins
            x: horizontalMargins
            y: verticalMargins
            property color color: titleTextColor
        }
    }

    Rectangle {
        width: parent.width
        height: gradientHeight

        gradient: Gradient {
            GradientStop { position: 0; color: titleBackgroundColor }
            GradientStop { position: 1; color: bodyBackgroundColor }
        }
    }

    Item {
        property real fontSize: block.fontSize;

        width: parent.width
        height: body.height + verticalMargins + innerMargins

        Rectangle {
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.margins: .5
            radius: block.radius
            width: radius * 2
            height: radius * 2
            color: bodyBackgroundColor
        }
        Rectangle {
            anchors.fill: parent
            anchors.leftMargin: block.radius
            color: bodyBackgroundColor
        }
        Rectangle {
            anchors.fill: parent
            anchors.bottomMargin: block.radius
            color: bodyBackgroundColor
        }

        Group {
            id: body
            width: parent.width - 2 * horizontalMargins
            x: horizontalMargins
            y: innerMargins
            property color color: bodyTextColor
        }
    }

    Group { id: content }
}
