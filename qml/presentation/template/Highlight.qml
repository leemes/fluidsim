import QtQuick 2.0

Item {
    width: parent.width
    height: content.height
    property real fontSize: (parent && parent.fontSize > 0) ? parent.fontSize : 10

    default property alias content: content.children

    property real animationDuration: 1000
    property real highlight: 0

    Rectangle {
        property real gray: .9 - .2 * highlight
        anchors.fill: parent
        anchors.margins: -radius
        radius: fontSize / 3
        color: Qt.rgba(gray, gray, gray, 1)
        border { width: 3; color: "gray" }
    }

    Item {
        id: content
        width: parent.width
        height: childrenRect.height
        property real fontSize: parent.fontSize
    }

    SequentialAnimation on highlight {
        id: animation
        running: false
        PauseAnimation { duration: 100 } // otherwise the animation might stutter
        NumberAnimation { from: 0; to: 1; duration: animationDuration * .3; easing.type: Easing.OutQuad }
        NumberAnimation { from: 1; to: 0; duration: animationDuration * .7; easing.type: Easing.InOutQuad }
    }

    onVisibleChanged: if (visible) animation.start()
}
