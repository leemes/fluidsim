import QtQuick 2.0
import "../components" as Original

Original.Presentation {

    property alias banner: titleSlide.banner

    TitleSlide {
        id: titleSlide
        title: presentation.title
    }
}
