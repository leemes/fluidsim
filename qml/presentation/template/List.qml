import QtQuick 2.0
import "../components"

AbstractList {
    id: list

    property real indentation: 2.5
    property real bulletSeparation: 1

    spacing: fontSize * (parent.isSlideContainer ? 1.5 : .5)

    delegate: Item {
        implicitHeight: content.implicitHeight

        function setContent(item) {
            item.parent = content;
        }

        Item {
            id: content

            property real fontSize: list.fontSize
            property variant slide: list.slide

            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: list.indentation * fontSize

            implicitHeight: children[0].implicitHeight
        }

        Rectangle {
            id: bulletPoint

            anchors.right: content.left
            anchors.top: content.top
            anchors.rightMargin: list.bulletSeparation * fontSize
            anchors.topMargin: fontSize * .5

            color: Qt.rgba(0,.59,.51,1)

            width: fontSize * .7
            height: fontSize * .7

            radius: fontSize * .2
        }
    }
}
