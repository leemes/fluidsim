import QtQuick 2.0
import "../components" as Original

Original.Slide {
    id: slide

    // Placeholder for the content
    default property alias content: content.children
    property alias spacing: content.spacing

    // Predefined auto content (without the need to add child items):
    property string centeredText

    // This theme:
    property string fontFamily: "Deja Vu Sans"
    property real titleFontSize: fontSize * 1.3
    property real titleContentSeparation: fontSize * 3.0
    property real footerFontSize: fontSize * 0.55
    property real pageHorizontalMargins: fontSize * 1.7
    property real pageVerticalMargins: fontSize * 1.7

    Rectangle {
        color: "#d4d4d4"
        anchors.fill: parent

        // White area with rounded corners
        Rectangle {
            anchors.fill: parent
            anchors.margins: Math.round(slide.fontSize * .8)
            anchors.bottomMargin: Math.round(slide.fontSize * 2.25)
            color: "white"

            property real kitCornerRadius: Math.round(slide.fontSize * .8)

            // Logo
            Image {
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.rightMargin: slide.fontSize * 2
                anchors.topMargin: slide.fontSize * 1.3
                source: "logo.png"
                width: slide.fontSize * 7
                height: width / 2
                sourceSize.width: width * presentation.scaleFactor      // for proper smooth scaling
                sourceSize.height: height * presentation.scaleFactor    // for proper smooth scaling
            }

            Text {
                id: titleText

                anchors.top: parent.top
                anchors.left: parent.left

                anchors.topMargin: slide.pageVerticalMargins
                anchors.leftMargin: slide.pageHorizontalMargins
                anchors.rightMargin: slide.pageHorizontalMargins

                font.pointSize: slide.titleFontSize
                color: "black"
                text: title
                font.bold: true
                font.family: slide.fontFamily
            }

            Column {
                id: content
                property bool isSlideContainer: true

                anchors.top: titleText.bottom
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right

                anchors.topMargin: slide.titleContentSeparation
                anchors.bottomMargin: slide.pageVerticalMargins
                anchors.leftMargin: slide.pageHorizontalMargins
                anchors.rightMargin: slide.pageHorizontalMargins

                spacing: fontSize * 2

                property real fontSize: slide.fontSize
                property variant slide: slide
            }

            // Predefined auto content (see properties):
            Text {
                font.pointSize: slide.fontSize
                text: slide.centeredText
                anchors.centerIn: parent
                horizontalAlignment: Text.AlignHCenter
                font.family: slide.fontFamily
            }

            // Overpaint with rounded corners
            Rectangle { // bottom left
                property real overpaintRadius: parent.kitCornerRadius
                anchors.fill: parent
                anchors.margins: -overpaintRadius
                anchors.topMargin: -overpaintRadius * 2
                anchors.rightMargin: -overpaintRadius * 2
                radius: overpaintRadius * 2
                color: "transparent"
                border { color: parent.parent.color; width: overpaintRadius }
            }
            Rectangle { // top right
                property real overpaintRadius: parent.kitCornerRadius
                anchors.fill: parent
                anchors.margins: -overpaintRadius
                anchors.bottomMargin: -overpaintRadius * 2
                anchors.leftMargin: -overpaintRadius * 2
                radius: overpaintRadius * 2
                color: "transparent"
                border { color: parent.parent.color; width: overpaintRadius }
            }
        }

        Text {
            id: slideNumText

            anchors.bottom: parent.bottom
            anchors.right: parent.right

            anchors.rightMargin: slide.fontSize * .8
            anchors.bottomMargin: slide.fontSize * .6

            font.pointSize: slide.footerFontSize
            visible: slide.slideNumbered
            text: '<b>' + slide.slideNumber + '</b> / ' + slide.presentation.numberedSlidesCount
            font.family: slide.fontFamily
        }

        Text {
            id: slideFooterText

            anchors.bottom: parent.bottom
            anchors.left: parent.left

            anchors.leftMargin: slide.fontSize * .8
            anchors.bottomMargin: slide.fontSize * .6

            font.pointSize: slide.footerFontSize
            visible: slide.slideNumbered
            text: presentation.title.replace(/\s+/g, ' ') +
                  " ‒ " + presentation.subTitle.replace(/\s+/, ' ') +
                  " ‒ " + presentation.name
            font.family: slide.fontFamily
        }
    }
}
