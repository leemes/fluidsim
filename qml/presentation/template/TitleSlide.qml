import QtQuick 2.0
import "../components" as Original

Original.Slide {
    id: slide

    property alias banner: banner.children

    slideNumbered: false

    // This theme:
    property string fontFamily: "Deja Vu Sans"
    property real pageHorizontalMargins: 40
    property real pageVerticalMargins: 20
    property real footerFontSize: fontSize * 0.55

    Rectangle {
        color: "#d4d4d4"
        anchors.fill: parent

        // White area with rounded corners
        Rectangle {
            anchors.fill: parent
            anchors.margins: Math.round(slide.fontSize * .8)
            anchors.bottomMargin: Math.round(slide.fontSize * 2.25)
            color: "white"
            property real kitCornerRadius: Math.round(slide.fontSize * .8)

            // KIT Logo
            Image {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: slide.fontSize * 2
                anchors.topMargin: slide.fontSize * 1.3
                source: "logo.png"
                width: slide.fontSize * 11
                height: width / 2
                sourceSize.width: width * presentation.scaleFactor      // for proper smooth scaling
                sourceSize.height: height * presentation.scaleFactor    // for proper smooth scaling
            }

            /*
            // Institute Logo
            Image {
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.rightMargin: slide.fontSize * 1.5
                anchors.topMargin: slide.fontSize * 1.5
                source: "institutelogo.png"
                width: slide.fontSize * 5
                height: width
                sourceSize.width: width * presentation.scaleFactor      // for proper smooth scaling
                sourceSize.height: height * presentation.scaleFactor    // for proper smooth scaling
            }
            */

            // Title text
            Column {
                anchors.bottom: grayBar.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: slide.fontSize * 2
                anchors.bottomMargin: slide.fontSize

                spacing: slide.fontSize * .75

                Text {
                    font.pointSize: slide.fontSize * 1.5
                    font.family: slide.fontFamily
                    text: presentation.title
                    font.bold: true
                }
                Text {
                    visible: text !== ""
                    font.pointSize: slide.fontSize
                    font.family: slide.fontFamily
                    text: presentation.subTitle
                }
                Text {
                    visible: text !== ""
                    font.pointSize: slide.fontSize * .75
                    font.family: slide.fontFamily
                    text: presentation.name + " ‒ " + Qt.formatDate(new Date(), 'MMM dd, yyyy')
                }
            }

            // Banner, can assigned e.g. an Image item
            Item {
                id: banner

                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                height: width * 0.3175
                clip: true

                // [... children of the TitleSlide go here ...]
            }

            // Gray bar
            Rectangle {
                id: grayBar

                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: banner.top

                height: slide.fontSize * 3

                color: "#707070"

                Text {
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.margins: slide.fontSize * 2
                    font.pointSize: slide.fontSize * .75
                    font.family: slide.fontFamily
                    color: "white"
                    text: presentation.institute
                }
            }

            // Overpaint with rounded corners
            Rectangle { // bottom left
                property real overpaintRadius: parent.kitCornerRadius
                anchors.fill: parent
                anchors.margins: -overpaintRadius
                anchors.topMargin: -overpaintRadius * 2
                anchors.rightMargin: -overpaintRadius * 2
                radius: overpaintRadius * 2
                color: "transparent"
                border { color: parent.parent.color; width: overpaintRadius }
            }
            Rectangle { // top right
                property real overpaintRadius: parent.kitCornerRadius
                anchors.fill: parent
                anchors.margins: -overpaintRadius
                anchors.bottomMargin: -overpaintRadius * 2
                anchors.leftMargin: -overpaintRadius * 2
                radius: overpaintRadius * 2
                color: "transparent"
                border { color: parent.parent.color; width: overpaintRadius }
            }
        }

        // Footer
        Text {
            id: slideFooterText

            anchors.bottom: parent.bottom
            anchors.left: parent.left

            anchors.leftMargin: slide.fontSize * .8
            anchors.bottomMargin: slide.fontSize * .6

            font.pointSize: slide.footerFontSize
            text: "KIT – University of the State of Baden-Wuerttemberg and National Laboratory of the Helmholtz Association"
            font.family: slide.fontFamily
        }
        Text {
            id: slideFooterUrl

            anchors.bottom: parent.bottom
            anchors.right: parent.right

            anchors.rightMargin: slide.fontSize * .8
            anchors.bottomMargin: slide.fontSize * .6

            font.pointSize: slide.footerFontSize
            text: "www.kit.edu"
            font.family: slide.fontFamily
            font.bold: true
        }
    }
}
