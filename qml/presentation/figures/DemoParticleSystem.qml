import QtQuick 2.0

Rectangle {
    id: ps

    width: parent.width
    height: parent.height

    property real kernelExponent: 1
    property real kernelOpacity: .2
    property int numParticles: 100

    border { width: 2; color: "gray" }

    Item {
        anchors.fill: parent
        anchors.margins: parent.border.width

        clip: true

        Repeater {
            model: m
            ParticleKernel {
                kernelExponent: ps.kernelExponent
                kernelOpacity: ps.kernelOpacity
                x: px
                y: py
            }
        }

        Repeater {
            model: m
            ParticleSphere {
                x: px
                y: py
                z: 1
                onMoved: {
                    m.set(index, {px: x, py: y})
                }
            }
        }
    }

    ListModel {
        id: m
    }

    function fillRandom(num) {
        for (var i = 0; i < num; ++i) {
            var item = {}
            item.px = Math.random() * width
            item.py = Math.random() * height
            m.append(item)
        }
    }

    Component.onCompleted: fillRandom(numParticles)
}
