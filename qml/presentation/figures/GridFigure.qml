import QtQuick 2.0

Item {
    width: 400
    height: 180

    ListModel {
        id: cellsModel
        ListElement { value: 0 }
        ListElement { value: 2 }
        ListElement { value: 7 }
        ListElement { value: 8 }
        ListElement { value: 8 }
        ListElement { value: 11 }
        ListElement { value: 15 }
        ListElement { value: 17 }
        ListElement { value: 19 }
    }

    Text {
        x: 40
        y: 5
        font.family: "Deja Vu Sans"
        font.pixelSize: 18
        text: "cells array:"
    }

    Text {
        x: 40
        y: 105
        font.family: "Deja Vu Sans"
        font.pixelSize: 18
        text: "particles array:"
    }

    Row {
        spacing: -1
        x: 200
        Repeater {
            model: cellsModel
            Rectangle {
                id: cell
                width: 30
                height: 30
                border { width: 1; color: "black" }
                color: index == cellsModel.count - 1 ? "lightgray" : "#aabbff"
                Text {
                    anchors.centerIn: parent
                    font.family: "Deja Vu Sans"
                    font.pixelSize: 18
                    text: value
                }
                Line {
                    z: -1
                    lineWidth: 3
                    color: Qt.rgba(0, 0, 0, .3)
                    start { x: 0; y: cell.height }
                    end { x: value * 29 - cell.x; y: 100 }
                }
            }
        }
    }

    Row {
        spacing: -1
        x: 200
        y: 100
        Repeater {
            model: cellsModel.get(cellsModel.count - 1).value
            Rectangle {
                width: 30
                height: 30
                border { width: 1; color: "black" }
                color: "#aaffaa"
                Text {
                    anchors.centerIn: parent
                    font.family: "Deja Vu Sans"
                    font.pixelSize: 18
                    text: index
                }
            }
        }
    }
}
