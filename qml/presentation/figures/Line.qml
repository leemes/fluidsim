import QtQuick 2.0

Item {
    id: line

    property point start: Qt.point(0, 0)
    property point end: Qt.point(100, 100)
    property real lineWidth: 1
    property color color: "black"
    property bool roundedCaps: true

    x: Math.min(start.x, end.x)
    y: Math.min(start.y, end.y)
    property real dx: end.x - start.x
    property real dy: end.y - start.y
    width: Math.abs(dx)
    height: Math.abs(dy)
    property real angle: Math.atan2(dy, dx)
    property real length: Math.sqrt(dx * dx + dy * dy)

    Rectangle {
        anchors.centerIn: parent
        width: length + 2 * radius
        height: line.lineWidth
        radius: roundedCaps ? height / 2 : 0
        color: line.color
        rotation: angle / Math.PI * 180.0
    }
}
