import QtQuick 2.0

Item {
    width: 1; height: 1

    property alias kernelExponent: shader.kernelExponent
    property real kernelOpacity: .2

    ShaderEffect {
        id: shader

        width: 150; height: 150
        anchors.centerIn: parent

        property color kernelColor: Qt.rgba(0, .1, .9, kernelOpacity)
        property real kernelExponent: 2

        fragmentShader: "
            varying highp vec2 qt_TexCoord0;

            uniform highp vec4 kernelColor;
            uniform highp float kernelExponent;
            uniform lowp float qt_Opacity;

            void main() {
                float r = length(qt_TexCoord0 * 2.0 - 1.0);
                if (r > 1.0)
                    gl_FragColor = vec4(0.0);
                else {
                    float k = pow(1.0 - r, kernelExponent);
                    gl_FragColor = kernelColor * k * qt_Opacity;
                }
            }"

    }
}

