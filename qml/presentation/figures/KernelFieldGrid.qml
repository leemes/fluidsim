import QtQuick 2.0

Item {
    width: height * 2.5; height: width * .4
    clip: true

    property alias kernelExponent: shader.kernelExponent

    ShaderEffect {
        id: shader
        width: parent.width; height: width
        y: height * -.275

        mesh: GridMesh { resolution: Qt.size(200, 200) }

        property variant source: Image { source: "grid.png" }
        property real kernelExponent: 2

        property real rotation: .2

        vertexShader: "
            uniform highp mat4 qt_Matrix;
            uniform highp float kernelExponent;
            uniform highp float rotation;
            uniform highp float width;
            uniform highp float height;

            attribute highp vec4 qt_Vertex;
            attribute highp vec2 qt_MultiTexCoord0;

            varying highp vec2 qt_TexCoord0;
            varying highp vec3 qt_Normal;
            varying highp float r;

            const highp float eps = 0.0001;

            mat4 makePerspective(float fovy, float aspect, float near, float far) {
                float f = 1.0 / tan(fovy / 360.0 * 3.14159265359);
                float a = f / aspect;
                float b = (far + near) / (near - far);
                float c = (2.0 * far * near) / (near - far);

                return mat4(a, 0, 0, 0,  // column-major notation!
                            0, f, 0, 0,
                            0, 0, b, -1,
                            0, 0, c, 1);
            }

            mat2 makeRotation(float angle) {
                float s = sin(angle), c = cos(angle);
                return mat2(c, s, -s, c);
            }

            float heightAt(vec2 pos) {
                float r = length(pos);
                float k = pow(1 - r, kernelExponent);
                return .5 * max(0, k);
            }

            void main() {
                // Modify vertex position
                vec2 pos = qt_MultiTexCoord0 * 2 - 1;
                float h = heightAt(pos);
                vec2 dh = vec2(heightAt(pos + vec2(eps, 0)), heightAt(pos + vec2(0, eps))) - h;
                vec3 normal = cross(vec3(0, dh.y, eps), vec3(eps, dh.x, 0));
                vec4 posInWorld = vec4(pos.x, h, pos.y, 1);

                // Model-View transformation
                posInWorld.xyz *= .15; // zoom
                posInWorld.xz = makeRotation(rotation) * posInWorld.xz;
                posInWorld.yz = makeRotation(0.3) * posInWorld.yz;

                // Perspective and QtQuick scene transformation
                mat4 matrixPers = makePerspective(20.0, 1.0, 0.0001, 10000.0);
                mat4 matrixClip = mat4(width/2, 0, 0, 0, 0, -height/2, 0, 0, 0, 0, 1, 0, width/2, height/2, 0, 1);
                gl_Position = qt_Matrix * (matrixClip * (matrixPers * posInWorld));

                // Other varying parameters
                qt_TexCoord0 = qt_MultiTexCoord0;
                qt_Normal = normalize(normal);
                r = length(pos);
            }"

        fragmentShader: "
            varying highp vec2 qt_TexCoord0;
            varying highp vec3 qt_Normal;
            varying highp float r;

            uniform sampler2D source;
            uniform lowp float qt_Opacity;

            const float ambient = .2;
            const float diffuse = .7;
            const float specular = 1;
            const float specularExp = 15;

            void main() {
                if (r > 1.0)
                    discard;
                lowp vec4 tex = texture2D(source, qt_TexCoord0);

                vec3 N = normalize(qt_Normal);
                vec3 L = normalize(vec3(1, 1, 1));
                float NdL = max(0, dot(N, L));
                float lighting = ambient + diffuse * NdL + specular * pow(NdL, specularExp);

                tex.rgb *= lighting;
                gl_FragColor = tex * qt_Opacity;
            }"
     }
}
