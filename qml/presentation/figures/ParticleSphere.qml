import QtQuick 2.0

Item {
    id: p
    width: 1; height: 1

    signal moved(var x, var y)

    Image {
        id: image
        sourceSize.width: 16
        sourceSize.height: 16
        anchors.centerIn: parent
        source: "particle.png"

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: mouseNavigationDisabled++
            onExited: mouseNavigationDisabled--

            onPositionChanged: {
                if (pressedButtons) {
                    var mouseInSceneX = mouseX + p.x - width/2
                    var mouseInSceneY = mouseY + p.y - height/2
                    p.moved(mouseInSceneX, mouseInSceneY)
                }
            }
        }
    }
}
