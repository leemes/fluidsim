import QtQuick 2.0

AbstractList {
    id: list

    property real indentation: 2.5
    property real bulletSeparation: .75
    property string bulletText: "\u2022"
    property real bulletScale: 1.5

    delegate: Item {
        implicitHeight: content.implicitHeight

        function setContent(item) {
            item.parent = content;
        }

        Item {
            id: content

            property real fontSize: list.fontSize
            property variant slide: list.slide

            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: list.indentation * fontSize

            implicitHeight: children[0].implicitHeight
        }

        Text {
            id: bulletPoint

            anchors.right: content.left
            anchors.rightMargin: list.bulletSeparation * fontSize

            text: list.bulletText

            font.pointSize: list.fontSize
            font.family: "Deja Vu Sans"
            scale: list.bulletScale
        }
    }
}
