import QtQuick 2.0

Item
{
    property real fontSize: parent.fontSize

    property bool isTab: true
    property real tabPos: tabRel * fontSize
    property real tabRel: 0
}

