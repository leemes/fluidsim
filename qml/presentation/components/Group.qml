import QtQuick 2.0

Column {
    id: item
    property real fontSize: (parent && parent.fontSize > 0) ? parent.fontSize : 10

    width: parent.width

    spacing: fontSize * .5
}
