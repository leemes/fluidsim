import QtQuick 2.0

AbstractList {
    id: list

    property real indentation: 2.5
    property real numberSeparation: .75
    property string numberFormat: "\\1."

    spacing: fontSize * (parent.isSlideContainer ? 1 : .333)

    delegate: Item {
        implicitHeight: content.implicitHeight
        property int number

        function setContent(item, number) {
            item.parent = content;
            this.number = number;
        }

        Item {
            id: content

            property real fontSize: list.fontSize
            property variant slide: list.slide

            anchors.right: parent.right
            anchors.left: parent.left
            anchors.leftMargin: list.indentation * fontSize

            implicitHeight: children[0].implicitHeight
        }

        Text {
            id: numberText

            anchors.right: content.left
            anchors.rightMargin: list.numberSeparation * fontSize

            text: list.numberFormat.
                replace('\\1', number).
                replace('\\a', String.fromCharCode(64 + number))

            font.pointSize: list.fontSize
            font.family: "Deja Vu Sans"
        }
    }
}
