import QtQuick 2.0

Rectangle {
    id: block

    property real fontSize: (parent && parent.fontSize > 0) ? parent.fontSize : 10

    property alias title: title.children
    default property alias content: content.children

    border { width: fontSize / 6; color: Qt.rgba(0,0,0,.2) }
    color: Qt.rgba(.9,.9,.9,1)
    radius: fontSize

    width: parent.width
    height: wrapper.implicitHeight + radius

    Column {
        id: wrapper

        width: parent.width - 2*block.radius
        x: block.radius
        y: block.radius/2

        spacing: block.radius / 2

        Column {
            id: title
            property real fontSize: block.fontSize
            width: parent.width
        }
        Column {
            id: content
            property real fontSize: block.fontSize
            width: parent.width

            spacing: fontSize * .5
        }
    }

    Component.onCompleted: {
        title.children[0].font.bold = true
    }
}
