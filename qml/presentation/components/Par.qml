import QtQuick 2.0

Item {
    id: par
    property real fontSize: (parent && parent.fontSize > 0) ? parent.fontSize : 10

    property string text
    property alias font: text.font
    property color color: (parent && parent.color) ? parent.color : "black"

    property real lineHeight: 1.1
    property real _lineSep: (fontSize * 2.0) * (lineHeight - 1.0)

    width: parent.width
    height: par.text ? text.implicitHeight : implicitHeight

    ParText {
        id: text
        property bool isSimpleText: true
        text: par.text
        color: par.color
        visible: par.text
        font.pointSize: fontSize
        width: parent.width
        wrapMode: Text.WordWrap
    }

    Timer {
        interval: 1; running: true
        onTriggered: par.relayout()
    }

    function relayout() {
        var w = width

        // per line:
        var x = 0, y = 0
        var aboveBase = 0
        var belowBase = 0
        var firstItem = 0

        for(var i = 0; i < children.length; ++i)
        {
            var item = children[i]
            var iw = Math.max(0, item.width)
            var ih = Math.max(0, item.height)
            var ibo = item.baselineOffset ? item.baselineOffset : ih

            if(item.isTab) {
                x = item.tabPos;
                continue;
            }
            
            if(iw == 0 || ih == 0 || item.isSimpleText)
                continue;

            //console.log(JSON.stringify({iw:iw,ih:ih,ibo:ibo,text:item.text}))

            // break line?
            if(x + iw > w) {
                //console.log('break line: ' + (x+iw) + '>' + w)
                // move the baseline down
                for(var j = firstItem; j < i; ++j)
                    children[j].y += aboveBase
                // update variables for next line
                y += aboveBase + belowBase + _lineSep
                //console.log("line height: " + (aboveBase + belowBase))
                x = aboveBase = belowBase = 0
                firstItem = i
            }

            var ix = x
            var iy = y - ibo // place the item so that its baseline = y
            item.x = ix
            item.y = iy
            x += iw
            aboveBase = Math.max(aboveBase, ibo)
            belowBase = Math.max(belowBase, ih - ibo)
        }

        // final line:
        //console.log("line height: " + (aboveBase + belowBase))

        // move the baseline down
        for(var j = firstItem; j < i; ++j)
            children[j].y += aboveBase

        implicitHeight = y + aboveBase + belowBase
    }
}
