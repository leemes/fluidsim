import QtQuick 2.0

Rectangle {
    id: presentation

    property int sourceWidth
    property int sourceHeight
    property real fontSize: sourceHeight * 0.024

    property real scaleFactor: viewport.scale

    property string title
    property string subTitle
    property string name
    property string institute

    default property alias content: viewport.children

    property int slidesCount
    property int numberedSlidesCount
    property int currentSlideIndex

    property int mouseNavigationDisabled: 0

    color: "black"
    width: sourceWidth
    height: sourceHeight

    onWidthChanged: updateScale()
    onHeightChanged: updateScale()

    signal toggleFullScreen();

    function updateScale() {
        var scaleX = width / sourceWidth
        var scaleY = height / sourceHeight
        viewport.scale = Math.min(scaleX, scaleY)
    }

    function next() {
        if(currentSlideIndex < slidesCount - 1)
            currentSlideIndex++;
    }

    function nextSlide() {
        var number = viewport.children[currentSlideIndex].slideNumber
        jumpToSlide(number + 1)
    }

    function prev() {
        if(currentSlideIndex > 0)
            currentSlideIndex--;
    }

    function prevSlide() {
        var number = viewport.children[currentSlideIndex].slideNumber
        jumpToSlide(number - 1)
    }

    function jumpToSlide(number) {
        for(var i = 0; i < viewport.children.length; ++i) {
            if(viewport.children[i].slideNumber === number) {
                currentSlideIndex = i;
                return true;
            }
        }
        return false;
    }

    Item {
        id: viewport
        anchors.centerIn: parent
        width: sourceWidth
        height: sourceHeight
        clip: true

        property variant presentation: presentation

        // [ ... content goes here ... ]
    }

    // Controls
    focus: jumpToPopup.visible == false
    Keys.onPressed: {
        switch(event.key) {
        case Qt.Key_Escape:
        case Qt.Key_Q:
            Qt.quit()
            break;
        case Qt.Key_Backspace:
        case Qt.Key_Up:
        case Qt.Key_PageUp:
            prev()
            break;
        case Qt.Key_Left:
            prevSlide()
            break;
        case Qt.Key_Space:
        case Qt.Key_Return:
        case Qt.Key_Enter:
        case Qt.Key_Down:
        case Qt.Key_PageDown:
            next()
            break;
        case Qt.Key_Right:
            nextSlide()
            break;
        case Qt.Key_F11:
        case Qt.Key_F5:
        case Qt.Key_F:
            toggleFullScreen()
            break;
        case Qt.Key_Period:
            viewport.opacity = 1 - viewport.opacity
            break;
        case Qt.Key_J:
            jumpToPopup.visible = true
            break;
        }
    }

    MouseArea {
        anchors.fill: parent
        enabled: presentation.mouseNavigationDisabled === 0
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onPressed: {
            if(mouse.button === Qt.LeftButton)
                next()
            if(mouse.button === Qt.RightButton)
                prev()
        }
    }

    Component.onCompleted: {
        var n = 0;
        for(var i = 0; i < viewport.children.length; i++) {
            var s = viewport.children[i];
            s.slideIndex = i;
            if(s.slideNumbered) {
                if(!s.isContinuation)
                    n++;
                s.slideNumber = n;
            }
        }
        presentation.slidesCount = viewport.children.length;
        presentation.numberedSlidesCount = n;

        //console.log('The presentation contains ' + presentation.slidesCount + ' slides.')
    }


    Rectangle {
        id: jumpToPopup
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, .5)
        visible: false

        Rectangle {
            color: Qt.rgba(0, 0, 0, .7)
            radius: sourceWidth / 50
            width: sourceWidth / 2
            height: sourceHeight / 4
            anchors.centerIn: parent

            Text {
                font.pixelSize: parent.height / 6
                font.family: "Deja Vu Sans"

                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: parent.height / 10

                text: "Jump to slide"
                color: "gray"
            }

            TextEdit {
                id: jumpToEdit

                font.pixelSize: parent.height / 4
                font.family: "Deja Vu Sans"

                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: parent.height / 4

                color: "white"
                focus: jumpToPopup.visible

                Keys.onReturnPressed: accept()
                Keys.onEnterPressed: accept()

                function accept() {
                    var ok = presentation.jumpToSlide(parseInt(jumpToEdit.text))
                    if(ok) {
                        jumpToPopup.visible = false
                        jumpToEdit.text = ''
                    } else {
                        jumpToEdit.selectAll()
                    }
                }
            }
        }
    }
}
