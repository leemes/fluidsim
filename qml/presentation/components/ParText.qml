import QtQuick 2.0

Text {
    id: text
    property real fontSize: (parent && parent.fontSize > 0) ? parent.fontSize : 10
    font.pointSize: fontSize
    font.family: "Deja Vu Sans"
    width: implicitWidth
    height: implicitHeight
    color: parent.color ? parent.color : "black"
}
