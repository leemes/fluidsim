import QtQuick 2.0

Column
{
    id: list
    property bool isList: true
    property bool isTopList: true

    property variant slide: parent.slide
    property real fontSize:  parent ? (parent.isSlideContainer ? parent.fontSize : parent.fontSize * 0.85) : 10

    default property alias items: items.children

    property variant title

    /** This delegate component is required to define a function placeContent(item) in which the item should be reparented. */
    property Component delegate

    width: parent.width
    spacing: fontSize * .333

    /** Temporary container. QML will add child items here (through the default property aliasing). */
    Item {
        id: items
        property real fontSize: list.fontSize
    }

    Component.onCompleted: {
        // Move the child items from the temporary container into newly created delegates
        var count = items.children.length; // copy value in temporary as the children will move away during loop!
        for(var i = 0; i < count; ++i) {
            var thisItem = items.children[0]; // take first (will be taken from the children list when reparenting)
            var thisDelegate = list.delegate.createObject(list)
            thisDelegate.setContent(thisItem, i + 1)
            thisDelegate.width = Qt.binding(function() { return list.width; }) // the delegate doesn't have to bother about the width property
        }
    }
}
