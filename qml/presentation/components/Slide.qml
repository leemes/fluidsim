import QtQuick 2.0

Item {
    id: slide
    property bool isSlide: true

    property real fontSize: presentation.fontSize

    /**
      The title of this slide. The slide template determines what to do with this property.
    */
    property string title: "(Title missing)"

    /**
      Set to true to not increase the slide number for this slide
    */
    property bool isContinuation: false

    property int slideIndex: 0
    property bool slideNumbered: true
    property int slideNumber: 0

    property variant presentation: parent.presentation

    //property real smoothedVisibility: slideIndex <= presentation.currentSlideIndex
    //Behavior on smoothedVisibility { NumberAnimation { duration: 300; easing.type: Easing.OutQuad } }
    visible: slideIndex === presentation.currentSlideIndex

    //opacity: smoothedVisibility
    //scale: 1 + (1 - smoothedVisibility) * .1

    anchors.fill: parent

    signal entered()
    signal exited()

    onVisibleChanged: if (visible) entered(); else exited()
}
