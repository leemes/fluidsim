import QtQuick 2.0

Rectangle {
    property alias src: image.source;
    property string alt;
    property alias source: image.source

    implicitHeight: image.implicitHeight
    implicitWidth: image.implicitWidth

    border { width: 1; color: "red" }

    baselineOffset: 5

    Image {
        id: image;
    }

    Rectangle {
        width: parent.width
        height: 1
        y: parent.baselineOffset
        color: "blue"
    }
}
