import QtQuick 2.0

Column {
    property real buttonSize: 30
    property real buttonRadius: 5

    property bool demoRunning: false

    signal resetPressed()
    signal startPressed()
    signal stopPressed()
    signal stepPressed()

    DemoButton {
        text: "reset"
        onPressed: resetPressed()
    }
    DemoButton {
        text: demoRunning ? "stop" : "start"
        onPressed: demoRunning ? stopPressed() : startPressed()
    }
    DemoButton {
        visible: !demoRunning
        text: "step"
        onPressed: stepPressed()
    }
}
