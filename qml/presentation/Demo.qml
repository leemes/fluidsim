import QtQuick 2.0
import FluidSimulation 1.0

Loader {
    id: demo
    anchors.left: parent.left
    anchors.right: parent.right
    height: width / (16/9)

    property bool enabled: visible

    property real frameRate: 61
    property bool border: true
    property bool info: true
    property bool buttons: true
    property bool running: false
    property bool interactive: true

    property real yaw: 0.1
    property real pitch: 0.5
    property real fovy: 16.0
    property real centerHeight: 0.1

    property list<Emitter> emitters
    property list<RigidBody> rigidBodies

    property int maxParticles: -1

    readonly property int gridSize: enabled ? item.gridSize : 0
    readonly property int numParticles: enabled ? item.numParticles : 0
    readonly property real simulationUpdateTime: enabled ? item.simulationUpdateTime : 0
    readonly property real gridConstructionTime: enabled ? item.gridConstructionTime : 0

    sourceComponent: enabled ? enabledComponent : disabledComponent

    Component {
        id: disabledComponent
        Rectangle {
            color: "#b0b0b0"
            Text {
                anchors.centerIn: parent
                font.family: "Deja Vu Sans"
                font.pixelSize: 20
                text: "(interactive demo disabled)"
            }
        }
    }

    Component {
        id: enabledComponent
        FluidSimulation {
            id: fluidSimulation

            centerHeight: demo.centerHeight

            yaw: demo.yaw
            pitch: demo.pitch
            fovy: demo.fovy

            function start() { timer.start() }
            function stop() { timer.stop() }
            function step() { timer.trigger() }

            Timer {
                id: timer
                interval: 1000.0 / demo.frameRate; running: false; repeat: true
                onTriggered: fluidSimulation.iterate()
            }
            Rectangle {
                anchors.fill: parent
                anchors.margins: 1
                z: -1
                color: "black"
            }

            emitters: demo.emitters
            rigidBodies: demo.rigidBodies
            maxParticles: demo.maxParticles
        }
    }

    Rectangle {
        anchors.fill: parent
        anchors.margins: -2
        radius: 9
        color: "transparent"
        border {
            color: mouseNavigationDisabled === 0 ? "lightgray" : "gray"
            width: demo.border ? 3 : 0
        }
        z: 1

        MouseArea {
            enabled: demo.enabled && demo.interactive
            hoverEnabled: demo.enabled && demo.interactive

            anchors.fill: parent
            anchors.margins: -20

            property real prevX
            property real prevY

            onEntered: mouseNavigationDisabled++
            onExited: mouseNavigationDisabled--

            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onPressed: {
                prevX = mouse.x
                prevY = mouse.y
            }
            onMouseXChanged: {
                if (mouse.buttons & Qt.RightButton)
                    demo.yaw -= (mouse.x - prevX) * (5 / width)
                prevX = mouse.x
            }
            onMouseYChanged: {
                if (mouse.buttons & Qt.RightButton)
                    demo.pitch += (mouse.y - prevY) * (5 / height)
                prevY = mouse.y
            }
            onWheel: {
                demo.fovy *= 1.0 - .001 * wheel.angleDelta.y
            }
        }

        Column {
            id: info
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 10
            visible: demo.enabled && demo.info

            Text {
                font.family: "Deja Vu Sans"; font.pixelSize: 14
                text: "Grid size: <b>" + gridSize + " ⨯ " + gridSize + " ⨯ " + gridSize + "</b>"
            }
            Text {
                font.family: "Deja Vu Sans"; font.pixelSize: 14
                text: "Number of particles: <b>" + numParticles + "</b>"
            }
            Text {
                font.family: "Deja Vu Sans"; font.pixelSize: 14
                text: "Simulation update: <b>" + simulationUpdateTime.toFixed(1) + " ms</b>"
            }
            Text {
                font.family: "Deja Vu Sans"; font.pixelSize: 14
                text: "Grid construction: <b>" + gridConstructionTime.toFixed(1) + " ms</b>"
            }
        }

        DemoButtons {
            id: buttons
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 5
            visible: demo.enabled && demo.buttons

            demoRunning: demo.running
            onResetPressed: { demo.visible = false; demo.visible = true }
            onStartPressed: { demo.running = true }
            onStopPressed: { demo.running = false }
            onStepPressed: { demo.item.step() }
        }
    }

    onLoaded: if (enabled && running) item.start() // Late starting the simulation
    onVisibleChanged: if (!enabled) running = false
    onRunningChanged: if (enabled && status == Loader.Ready) running ? item.start() : item.stop()
}
