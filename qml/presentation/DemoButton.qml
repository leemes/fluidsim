import QtQuick 2.0

Item {
    property real radius: parent.buttonRadius
    property alias text: label.text

    width: parent.buttonSize * 2
    height: parent.buttonSize

    signal pressed()

    Rectangle {
        anchors.fill: parent
        anchors.margins: 2
        color: "black"
        border.color: ma.containsMouse ? "white" : "transparent"
        opacity: ma.containsMouse ? (ma.pressedButtons ? 1 : .7) : .4
        border.width: 2
        radius: parent.radius

        Text {
            id: label
            anchors.centerIn: parent
            color: "white"
            font.family: "Deja Vu Sans"
            font.pixelSize: parent.height * .6
        }
    }

    MouseArea {
        id: ma
        hoverEnabled: true
        anchors.fill: parent
        onEntered: mouseNavigationDisabled++
        onExited: mouseNavigationDisabled--
        onReleased: if (containsMouse) parent.pressed()
    }
}
