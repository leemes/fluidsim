import QtQuick 2.0
import "components"
import "template"
import "algorithm"
import "demos"
import "figures"
import "helper"
import "."


Presentation {
    id: presentation

    sourceWidth: 960
    sourceHeight: sourceWidth / (4/3)
    title: "Realtime Fluid Simulation"
    subTitle: "Smoothed Particle Hydrodynamics in OpenCL"
    institute: "GPU Computing Course Summer 2013"
    name: "Sebastian Lehmann"

    // Title demo
    banner: DemoBanner {}


    /////////////////////////////////////////////////////////////////////////////////////
    // OVERVIEW
    /////////////////////////////////////////////////////////////////////////////////////

    Slide {
        title: "Smoothed Particle Hydrodynamics"

        List {
            ParText { text: "Well-known simulation scheme (developed in early 1990s)" }

            Group {
                ParText { text: "Computation in distinct time steps" }
                List {
                    ParText { text: "smaller steps ⇔ better results" }
                }
            }

            Group {
                ParText { text: "Physical properties <b>defined per particle</b>" }
                List {
                    ParText { text: "position" }
                    ParText { text: "velocity" }
                    ParText { text: "mass (often implicit)" }
                    ParText { text: "(can be extended easily...)" }
                }
            }

            Group {
                ParText { text: "<b>Smoothed</b> fluid properties" }
                List {
                    ParText { text: "<i>neighborhood</i> = particles within <i>radius of interaction</i> r = 1" }
                    ParText { text: "distant particles have less influence (<i>\"kernel\"</i>)" }
                }
            }
        }
    }



    Slide {
        title: "Smooth Kernels"

        Row {
            width: parent.width
            property real fontSize: parent.fontSize
            Column {
                width: parent.width * .5
                spacing: fontSize
                property real fontSize: parent.fontSize
                ParText { text: "k = (1 – d)ⁿ"; font.bold: true }
                ParText { text: "(<b>d</b>: distance to particle)" }
                Slider {
                    id: kernelExponentSlider
                    width: parent.width
                    minimum: 1; maximum: 3
                }
                ParText { text: "<b>n</b> = " + kernelExponentSlider.value.toFixed(2) }
            }
            KernelFieldGrid {
                width: parent.width * .5
                kernelExponent: kernelExponentSlider.value
            }
        }

        DemoParticleSystem {
            kernelExponent: kernelExponentSlider.value
            kernelOpacity: .2 * kernelExponent
            height: .33 * width
            numParticles: 50
        }

    }

    Slide {
        title: "SPH: Sequential Implementation"

        Algorithm {
            Procedure { text: "updateParticles"
                ForEach { text: "particle"
                    Do { text: "integrate" }
                }
                ForEach { text: "pair of near particles"
                    Do { text: "apply viscosity" }
                    Do { text: "correct density errors" }
                }
                ForEach { text: "pair of particle and rigid body"
                    Do { text: "test and resolve collisions" }
                }
            }
        }
    }
    Slide {
        title: "SPH: Parallel Implementation"
        isContinuation: true

        Algorithm {
            Procedure { text: "updateParticles"
                ForEach { text: "particle <b>do in parallel</b>"
                    Do { text: "integrate" }
                    Do { text: "apply viscosity using neighborhood" }
                    Do { text: "correct density errors using neighborhood" }
                    ForEach { text: "rigid body"
                        Do { text: "test and resolve collisions" }
                    }
                }
            }
        }
    }
    Slide {
        title: "SPH: Parallel Implementation"
        isContinuation: true

        Algorithm {
            Procedure { text: "updateParticles"
                ForEach { text: "particle <b>do in parallel</b>"
                    Highlight { Do { text: "integrate" } }
                    Highlight { Do { text: "apply viscosity using neighborhood" } }
                    Highlight { Do { text: "correct density errors using neighborhood" } }
                    ForEach { text: "rigid body"
                        Highlight { Do { text: "test and resolve collisions" } }
                    }
                }
            }
        }

        Block {
            title: ParText { text: "Physical Problem" }
            ParText { text: "How are the formulas for the physics?" }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////
    // PHYSICS
    /////////////////////////////////////////////////////////////////////////////////////

    Slide {
        title: "Integration"

        Block {
            title: ParText { text: "Integration" }
            ParText { text: "Apply Newton's laws of motion" }
        }

        Algorithm {
            Procedure { text: "integrate"
                Do { text: "increase velocity by <i>Δt · gravity</i>" }
                Do { text: "advance position by <i>Δt · velocity</i>" }
            }
        }
    }



    Slide {
        title: "Viscosity"

        Block {
            title: ParText { text: "Viscosity" }
            ParText { text: "Velocities of near particles approximate progressively (friction)" }
        }

        Algorithm {
            Procedure { text: "applyViscosity"
                Do { text: "compute <i>relative velocity</i> of neighbors" }
                Do { text: "multiply with <i>\"viscosity\"</i> (small factor)" }
                Do { text: "add result to own velocity" }
            }
        }
    }



    Slide {
        title: "Density Correction"

        Block {
            title: ParText { text: "Incompressibility" }
            ParText { text: "Density in fluids is almost constant" }
        }

        ParText { text: "Very important for realistic behavior!" }

        Algorithm {
            Procedure { text: "correctDensity"
                Do { text: "compute <i>relative position</i> of neighbors" }
                Do { text: "compute <i>local density</i> of the neighborhood" }
                If { text: "local density too high"
                    Do { text: "apply force <i>away</i> from neighbors" }
                }
                ElseIf { text: "local density too low"
                    Do { text: "apply force <i>towards</i> neighbors" }
                }
            }
        }
    }



    Slide {
        title: "Collision with Scene"

        Block {
            title: ParText { text: "Rigid Bodies" }
            ParText { text: "Particles collide with the scene, but scene is fixed" }
        }

        ParText { text: "Prerequisites: <i>signed distance field</i> of the scene (can be implicit)" }

        Algorithm {
            Procedure { text: "resolveCollisions"
                Do { text: "compute <i>distance</i> to rigid bodies of the scene" }
                If { text: "<i>distance &lt; 0</i> (particle is within rigid body)"
                    Do { text: "compute <i>normal</i> (= gradient of distance field)" }
                    Do { text: "move particle out (subtract <i>distance · normal</i> from position)" }
                    Do { text: "reflect velocity at <i>normal</i> (and apply friction and elasticity)" }
                }
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////
    // GRID
    /////////////////////////////////////////////////////////////////////////////////////

    Slide {
        title: "SPH: Parallel Implementation"

        Algorithm {
            Procedure { text: "updateParticles"
                ForEach { text: "particle <b>do in parallel</b>"
                    Do { text: "integrate" }
                    Do { text: "apply viscosity using neighborhood" }
                    Do { text: "correct density errors using neighborhood" }
                    ForEach { text: "rigid body"
                        Do { text: "test and resolve collisions" }
                    }
                }
            }
        }
    }
    Slide {
        title: "SPH: Parallel Implementation"

        Algorithm {
            Procedure { text: "updateParticles"
                ForEach { text: "particle <b>do in parallel</b>"
                    Do { text: "integrate" }
                    ForEach { text: "particle in neighborhood"
                        Do { text: "collect information for viscosity" }
                    }
                    Do { text: "apply viscosity" }
                    ForEach { text: "particle in neighborhood"
                        Do { text: "collect information for density correction" }
                    }
                    Do { text: "apply density correction" }
                    ForEach { text: "rigid body"
                        Do { text: "test and resolve collisions" }
                    }
                }
            }
        }
    }
    Slide {
        title: "SPH: Parallel Implementation"

        Algorithm {
            Procedure { text: "updateParticles"
                ForEach { text: "particle <b>do in parallel</b>"
                    Do { text: "integrate" }
                    Highlight {
                        ForEach { text: "particle in neighborhood"
                            Do { text: "collect information for viscosity" }
                        }
                    }
                    Do { text: "apply viscosity" }
                    Highlight {
                        ForEach { text: "particle in neighborhood"
                            Do { text: "collect information for density correction" }
                        }
                    }
                    Do { text: "apply density correction" }
                    ForEach { text: "rigid body"
                        Do { text: "test and resolve collisions" }
                    }
                }
            }
        }

        Block {
            title: ParText { text: "Technical Problem" }
            ParText { text: "How to find neighborhood of particles?" }
        }
    }



    Slide {
        title: "Neighbor search"

        List {
            Group {
                ParText { text: "Data structure required" }
                ParText { text: "⇒ <b>regular grid:</b> simple and efficient" }
            }
            Group {
                ParText { text: "Rebuild grid in <i>each simulation step</i>" }
                ParText { text: "⇒ has to be very efficient" }
            }
        }
    }



    Slide {
        title: "Grid data structure"

        List {
            Group {
                spacing: fontSize
                ParText { text: "Array of cells; each cell points at its <i>first particle:</i>" }
                GridFigure {}
            }

            Group {
                ParText { text: "Grid construction:" }
                List {
                    ParText { text: "<b>sort</b> particles according to their cell ⇒ <i>radix sort</i>" }
                    ParText { text: "find <b>first particle</b> for each cell" }
                }
            }
        }
    }



    Slide {
        title: "Radix Sort"

        Block {
            title: ParText { text: "Radix Sort" }
            List {
                ParText { text: "multi-criteria sorting (<b>k</b> criteria, e.g. digits of numbers)" }
                ParText { text: "<b>k</b> · stable bucket sort" }
                ParText { text: "from <i>least significant</i> to <i>most significant</i> criterion" }
            }
        }

        ParText { text: "⇒ radix sort = <b>k</b> · stable bucket sort" }
    }



    Slide {
        title: "Parallel Stable Bucket Sort"

        ParText { text: "Bucket function: <b>b(i)</b> = target bucket of element <b>i</b>" }

        Block {
            title: ParText { text: "Bucket Sort" }
            List {
                ParText { text: "<b>N</b> elements, <b>B</b> buckets" }
                ParText { text: "sort elements according to their bucket number <b>b</b>" }
            }
        }

        List {
            Group {
                ParText { text: "Naïve (similar to stream compaction):" }
                List {
                    ParText { text: "array <b>a</b> of size <b>B · N</b> (imagine as <b>B</b> arrays of size <b>N</b>), initialize with:<br/>" +
                                    "&nbsp; <b>a[i + k · N]</b> = 1 &nbsp; if <b>k</b> == <b>b(i)</b>,<br/>" +
                                    "&nbsp; <b>a[i + k · N]</b> = 0 &nbsp; otherwise" }
                    ParText { text: "scan <b>a</b> (exclusively)" }
                    ParText { text: "put element <b>i</b> at <b>a[i + b(i) · N]</b> in output array" }
                }
            }
            ParText { text: "Memory consumption is in <b>O(B · N)</b>, we want <b>O(N)</b>!" }
        }
    }



    Slide {
        title: "Parallel Stable Bucket Sort: Optimization"

        ParText { text: "Compress <b>a</b> in global memory." }

        List {
            spacing: fontSize * .5
            ParText { text: "<i>initial step:</i> compute partial sums of <b>a</b> (<b>X:1</b> reduction)" }
            ParText { text: "<i>scan step:</i> scan these partial sums (exclusively)" }
            ParText { text: "<i>final step:</i> compute partial scan of <b>a</b>, add scanned partial sum<br/>" +
                            "&nbsp; ⇒ also tells us the target index" }
        }

        ParText { text: "Reduces memory consumption by factor <b>X</b> (<b>X == B</b> ⇒ <b>O(N)</b>)" }
    }

    Slide {
        title: "SPH: Parallel Implementation using Grid"

        Algorithm {
            Procedure { text: "updateParticles"
                ForEach { text: "particle <b>do in parallel</b>"
                    Do { text: "integrate" }
                    Do { text: "construct grid" }
                    Highlight {
                        ForEach { text: "particle in neighborhood using grid"
                            Do { text: "collect information for viscosity" }
                        }
                    }
                    Do { text: "apply viscosity" }
                    Highlight {
                        ForEach { text: "particle in neighborhood using grid"
                            Do { text: "collect information for density correction" }
                        }
                    }
                    Do { text: "apply density correction" }
                    ForEach { text: "rigid body"
                        Do { text: "test and resolve collisions" }
                    }
                }
            }
        }
    }
    Slide {
        title: "SPH: Parallel Implementation: Optimization"
        isContinuation: true


        Algorithm {
            Procedure { text: "updateParticles"
                ForEach { text: "particle <b>do in parallel</b>"
                    Do { text: "integrate" }
                    Do { text: "construct grid" }
                    Highlight {
                        ForEach { text: "particle in neighborhood using grid"
                            Do { text: "collect information" }
                        }
                    }
                    Do { text: "apply viscosity" }
                    Do { text: "apply density correction" }
                    ForEach { text: "rigid body"
                        Do { text: "test and resolve collisions" }
                    }
                }
            }
        }

        Block {
            title: ParText { text: "Processing neighborhood is expensive" }
            ParText { text: "Only iterate over neighborhood <b>once</b>" }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////
    // DEMOS
    /////////////////////////////////////////////////////////////////////////////////////

    Slide {
        title: "Demo: Water on Floor"
        DemoPourFloor {}
    }

    Slide {
        title: "Demo: Rain"
        DemoRain {}
    }

    Slide {
        title: "Demo: Waves at the Beach"
        DemoBeach {}
    }
}
