import QtQuick 2.0
import FluidSimulation 1.0
import ".."

Demo {
    maxParticles: 1 << 17
    centerHeight: .1

    property real waveAngle
    property real wavePos: .5 * waveAmplitude * (1 - Math.cos(waveAngle / Math.PI * 180))
    property real waveAmplitude: .05

    emitters: [
        Emitter { rate: 200; position: Qt.vector3d(.1, .5, .05) },
        Emitter { rate: 200; position: Qt.vector3d(.2, .5, .05) },
        Emitter { rate: 150; position: Qt.vector3d(.3, .5, .05) },
        Emitter { rate: 150; position: Qt.vector3d(.4, .5, .1) },
        Emitter { rate: 100; position: Qt.vector3d(.5, .5, .1) },
        Emitter { rate: 100; position: Qt.vector3d(.6, .5, .15) },
        Emitter { rate: 50; position: Qt.vector3d(.7, .5, .15) },
        Emitter { rate: 50; position: Qt.vector3d(.8, .5, .2) }
    ]
    rigidBodies: [
        RigidBox { position: Qt.vector3d(.5, .5, .5); size: Qt.vector3d(.49, .15, .49); inverted: true },
        RigidSphere { position: Qt.vector3d(1.5, .5, -5.0); radius: 5.15 },
        RigidBox { position: Qt.vector3d(-.5 + wavePos, .5, .5); size: Qt.vector3d(.5, .6, .6) }
    ]

    SequentialAnimation on waveAngle {
        PauseAnimation { duration: 30000 }
        NumberAnimation { loops: Animation.Infinite; from: 0; to: 360; duration: 40000000 }
    }
}
