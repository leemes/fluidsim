import QtQuick 2.0
import FluidSimulation 1.0
import ".."

Demo {
    maxParticles: 1 << 19
    centerHeight: .1

    emitters: [
        Emitter { id: rain; rate: 20; velocity: Qt.vector3d(0, 0, -.003); density: 2 }
    ]
    rigidBodies: [
        RigidBox { position: Qt.vector3d(.5, .5, .5); size: Qt.vector3d(.25, .25, .49); inverted: true; reflectNormal: .1; reflectTangential: .1 }
    ]

    Timer {
        interval: 20; running: true; repeat: true
        onTriggered: {
            rain.position = Qt.vector3d(Math.random() * .4 + .3, Math.random() * .4 + .3, .5)
        }
    }
}
