import FluidSimulation 1.0
import ".."

Demo {
    maxParticles: 1 << 16
    centerHeight: .1

    emitters: [
        Emitter { rate: 50 }
    ]
    rigidBodies: [
        RigidBox { position: Qt.vector3d(.5, .5, .5); size: Qt.vector3d(.49, .49, .49); inverted: true }
    ]
}
