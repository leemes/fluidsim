import FluidSimulation 1.0
import ".."

Demo {
    maxParticles: 1 << 16
    anchors.fill: parent
    centerHeight: 0.125
    yaw: 0.2
    pitch: .3
    fovy: 8

    border: false; info: false; buttons: false
    running: true

    emitters: [
        Emitter {
            rate: 75
            position: Qt.vector3d(.6, .5, .4)
            velocity: Qt.vector3d(-.02, 0, -.02)
        }
    ]
    rigidBodies: [
        RigidBox { position: Qt.vector3d(.5, .5, .5); size: Qt.vector3d(.25, .125, .49); inverted: true }
    ]
}
