import QtQuick 2.0
import FluidSimulation 1.0

Rectangle {
    id: root
    focus: true
    width: 1280
    height: 720
    color: "black"

    FluidSimulation {
        id: fluidSimulation
        anchors.fill: parent

        property real waveAngle
        property real waveAmplitude: .1
        property real wavePos: (1 - Math.cos(waveAngle / 180.0 * Math.PI)) * .5 * waveAmplitude  // 0 ... waveAmplitude

        rigidBodies: [
            RigidBox    { position: Qt.vector3d(-.4, .5, .7 - fluidSimulation.wavePos); size: Qt.vector3d(.51, .51, .51) },
           // RigidBox    { position: Qt.vector3d(.5, .5, .5); size: Qt.vector3d(.5, .5, .5); inverted: true },
            RigidBox    { position: Qt.vector3d(.5, .5, .5); size: Qt.vector3d(.49, .49, .49); inverted: true }
           // RigidSphere { position: Qt.vector3d(2, .5, -6); radius: 6.2 },
           // RigidSphere { position: Qt.vector3d(.5, .5, .5); radius: .5; inverted: true }
        ]

        emitters: [
            Emitter {
                id: emitter
                property real yaw: -Math.PI / 2
                property real pitch: Math.PI / 2
                property real vel: .01
                property real dist: .4
                property vector3d d: Qt.vector3d(Math.sin(yaw) * Math.cos(pitch), Math.cos(yaw) * Math.cos(pitch), Math.sin(pitch))
                position: Qt.vector3d(.5 - dist * d.x, .5 - dist * d.y, .5 - dist * d.z)
                velocity: Qt.vector3d(vel * d.x, vel * d.y, vel * d.z)
                rate: 60
                enabled: true
            },

            Emitter {
                id: rain
                rate: 30
                enabled: false
                position: Qt.vector3d(.5, .5, .9)
            }
        ]

        Timer {
            interval: 1; running: true; repeat: true
            onTriggered: parent.iterate()
        }
        Timer {
            interval: 100; running: true; repeat: true
            onTriggered: {
                rain.position = Qt.vector3d(Math.random() * .9 + .05, Math.random() * .9 + .05, .9)
            }
        }

        RotationAnimation on waveAngle {
            id: wave
            from: 0
            to: 360
            duration: 6000
            running: false
        }
    }

    MouseArea {
        anchors.fill: parent
        property real prevX
        property real prevY
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onPressed: {
            prevX = mouse.x
            prevY = mouse.y
        }
        onMouseXChanged: {
            if (mouse.buttons & Qt.RightButton)
                fluidSimulation.yaw -= (mouse.x - prevX) * (5 / width)
            if (mouse.buttons & Qt.LeftButton)
                emitter.yaw += (mouse.x - prevX) * (5 / width)
            prevX = mouse.x
        }
        onMouseYChanged: {
            if (mouse.buttons & Qt.RightButton)
                fluidSimulation.pitch += (mouse.y - prevY) * (5 / height)
            if (mouse.buttons & Qt.LeftButton)
                emitter.pitch += (mouse.y - prevY) * (5 / height)
            prevY = mouse.y
        }
        onWheel: {
            fluidSimulation.fovy *= 1.0 - .001 * wheel.angleDelta.y
        }
    }

    Column {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: root.height / 80
        Text {
            font.pointSize: root.height / 40
            text: "Grid size: <b>" + fluidSimulation.gridSize + " ⨯ " + fluidSimulation.gridSize + " ⨯ " + fluidSimulation.gridSize + "</b>"
        }
        Text {
            font.pointSize: root.height / 40
            text: "Number of particles: <b>" + Math.floor(fluidSimulation.numParticles / 1024) + "K</b>"
        }
        Text {
            font.pointSize: root.height / 40
            text: "Simulation update: <b>" + fluidSimulation.simulationUpdateTime.toFixed(1) + " ms</b>"
        }
        Text {
            font.pointSize: root.height / 40
            text: "Grid construction: <b>" + fluidSimulation.gridConstructionTime.toFixed(1) + " ms</b>"
        }
    }

    Keys.onSpacePressed: wave.start()
}
