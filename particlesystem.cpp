#include "particlesystem.h"
#include "grid.h"
#include "simulation.h"
#include "util/move.h"
//#include "renderer.h"
#include "util.h"
#include <QTimer>
#include <QAction>
#include <QSignalMapper>
#include <QApplication>
#include <QFile>
#include <rigidbody.h>


// OpenCL shared code:
#include "cl/particle.h"
#include "cl/simulationconstants.h"
#include "cl/rigidbody.h"



QPointF randDisc(float rMax) {
    float r = frand();
    r = sqrt(r) * rMax;
    float phi = 2.0f * M_PI * frand();
    return QPointF(r * cos(phi), r * sin(phi));
}


ParticleSystem::ParticleSystem(uint gridDepth) :
    QObject(0),
    m_context(":/cl/", (CLContext::Options)(CLContext::Profiling | CLContext::GlSharing)),
    m_gridDepth(gridDepth)
{
    Q_ASSERT(gridDepth <= 8 && gridDepth >= 5);

    m_context.addAutoInclude("ParticleBasic", "particle.h");
    m_context.addAutoInclude("ParticleVelocity", "particle.h");
    m_context.addAutoInclude("ParticleRender", "particle.h");
    m_context.addAutoInclude("ParticleSprings", "particle.h");
    m_context.addAutoInclude("RigidBox", "rigidbody.h");
    m_context.setCompilerFlag(CLContext::OptimizationMadEnable);
    m_context.setCompilerFlag(CLContext::OptimizationNoSignedZeros);
    m_context.setCompilerFlag(CLContext::OptimizationUnsafeMath);
    m_context.setCompilerFlag(CLContext::OptimizationFiniteMathOnly);
    m_context.setCompilerFlag(CLContext::OptimizationFastRelaxedMath);
    m_context.setCompilerFlag(CLContext::OptimizationSinglePrecConst);
    m_context.setCompilerFlag(CLContext::OptimizationDenormsAreZero);
    m_context.setCompilerFlag(CLContext::OptimizationStrictAliasing);

    qDebug("Compiling OpenCL kernels...");

    m_grid.reset(new Grid(m_context, gridDepth));
    m_simulation.reset(new Simulation(m_context, gridDepth));
    m_moveParticleVelocity.reset(new Move(m_context, "ParticleVelocity", "ParticleVelocity", "element", Move::TableNewOld));

    qDebug("Done.");

    m_maxParticles = m_allocParticles = 1 << 19;
    m_numParticles = 0;
    m_allocRigidBodies = 1 << 10;
    allocateBuffers();

    qDebug() << "CTOR THREAD: " << QThread::currentThread();
    qDebug() << "thread() returns " << thread();
}

ParticleSystem::~ParticleSystem()
{
    qDebug() << "DTOR THREAD: " << QThread::currentThreadId();
    qDebug() << "thread() returns " << thread();
}

void ParticleSystem::setMaxParticles(uint maxParticles)
{
    m_maxParticles = maxParticles;
}

void ParticleSystem::addRigidBody(RigidBody *object)
{
    writeRigidBody(object, m_rigidBodies.count());
    connect(object, SIGNAL(changed()), SLOT(rigidBodyChanged()));
    m_rigidBodies << object;
}

QList<RigidBody*> ParticleSystem::rigidBodies() const
{
    return m_rigidBodies;
}

void ParticleSystem::clearRigidBodies()
{
    m_rigidBodies.clear();
}

void ParticleSystem::addEmitter(Emitter *emitter)
{
    m_emitters << emitter;
}

QList<Emitter *> ParticleSystem::emitters() const
{
    return m_emitters;
}

void ParticleSystem::clearEmitters()
{
    m_emitters.clear();
}

void ParticleSystem::setProfilingOutput(QFile *profilingOutput)
{
    m_profilingOutput = profilingOutput;
}

void ParticleSystem::update()
{
    // Acquire OpenCL shared buffers
    glFinish();
    m_context.acquire(m_particleBasicBuffer);
    m_context.acquire(m_particleVelocityBuffer);

    // SIMULATION 1
    CLEventTree eventsSimulation;
    eventsSimulation << m_simulation->integrate(m_particleBasicBuffer, m_particleVelocityBuffer, m_numParticles);
    eventsSimulation << emitParticlesStep(m_particleBasicBuffer, m_particleVelocityBuffer);

    // GRID
    CLEventTree eventsGrid;
    eventsGrid << m_grid->update(m_particleBasicBuffer, m_particleBasicAuxBuffer, m_numParticles, m_cellStartsBuffer, m_usedCellsBuffer);
    eventsGrid << (*m_moveParticleVelocity)(m_particleVelocityBuffer, m_particleVelocityAuxBuffer, m_numParticles, m_grid->lastMoveTable());

    // SIMULATION 2
    eventsSimulation << m_simulation->relax(m_particleBasicAuxBuffer, m_particleVelocityAuxBuffer,
                                              m_particleBasicBuffer, m_particleVelocityBuffer,
                                              m_cellStartsBuffer, m_usedCellsBuffer,
                                              m_rigidBodiesBuffer, m_rigidBodies.count());

    // Profiling data
    m_context.finish();
    m_prevGridConstructionTime = eventsGrid.milliSeconds();
    m_prevSimulationUpdateTime = eventsSimulation.milliSeconds();
    m_maxGridConstructionTime = qMax(m_maxGridConstructionTime, m_prevGridConstructionTime);
    m_maxSimulationUpdateTime = qMax(m_maxSimulationUpdateTime, m_prevSimulationUpdateTime);

    // Profiling output
    if (m_profilingOutput) {
        QByteArray line;
        line += QByteArray::number(m_iteration) + "\t";
        line += QByteArray::number(m_numParticles) + "\t";
        line += QByteArray::number(m_prevGridConstructionTime) + "\t";
        line += QByteArray::number(m_prevSimulationUpdateTime) + "\n";
        m_profilingOutput->write(line);
        if (m_iteration % 16 == 0)
            m_profilingOutput->flush();
    }

    // Release OpenCL shared buffers
    m_context.release(m_particleBasicBuffer);
    m_context.release(m_particleVelocityBuffer);
    m_context.finish();

    ++m_iteration;
}

void ParticleSystem::render(float pointSize)
{
    glPointSize(pointSize);

    glPushMatrix();
    float scale = 1.0f / INTEGER_FACTOR;
    glScalef(scale, scale, scale);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    m_particleBasicBufferGL.bind();
    glVertexPointer(3, GL_INT, sizeof(ParticleBasic), (char*)offsetof(ParticleBasic, posX));
    m_particleVelocityBufferGL.bind();
    glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(ParticleVelocity), (char*)offsetof(ParticleVelocity, color));
    glDrawArrays(GL_POINTS, 0, m_numParticles);
    m_particleVelocityBufferGL.release();
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

    glPopMatrix();
}

void ParticleSystem::rigidBodyChanged()
{
    RigidBody *object = qobject_cast<RigidBody*>(sender());
    if (object) {
        int index = m_rigidBodies.indexOf(object);
        Q_ASSERT(index != -1);
        writeRigidBody(object, index);
    }
}

void ParticleSystem::allocateBuffers()
{
    ParticleBasic *basicData = nullptr;
    ParticleVelocity *velocityData = nullptr;

    // Initialize the scene with some particles?
    if (m_numParticles) {
        basicData = new ParticleBasic[m_numParticles];
        velocityData = new ParticleVelocity[m_numParticles];

        // The particle distance where the density is relaxed
        float d = 0.75 / pow(DENSITY_NOMINAL, 1.0 / 3.0);

        int numPerDim = ceil(pow((double)m_numParticles, 1.0 / 3.0));
        QVector3D base = { .5, .5, .5 };
        base *= 1 << m_gridDepth;
        float sizeHalf = d * numPerDim / 2;
        base -= QVector3D(sizeHalf, sizeHalf, sizeHalf);

        int id = 0;
        for (int z = 0; z < numPerDim && id < (int)m_numParticles; ++z) {
            for (int y = 0; y < numPerDim && id < (int)m_numParticles; ++y) {
                for (int x = 0; x < numPerDim && id < (int)m_numParticles; ++x, ++id) {
                    QVector3D position = base + d * QVector3D(x, y, z);
                    QVector3D offset = .8 * d * QVector3D(frand(), frand(), frand());
                    position += offset;
                    position *= INTEGER_FACTOR;

                    ParticleBasic &pBasic = basicData[id];
                    pBasic.posX = position.x(); pBasic.posY = position.y(); pBasic.posZ = position.z();
                    pBasic.id = id;

                    ParticleVelocity &pVelocity = velocityData[id];
                    pVelocity.velX = 0; pVelocity.velY = 0; pVelocity.velZ = 0;
                }
            }
        }
    }

    // Create shared buffers
    m_particleBasicBufferGL = QGLBuffer(QGLBuffer::VertexBuffer);
    if (m_particleBasicBufferGL.create()) {
        if (m_particleBasicBufferGL.bind()) {
            // Create GL buffer:
            m_particleBasicBufferGL.setUsagePattern(QGLBuffer::DynamicDraw);
            m_particleBasicBufferGL.allocate(m_allocParticles * sizeof(ParticleBasic));
            if (basicData)
                m_particleBasicBufferGL.write(0, basicData, m_numParticles * sizeof(ParticleBasic));
            m_particleBasicBufferGL.release();
            // Create CL buffer:
            m_particleBasicBuffer = m_context.createGLBuffer(m_particleBasicBufferGL, QCLMemoryObject::ReadWrite);
        } else {
            qWarning("Failed to bind vertex buffer");
        }
    } else {
        qWarning("Failed to create vertex buffer");
    }
    m_particleVelocityBufferGL = QGLBuffer(QGLBuffer::VertexBuffer);
    if (m_particleVelocityBufferGL.create()) {
        if (m_particleVelocityBufferGL.bind()) {
            // Create GL buffer:
            m_particleVelocityBufferGL.setUsagePattern(QGLBuffer::DynamicDraw);
            m_particleVelocityBufferGL.allocate(m_allocParticles * sizeof(ParticleVelocity));
            if (velocityData)
                m_particleVelocityBufferGL.write(0, velocityData, m_numParticles * sizeof(ParticleVelocity));
            m_particleVelocityBufferGL.release();
            // Create CL buffer:
            m_particleVelocityBuffer = m_context.createGLBuffer(m_particleVelocityBufferGL, QCLMemoryObject::ReadWrite);
        } else {
            qWarning("Failed to bind vertex buffer");
        }
    } else {
        qWarning("Failed to create vertex buffer");
    }

    delete[] basicData;
    delete[] velocityData;

    // Create the OpenCL-only buffers
    m_particleBasicAuxBuffer = m_context.createBufferDevice(m_allocParticles * sizeof(ParticleBasic), QCLMemoryObject::ReadWrite);
    m_particleVelocityAuxBuffer = m_context.createBufferDevice(m_allocParticles * sizeof(ParticleVelocity), QCLMemoryObject::ReadWrite);
    m_cellStartsBuffer = m_context.createBufferDevice(sizeof(cl_uint) * (m_grid->numGridCells() + 1), QCLMemoryObject::ReadWrite);
    m_usedCellsBuffer = m_context.createBufferDevice(sizeof(cl_uint) * m_grid->numGridCells(), QCLMemoryObject::ReadWrite);
    m_rigidBodiesBuffer = m_context.createBufferDevice(sizeof(RigidBodyData) * m_allocRigidBodies, QCLMemoryObject::ReadWrite);
}

CLEventTree ParticleSystem::emitParticlesStep(QCLBuffer & particleBasic, QCLBuffer & particleVelocity)
{
    QVector<ParticleBasic> addParticlesBasic;
    QVector<ParticleVelocity> addParticlesVelocity;

    for (Emitter *emitter : m_emitters) {
        if (emitter->isEnabled()) {
            int e = emitter->emitCount(DT);
            for (int k = 0; k < e; ++k) {
                QVector3D position = emitter->position() * size();
                QVector3D velocity = emitter->velocity() * size();

                // Form a cylindrical stream of emitted particles. For this, we distribute the emitted particles on a disc.
                float velocityLength = velocity.length();
                if (velocityLength > VELOCITY_MAX) {
                    velocity *= VELOCITY_MAX / velocityLength;
                    velocityLength = VELOCITY_MAX;
                }
                QVector3D n = (velocityLength < 0.01f) ? QVector3D(0, 0, -1) : (velocity / velocityLength);
                QVector3D up = qAbs(n.z()) >= .7f ? QVector3D(0, 1, 0) : QVector3D(0, 0, 1); // An arbitrary vector which only has to point in a different direction than the velocity.
                QVector3D u = QVector3D::crossProduct(n, up).normalized();
                QVector3D v = QVector3D::crossProduct(n, u).normalized();
                float r = sqrt(.15 / DENSITY_NOMINAL * emitter->rate() / emitter->density() / (velocityLength + 0.01f));
                QPointF pointOnDisc = randDisc(r);
                position += u * pointOnDisc.x() + v * pointOnDisc.y();
                position.setX(qBound(0.0f, (float)position.x(), (float)size()));
                position.setY(qBound(0.0f, (float)position.y(), (float)size()));
                position.setZ(qBound(0.0f, (float)position.z(), (float)size()));

                // Construct particle
                ParticleBasic pBasic = ParticleBasic();
                ParticleVelocity pVelocity = ParticleVelocity();
                position *= INTEGER_FACTOR;
                pBasic.posX = position.x(); pBasic.posY = position.y(); pBasic.posZ = position.z();
                pVelocity.velX = velocity.x(); pVelocity.velY = velocity.y(); pVelocity.velZ = velocity.z();
                pBasic.id = m_numParticles + addParticlesBasic.count();

                addParticlesBasic << pBasic;
                addParticlesVelocity << pVelocity;
            }
        }
    }

    if (addParticlesBasic.isEmpty())
        return CLEventTree();

    uint numAddParticles = addParticlesBasic.count();
    uint newNumParticles = m_numParticles + numAddParticles;
    if (newNumParticles > m_maxParticles) {
        //qWarning("Limit reached. I will not add any more particles.");
        return CLEventTree();
    }
    if (newNumParticles > m_allocParticles) {
        //qWarning("Particle buffer full. I will not add any more particles.");
        return CLEventTree();
    }

    CLEventTree event1(particleBasic.writeAsync(sizeof(ParticleBasic) * m_numParticles,
                          &addParticlesBasic.first(),
                          sizeof(ParticleBasic) * numAddParticles),
                      "appendToBasicBuffer");

    CLEventTree event2(particleVelocity.writeAsync(sizeof(ParticleBasic) * m_numParticles,
                          &addParticlesVelocity.first(),
                          sizeof(ParticleBasic) * numAddParticles),
                      "appendToVelocityBuffer");

    m_numParticles = newNumParticles;
    qDebug("numParticles = %d", m_numParticles);

    return CLEventTree("emitParticlesStep") << event1 << event2;
}

void ParticleSystem::writeRigidBody(const RigidBody *object, int index)
{
    RigidBodyData data;

    // Type-independent properties
    data.origin[0] = object->position().x() * size();
    data.origin[1] = object->position().y() * size();
    data.origin[2] = object->position().z() * size();
    data.inverted = object->isInverted();
    data.reflectT = 255.99 * qBound(0.0, object->reflectTangential(), 1.0);
    data.reflectN = 255.99 * qBound(0.0, object->reflectNormal(), 1.0);
    data.type = 0;

    // Type-specific properties
    const QMetaObject *metaObject = object->metaObject();
    if (metaObject == &RigidBox::staticMetaObject) {
        data.type = RIGIDBODY_TYPE_BOX;
        const RigidBox *box = qobject_cast<const RigidBox*>(object);
        data.ex.box.size.s[0] = box->size().x() * size();
        data.ex.box.size.s[1] = box->size().y() * size();
        data.ex.box.size.s[2] = box->size().z() * size();
    } else if (metaObject == &RigidSphere::staticMetaObject) {
        data.type = RIGIDBODY_TYPE_SPHERE;
        const RigidSphere *sphere = qobject_cast<const RigidSphere*>(object);
        data.ex.sphere.radius = sphere->radius() * size();
    } else {
        qFatal("Unknown rigid body type: %s", metaObject->className());
        return;
    }

    // Write into CL buffer
    m_rigidBodiesBuffer.writeAsync(index * sizeof(RigidBodyData), &data, sizeof(RigidBodyData));
}
