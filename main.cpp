//#include "mainwidget.h"
#include <QApplication>
#include <qclcontextgl.h>
#include "clcontext.h"
#include "particlesystem.h"
//#include "viewport.h"

#include "test/runtests.h"
#include "benchmark/guibenchmark.h"

#include "util.h"
#include <QFile>

#include "rigidbody.h"
#include "fluidsimulation.h"
#include <QQuickView>
#include <QProcess>
#include <QTimer>
#include <QQmlEngine>
#include <QSignalMapper>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    srand(0);

    // Register QML types
    qmlRegisterType<FluidSimulation>("FluidSimulation", 1, 0, "FluidSimulation");
    qmlRegisterType<Emitter>("FluidSimulation", 1, 0, "Emitter");
    qmlRegisterType<RigidBody>("FluidSimulation", 1, 0, "RigidBody");
    qmlRegisterType<RigidBox>("FluidSimulation", 1, 0, "RigidBox");
    qmlRegisterType<RigidSphere>("FluidSimulation", 1, 0, "RigidSphere");

    if (a.arguments().contains("test"))
    {
        CLContext context(":/cl/");
        runTests(context);
        qDebug("All tests passed.");
        return 0;
    }
    else if (a.arguments().contains("benchmark"))
    {
        CLContext context(":/cl/", CLContext::Profiling);
        GuiBenchmark gui(context);
        gui.show();
        return a.exec();
    }
    else if (a.arguments().contains("presentation"))
    {
        QSurfaceFormat format;
        format.setSamples(2);
        bool isFullScreen = true;

        QQuickView w;
        w.setFormat(format);
        w.setSource(QUrl("qrc:///qml/presentation/presentation.qml"));
        w.setResizeMode(QQuickView::SizeRootObjectToView);
        if (isFullScreen)
            w.showFullScreen();
        else
            w.show();
        QObject::connect(w.engine(), SIGNAL(quit()), qApp, SLOT(quit()));

        QSignalMapper toggleFullScreen;
        toggleFullScreen.setMapping(w.rootObject(), 42);
        QObject::connect(w.rootObject(), SIGNAL(toggleFullScreen()), &toggleFullScreen, SLOT(map()));
        QObject::connect(&toggleFullScreen, (void (QSignalMapper::*)(int)) &QSignalMapper::mapped, [&](int){
#if (QT_VERSION >= QT_VERSION_CHECK(5, 1, 0))
            if (w.visibility() & QWindow::FullScreen)
                w.showNormal();
            else
                w.showFullScreen();
#else
            if (isFullScreen)
                w.showNormal();
            else
                w.showFullScreen();
            isFullScreen = !isFullScreen;
#endif
        });
        return a.exec();
    }
    else
    {
        QQuickView w;
        w.setSource(QUrl("qrc:///qml/main.qml"));
        w.setResizeMode(QQuickView::SizeRootObjectToView);
        w.show();
        w.create();
        w.resize(1280, 720);


        QProcess *videoRecordingDevice = 0;

        // Enable this code to render the simulation into a video file.
        // "mencoder" is required for this.
        // You can adapt the mencoder settings.
#if 0
        videoRecordingDevice = new QProcess(&w);
        videoRecordingDevice->setProcessChannelMode(QProcess::MergedChannels);

        QString prog = "/usr/bin/mencoder";

        QStringList args;
        args << "-"; // input: stdin
        args << "-demuxer" << "lavf";
        args << "-lavfdopts" << "format=mjpeg";
        args << "-fps" << "60";
        args << "-ovc" << "x264";
        args << "-x264encopts" << "subq=5:8x8dct:frameref=2:bframes=3:b_pyramid=normal:weight_b";
        args << "-oac" << "copy";
        args << "-o" << "video6.avi";

        videoRecordingDevice->start(prog, args);
        videoRecordingDevice->waitForStarted();

        if (!videoRecordingDevice->isOpen()) {
            qFatal("Failed to run mencoder.");
        }


        auto grabFrame = [&]{
            w.grabWindow().save(videoRecordingDevice, "jpg", 100);
            std::cout << videoRecordingDevice->readAll().constData();
        };
        QTimer timer;
        timer.setInterval(18);
        QObject::connect(&timer, &QTimer::timeout, grabFrame);
        timer.start();
#endif

        int r = a.exec();

        if (videoRecordingDevice) {
            videoRecordingDevice->closeWriteChannel();
            videoRecordingDevice->waitForFinished();
        }

        return r;
    }
}
