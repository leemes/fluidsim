#include "grid.h"
#include "util/partition.h"
#include "util/streamcompaction.h"
#include "util.h"
#include "cl/particle.h"
#include "clcontext.h"
#include "cleventtree.h"


const QString typeName = "ParticleBasic";
const size_t typeSize = sizeof(ParticleBasic);
const QString keyExpression = "ParticleBasic_getCell24(& element)";


Grid::Grid(CLContext &context, uint hierarchyDepth) :
    m_depth(hierarchyDepth),
    m_numGridCells(1 << (3 * hierarchyDepth)),
    m_partition(new Partition(context, typeName, typeSize, keyExpression, 3 * hierarchyDepth)),
    m_compaction(new StreamCompaction(context, "uint", sizeof(cl_uint), "uint", sizeof(cl_uint), 1, "input[index+INPUT_STRIDE] > input[index]", "index", "UINT_MAX"))
{
}

Grid::~Grid()
{
}

uint Grid::minBufferSize() const
{
    return m_partition->minBufferSize();
}

uint Grid::minBufferSizeForNumParticles(uint numParticles) const
{
    return m_partition->minBufferSizeForNumElements(numParticles);
}

CLEventTree Grid::update(QCLBuffer &particlesInput, QCLBuffer &sortedParticlesOutput, uint numParticles, QCLBuffer &cellStartsOutput, const QCLBuffer &usedGridCellsOutput) const
{
    Q_ASSERT(particlesInput.access() == QCLBuffer::ReadOnly || particlesInput.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(sortedParticlesOutput.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(cellStartsOutput.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(cellStartsOutput.size() >= sizeof(cl_uint) * (m_numGridCells + 1));
    Q_ASSERT(usedGridCellsOutput.isNull() || usedGridCellsOutput.access() == QCLBuffer::ReadWrite);
    Q_ASSERT(usedGridCellsOutput.isNull() || usedGridCellsOutput.size() >= sizeof(cl_uint) * m_numGridCells);

    CLEventTree events("Grid::update");

    events << (*m_partition)(particlesInput, sortedParticlesOutput, numParticles, cellStartsOutput);

    if (!usedGridCellsOutput.isNull())
        events << (*m_compaction)(cellStartsOutput, usedGridCellsOutput, m_numGridCells);

    return events;
}

const QCLBuffer &Grid::lastMoveTable() const
{
    return m_partition->lastMoveTable();
}

