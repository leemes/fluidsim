#include "simulation.h"
#include "clcontext.h"
#include "cleventtree.h"
#include <QTimer>
#include "grid.h"
#include "util/fill.h"
#include "util.h"
#include "debug.h"

// OpenCL shared code:
#include "cl/particle.h"


// We will start this many work groups. If there are more occupied cell groups in the scene,
// the work groups will proceed with the next cell group (strided by the number of work groups).
#define NUM_PARALLEL_CELLS     (1 << 12)


Simulation::Simulation(CLContext &context, uint gridDepth) :
    m_context(context)
{
    uint gridSize = 1U << gridDepth;
    uint gridVolume = gridSize * gridSize * gridSize;

    QMap<QString,QString> defineMacros;
    defineMacros["GRID_SIZE"] = QString::number(gridSize);

    m_program = context.buildProgram("simulation.cl", defineMacros);

    m_kernelIntegrate = m_program.createKernel("integrate");
    m_kernelRelax = m_program.createKernel("relax");

    // Set work sizes
    m_kernelIntegrate.setLocalWorkSize(256);
    m_workGroupSize = m_kernelRelax.declaredWorkGroupSize().width();
    m_kernelRelax.setLocalWorkSize(m_workGroupSize);
}

CLEventTree Simulation::integrate(const QCLBuffer &particleBasicInplace, const QCLBuffer &particleVelocityInplace, uint numParticles)
{
    m_kernelIntegrate.setRoundedGlobalWorkSize(numParticles);

    return CLEventTree(m_kernelIntegrate(particleBasicInplace, particleVelocityInplace, numParticles),
                       "Simulation::integrate");
}

CLEventTree Simulation::relax(const QCLBuffer &particleBasicInput, const QCLBuffer &particleVelocityInput,
                              const QCLBuffer &particleBasicOutput, const QCLBuffer &particleVelocityOutput,
                              const QCLBuffer &gridCellStarts, const QCLBuffer &usedGridCells,
                              const QCLBuffer &rigidBodies, uint numRigidBodies)
{
    m_kernelRelax.setGlobalWorkSize(NUM_PARALLEL_CELLS * m_workGroupSize);

    return CLEventTree(m_kernelRelax(particleBasicInput, particleVelocityInput,
                                     particleBasicOutput, particleVelocityOutput,
                                     gridCellStarts, usedGridCells,
                                     rigidBodies, numRigidBodies),
                       "Simulation::relax");
}
