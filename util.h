#ifndef UTIL_H
#define UTIL_H

#include <QString>

inline QString humanReadableBytes(size_t bytes) {
    QString units[] = {
        "Bytes",
        "KB",
        "MB",
        "GB",
        "TB"
    };
    uint i = 0;
    while (bytes >= (1ULL << (10 * (i + 1))) && i < sizeof(units)/sizeof(*units) - 1)
        ++i;
    qreal x = (qreal)bytes / (1ULL << (10 * i));
    if (i)
        return QString::number(x, 'f', 2) + ' ' + units[i];
    else
        return QString::number(bytes) + ' ' + units[i];
}

inline uint bits(uint i) {
    uint l = i > 0;
    while(i /= 2) ++l;
    return l;
}
inline uint log2i_ceil(uint i) {
    return bits(i) - 1;
}
inline uint log2i_floor(uint i) {
    return i ? bits(i - 1) : -1;
}
inline uint roundUpPowerOfTwo(uint i) {
    return 1 << log2i_floor(i);
}

// Random clamp value [0.0, 1.0), i.e. 1.0 is NOT included
inline float frand() {
    float r = (float)rand() / ((float)RAND_MAX + 1);
    if (r >= 1.0) r = 0.0;
    return r;
}

#endif // UTIL_H
