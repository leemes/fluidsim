#ifndef CLCONTEXT_H
#define CLCONTEXT_H

#include <qclmemoryobject.h>

class CLSourceLoader;
class QCLContextGL;
class QCLCommandQueue;
class QCLBuffer;
class QCLProgram;
class QCLImage2D;
class QGLBuffer;

typedef unsigned int GLuint;

class CLContext
{
public:
    enum Options {
        NullOptions = 0,
        OutOfOrder  = 1,
        Profiling   = 2,
        GlSharing   = 4
    };

    enum CompilerFlags {
        NullCompilerFlags           = 0,
        OptimizationMadEnable       = 1,
        OptimizationNoSignedZeros   = 2,
        OptimizationUnsafeMath      = 4,
        OptimizationFiniteMathOnly  = 8,
        OptimizationFastRelaxedMath = 16,
        OptimizationSinglePrecConst = 32,
        OptimizationDenormsAreZero  = 64,
        OptimizationStrictAliasing  = 128
    };

    CLContext(QString sourceFileBase, Options options = NullOptions);
    ~CLContext();

    Options options() const { return m_options; }

    // Compiler flags. They are used when building a program directly using this context.
    void setCompilerFlags(CompilerFlags flags);
    void setCompilerFlag(CompilerFlags flag);
    void unsetCompilerFlag(CompilerFlags flag);

    // Adds an "auto include" rule, which will include a header file every time a type appears in the source code.
    // Note that this "appearance" is based on string search (in source code and macros defined with "-D"), not by using a code model.
    void addAutoInclude(QString typeName, QString fileName);

    QCLProgram createProgram(const QString &relativeFileName);
    QCLProgram createProgram(const QString &relativeFileName, const QByteArray &prependedCode);
    QCLProgram buildProgram(const QString &relativeFileName, const QMap<QString, QString> &defineMacros = QMap<QString, QString>());
    QCLProgram buildProgram(const QString &relativeFileName, const QByteArray &prependedCode, const QMap<QString, QString> &defineMacros = QMap<QString, QString>());

    QCLBuffer createBufferDevice(size_t size, QCLMemoryObject::Access access);
    QCLBuffer createBufferHost(void *data, size_t size, QCLMemoryObject::Access access);
    QCLBuffer createBufferCopy(const void *data, size_t size, QCLMemoryObject::Access access);

    QCLCommandQueue commandQueue();

    void finish();

    // GlSharing:
    QCLImage2D createTexture2D(GLuint texture, QCLMemoryObject::Access access);
    QCLBuffer createGLBuffer(GLuint buffer, QCLMemoryObject::Access access);
    QCLBuffer createGLBuffer(const QGLBuffer &buffer, QCLMemoryObject::Access access);
    QCLEvent acquire(const QCLMemoryObject &mem);
    QCLEvent release(const QCLMemoryObject &mem);

private:
    QScopedPointer<CLSourceLoader> m_sourceLoader;
    QScopedPointer<QCLContext> m_context;
    QCLContextGL *m_contextGL;
    Options m_options;
    CompilerFlags m_compilerFlags;

    static QString compilerFlagToString(CompilerFlags flag);
    static QString compilerFlagsToString(CompilerFlags flags);
};

#endif // CLCONTEXT_H
