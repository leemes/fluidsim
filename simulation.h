#ifndef PRINTSIM_H
#define PRINTSIM_H

#include <qclkernel.h>
#include <qclprogram.h>
#include <QColor>
#include "size3d.h"

class CLContext;
class CLEventTree;


class Simulation
{
public:
    Simulation(CLContext &context, uint gridDepth);

    CLContext & context() const { return m_context; }

    CLEventTree integrate(const QCLBuffer &particleBasicInplace,
                          const QCLBuffer &particleVelocityInplace,
                          uint numParticles);

    CLEventTree relax(const QCLBuffer &particleBasicInput, const QCLBuffer &particleVelocityInput,
                      const QCLBuffer &particleBasicOutput, const QCLBuffer &particleVelocityOutput,
                      const QCLBuffer &gridCellStarts, const QCLBuffer &usedGridCells,
                      const QCLBuffer &rigidBodies, uint numRigidBodies);

private:
    CLContext & m_context;
    QCLProgram m_program;
    QCLKernel m_kernelIntegrate;
    QCLKernel m_kernelRelax;
    uint m_workGroupSize;

    QCLBuffer m_gridCellCurrentHotnessBuffer;     // updated for every cell (float)
    QCLBuffer m_gridCellOldForceAndDensityBuffer; // updated for hot cells only (float4 with xyz: force, w: density)
};

#endif // PRINTSIM_H
