#ifndef SIZE3D_H
#define SIZE3D_H

#include <QVector3D>

typedef unsigned int uint;

struct Size3D {
    uint x, y, z;
    inline uint volume() const { return x * y * z; }
};

inline QVector3D operator *(const QVector3D &v, const Size3D &s) {
    return QVector3D(v.x() * s.x, v.y() * s.y, v.z() * s.z);
}

#endif // SIZE3D_H
