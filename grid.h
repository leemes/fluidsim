#ifndef GRID_H
#define GRID_H

#include <QScopedPointer>
#include <qclbuffer.h>
#include "size3d.h"

class CLContext;
class CLEventTree;
class Partition;
class StreamCompaction;

class Grid
{
public:
    Grid(CLContext &context, uint hierarchyDepth = 8);
    ~Grid();

    uint hierarchyDepth() const { return m_depth; }
    uint numGridCells() const { return m_numGridCells; }

    uint minBufferSize() const;
    uint minBufferSizeForNumParticles(uint numParticles) const;

    // The particles will be sorted by their morton index and a morton grid will be constructed.
    // For each grid cell, the cellStartsOutput contains its "cell start" (uint), which is the index of the first particle in the sorted output within this cell.
    // Additionally, the number of particles will be appended to this array, so you can always just look at the next value in this array to query the "cell end".
    // Optionally, an array (uint) of non-empty (i.e. used) grid cells will be computed and stored. It contains their indices, the rest is filled with UINT_MAX.
    CLEventTree update(QCLBuffer &particlesInput, QCLBuffer &sortedParticlesOutput, uint numParticles,
                       QCLBuffer &cellStartsOutput, const QCLBuffer &usedGridCellsOutput = QCLBuffer()) const;

    const QCLBuffer &lastMoveTable() const;

private:
    const uint m_depth;
    const uint m_numGridCells;

    // algos:
    const QScopedPointer<Partition> m_partition;
    const QScopedPointer<StreamCompaction> m_compaction;
};

#endif // GRID_H
